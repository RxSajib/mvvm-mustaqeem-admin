package com.mustaqeem.cometogetherendproblemadmin.Adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.mustaqeem.cometogetherendproblemadmin.JobPage.AcceptJob;
import com.mustaqeem.cometogetherendproblemadmin.JobPage.PendingJob;
import com.mustaqeem.cometogetherendproblemadmin.JobPage.RejectJob;

public class JobTabAdapter extends FragmentPagerAdapter {

    private String[] job_tabs = {"Pending", "Remove", "Accept"};

    public JobTabAdapter(FragmentManager supportFragmentManager, Context applicationContext) {
        super(supportFragmentManager);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new PendingJob();

            case 1:
                return new RejectJob();

            case 2:
                return new AcceptJob();

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return job_tabs.length;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return job_tabs[position];
    }
}
