package com.mustaqeem.cometogetherendproblemadmin.Adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mustaqeem.cometogetherendproblemadmin.Model.HairModel;
import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.ViewHolder.HaireViewHolder;
import com.mustaqeem.cometogetherendproblemadmin.data.TimesSinceAgo;
import com.mustaqeem.cometogetherendproblemadmin.databinding.HairSingleItemsBinding;

import java.util.List;

public class HairAdapter extends RecyclerView.Adapter<HaireViewHolder> {

    private List<HairModel> hairModelList;
    private SetOnclick SetOnclick;
    private LayoutInflater layoutInflater;

    public void setHairModelList(List<HairModel> hairModelList) {
        this.hairModelList = hairModelList;
    }

    @NonNull
    @Override
    public HaireViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        layoutInflater = LayoutInflater.from(parent.getContext());
        HairSingleItemsBinding binding = HairSingleItemsBinding.inflate(layoutInflater, parent, false);
        return new HaireViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull HaireViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.binding.Name.setText(hairModelList.get(position).getName());
        holder.binding.Work.setText(hairModelList.get(position).getWork());

        TimesSinceAgo timesSinceAgo = new TimesSinceAgo();
        String Time = timesSinceAgo.getTimeAgo(hairModelList.get(position).getTimestamp(), holder.itemView.getContext());
        holder.binding.Time.setText(Time);

        holder.binding.Contact.setText(
                hairModelList.get(position).getLocation()+"\n"
                        +hairModelList.get(position).getPhone()+"\n"
                        +hairModelList.get(position).getEmail()
        );

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetOnclick.Click(hairModelList.get(position).getTimestamp());
            }
        });


    }

    @Override
    public int getItemCount() {
        if (hairModelList == null) {
            return 0;
        }else {
            return hairModelList.size();
        }
    }

    public interface SetOnclick{
        void Click(long Timestamp);
    }

    public void SetOnclickLisiner(SetOnclick SetOnclick){
        this.SetOnclick = SetOnclick;
    }
}
