package com.mustaqeem.cometogetherendproblemadmin.Adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mustaqeem.cometogetherendproblemadmin.Model.NavigationIconModel;
import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.ViewHolder.NavViewHolder;
import com.mustaqeem.cometogetherendproblemadmin.databinding.NavagationLayoutBinding;

import java.util.List;


public class NavAdapter extends RecyclerView.Adapter<NavViewHolder> {

   private List<NavigationIconModel> list;
   private LayoutInflater layoutInflater;
   private Onclick Onclick;
   private int row_index;

    public NavAdapter(List<NavigationIconModel> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public NavViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        layoutInflater = LayoutInflater.from(parent.getContext());
        NavagationLayoutBinding binding = NavagationLayoutBinding.inflate(layoutInflater, parent, false);
        return new NavViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull NavViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.binding.NavIcon.setImageResource(list.get(position).getIcon());
        holder.binding.NavTitle.setText(list.get(position).getTitle());
        holder.binding.ItemCount.setText(String.valueOf(list.get(position).getSize()));

        holder.itemView.setOnClickListener(view -> {
            Onclick.NavClick(list.get(position).getTitle());

            row_index=position;
            notifyDataSetChanged();

        });
        if(row_index==position){
            holder.binding.NavView.setBackground(holder.itemView.getContext().getResources().getDrawable(R.drawable.nav_teamselected));
        } else {
            holder.binding.NavView.setBackground(holder.itemView.getContext().getResources().getDrawable(R.drawable.nav_unselected));
        }
    }

    @Override
    public int getItemCount() {
        if(list == null){
            return 0;
        }else {
            return list.size();
        }
    }

    public interface Onclick{
        void NavClick(String Title);
    }
    public void OnClickLisiner(Onclick Onclick){
        this.Onclick = Onclick;
    }
}
