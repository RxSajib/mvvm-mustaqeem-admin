package com.mustaqeem.cometogetherendproblemadmin.Adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mustaqeem.cometogetherendproblemadmin.Model.UsersModel;
import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.ViewHolder.UserCounterViewHolder;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class CounterUserAdapter extends RecyclerView.Adapter<UserCounterViewHolder> {

    private List<UsersModel> userModelList;

    public void setUserModelList(List<UsersModel> userModelList) {
        this.userModelList = userModelList;
    }

    @NonNull
    @NotNull
    @Override
    public UserCounterViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_profile_layout, parent, false);
        return new UserCounterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull UserCounterViewHolder holder, int position) {
        Picasso.get().load(userModelList.get(position).getProfileImage()).into(holder.imageView);
        holder.imageView.setVisibility(View.VISIBLE);
        Log.d("img", userModelList.get(position).getProfileImage());
       /* if(position == 9){
            holder.imagecount.setVisibility(View.VISIBLE);
        }else {
            holder.imagecount.setVisibility(View.GONE);
        }*/
    }

    @Override
    public int getItemCount() {

        if(userModelList == null){
            return 0;
        }
        else {
            return userModelList.size();
        }


    }
}
