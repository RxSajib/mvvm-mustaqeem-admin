package com.mustaqeem.cometogetherendproblemadmin.Adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.mustaqeem.cometogetherendproblemadmin.Feedback.ReceiverNotifaction;
import com.mustaqeem.cometogetherendproblemadmin.Feedback.SenderNotifaction;

public class FeedbackPagerAdapter extends FragmentPagerAdapter {

    private String item[] = {"Received", "Send"};

    public FeedbackPagerAdapter(FragmentManager supportFragmentManager, Context applicationContext) {
        super(supportFragmentManager);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new ReceiverNotifaction();

            case 1:
                return new SenderNotifaction();

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return item.length;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return item[position];
    }
}
