package com.mustaqeem.cometogetherendproblemadmin.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mustaqeem.cometogetherendproblemadmin.Model.TeamModel;
import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.ViewHolder.TeamViewHolder;
import com.mustaqeem.cometogetherendproblemadmin.databinding.TeamLayoutBinding;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class TeamAdapter extends RecyclerView.Adapter<TeamViewHolder> {

    private List<TeamModel> teamModelList;
    private LayoutInflater layoutInflater;
    private SetOnClick SetOnClick;

    public void setTeamModelList(List<TeamModel> teamModelList) {
        this.teamModelList = teamModelList;
    }

    @NonNull
    @NotNull
    @Override
    public TeamViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        layoutInflater = LayoutInflater.from(parent.getContext());
        TeamLayoutBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.team_layout, parent, false);
        return new TeamViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull TeamViewHolder holder, int position) {
        holder.binding.TeamPersonNameID.setText(teamModelList.get(position).getName());
        holder.binding.TeamDegID.setText(teamModelList.get(position).getDesignation());
        Picasso.get().load(teamModelList.get(position).getPosterPath()).placeholder(R.drawable.profileimage_placeholder)
                .into(holder.binding.TeamImageID);

        holder.binding.UpdateButton.setOnClickListener(view -> {
            SetOnClick.OnClick(teamModelList.get(position).getTimestamp(), teamModelList.get(position).getPosterPath(), teamModelList.get(position).getName(), teamModelList.get(position).getDesignation());

        });
    }

    @Override
    public int getItemCount() {
        if(teamModelList == null){
            return 0;
        }else {
            return teamModelList.size();
        }
    }


    public interface SetOnClick{
        void OnClick(long Timestamp, String ImageUri, String Name, String Deg);
    }
    public void SetOnLongClickLisiner(SetOnClick SetOnClick){
        this.SetOnClick = SetOnClick;
    }
}
