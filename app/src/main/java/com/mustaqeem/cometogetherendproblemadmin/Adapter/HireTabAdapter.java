package com.mustaqeem.cometogetherendproblemadmin.Adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.mustaqeem.cometogetherendproblemadmin.TabHire.Accept;
import com.mustaqeem.cometogetherendproblemadmin.TabHire.Decline;
import com.mustaqeem.cometogetherendproblemadmin.TabHire.Pending;

public class HireTabAdapter extends FragmentPagerAdapter {

    private String iteam[] = {"Pending", "Remove", "Accept"};

    public HireTabAdapter(FragmentManager supportFragmentManager, Context applicationContext) {
        super(supportFragmentManager);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new Pending();
            case 1:
                return new Decline();
            case 2:
                return new Accept();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return iteam.length;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return iteam[position];
    }
}
