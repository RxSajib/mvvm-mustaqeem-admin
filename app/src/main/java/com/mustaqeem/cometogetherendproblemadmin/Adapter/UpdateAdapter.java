package com.mustaqeem.cometogetherendproblemadmin.Adapter;

import android.annotation.SuppressLint;
import android.text.SpannableString;
import android.text.format.DateFormat;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mustaqeem.cometogetherendproblemadmin.Model.UpdateModel;
import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.ViewHolder.UpdateViewHolder;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;
import com.mustaqeem.cometogetherendproblemadmin.databinding.UpdateSingleIteamBinding;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class UpdateAdapter extends RecyclerView.Adapter<UpdateViewHolder> {

    private List<UpdateModel> updateModelList;
    private LayoutInflater layoutInflater;
    private OnLongClick OnLongClick;
    private Integer RowIndex = null;
    private OnDateClick OnDateClick;

    public void setUpdateModelList(List<UpdateModel> updateModelList) {
        this.updateModelList = updateModelList;
    }

    @NonNull
    @NotNull
    @Override
    public UpdateViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        layoutInflater = LayoutInflater.from(parent.getContext());
        UpdateSingleIteamBinding binding = UpdateSingleIteamBinding.inflate(layoutInflater, parent,false);
        return new UpdateViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull UpdateViewHolder holder, @SuppressLint("RecyclerView") int position) {
        if(updateModelList.get(position).getPostType().equals(DataManager.Image)){
            Picasso.get().load(updateModelList.get(position).getPosterPath())
                    .into(holder.binding.NewsImage);

            Log.d("TAG", updateModelList.get(position).getPosterPath());
        }
        holder.binding.AuthorBy.setText(updateModelList.get(position).getAuthorBy());
        holder.binding.Details.setText(updateModelList.get(position).getDetails());
        holder.binding.TypeID.setText(updateModelList.get(position).getType());
        holder.binding.Title.setText(updateModelList.get(position).getTitle());


        holder.itemView.setOnLongClickListener(view -> {
            OnLongClick.Click(updateModelList.get(position).getTimestamp());
             RowIndex = position;
            notifyDataSetChanged();
            return true;
        });

        if(RowIndex != null){
            if(RowIndex == position){
                holder.binding.Iteam.setBackgroundResource(R.color.carbon_red_50);
            }else {
                holder.binding.Iteam.setBackgroundResource(R.color.carbon_white);
            }
        }

        Calendar calendar_date = Calendar.getInstance(Locale.ENGLISH);
        calendar_date.setTimeInMillis(updateModelList.get(position).getTimestamp() * 1000);
        String Date = DateFormat.format(DataManager.DateFormat, calendar_date).toString();
        holder.binding.TimeDate.setText(Date);

        SpannableString content = new SpannableString(updateModelList.get(position).getWebUri());
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        holder.binding.WebLink.setText(content);

        holder.binding.WebLink.setOnClickListener(view -> {
            OnDateClick.DateClick(updateModelList.get(position).getWebUri());
        });
    }

    @Override
    public int getItemCount() {
        if(updateModelList == null){
            return 0;
        }else {
            return updateModelList.size();
        }
    }

    public interface OnLongClick{
        void Click(long NewsID);
    }
    public void SetOnLongClickLis(OnLongClick OnLongClick){
        this.OnLongClick = OnLongClick;
    }

    public interface OnDateClick{
        void DateClick(String Uri);
    }
    public void OnDateClickEvent(OnDateClick OnDateClick){
        this.OnDateClick = OnDateClick;
    }
}
