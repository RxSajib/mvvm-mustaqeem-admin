package com.mustaqeem.cometogetherendproblemadmin.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mustaqeem.cometogetherendproblemadmin.Model.UsersModel;
import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.ViewHolder.UserViewHolder;
import com.mustaqeem.cometogetherendproblemadmin.databinding.UserLayoutBinding;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class UserAdapter  extends RecyclerView.Adapter<UserViewHolder> {

    private List<UsersModel> userModelList;
    private LayoutInflater layoutInflater;
    private Click Click;

    public void setUserModelList(List<UsersModel> userModelList) {
        this.userModelList = userModelList;
    }

    @NonNull
    @NotNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {

        layoutInflater = LayoutInflater.from(parent.getContext());
        UserLayoutBinding binding = UserLayoutBinding.inflate(layoutInflater, parent, false);
        return new UserViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull UserViewHolder holder, int position) {

        holder.binding.UserEmailID.setText(userModelList.get(position).getEmail());
        holder.binding.UserNameID.setText(userModelList.get(position).getName());
        Picasso.get().load(userModelList.get(position).getProfileImage()).placeholder(R.drawable.image_placeholder).into(holder.binding.ProfileImageID);

        holder.itemView.setOnLongClickListener(view -> {
            Click.Click(userModelList.get(position).getName(), userModelList.get(position).getUID());
            return true;
        });

    }

    @Override
    public int getItemCount() {
        if(userModelList == null){
            return 0;
        }else {
            return userModelList.size();
        }
    }

    public interface Click{
        void Click(String Name, String UID);
    }

    public void SetOnClickLisiner(Click Click){
        this.Click = Click;
    }
}
