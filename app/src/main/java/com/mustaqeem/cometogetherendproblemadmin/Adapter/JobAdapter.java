package com.mustaqeem.cometogetherendproblemadmin.Adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mustaqeem.cometogetherendproblemadmin.Model.JobModel;
import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.ViewHolder.JobViewHolder;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;
import com.mustaqeem.cometogetherendproblemadmin.data.TimesSinceAgo;
import com.mustaqeem.cometogetherendproblemadmin.databinding.JobsinglelayoutBinding;

import java.util.Currency;
import java.util.List;
import java.util.Locale;

public class JobAdapter extends RecyclerView.Adapter<JobViewHolder> {

    private List<JobModel> jobModelList;
    public void setJobModelList(List<JobModel> jobModelList) {
        this.jobModelList = jobModelList;
    }
    private SetOnclick SetOnclick;
    private LayoutInflater layoutInflater;


    @NonNull
    @Override
    public JobViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        layoutInflater = LayoutInflater.from(parent.getContext());
        JobsinglelayoutBinding binding = JobsinglelayoutBinding.inflate(layoutInflater, parent, false);
        return new JobViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull JobViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.binding.Industry.setText(jobModelList.get(position).getIndustry());
        holder.binding.Price.setText(jobModelList.get(position).getStartSalary()+" - "+jobModelList.get(position).getEndSalary()+DataManager.Par_month);
        holder.binding.Location.setText(jobModelList.get(position).getLocation());
        holder.binding.JobType.setText(jobModelList.get(position).getJobType());
        holder.binding.Contact.setText(jobModelList.get(position).getContract());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetOnclick.Onclick(jobModelList.get(position).getTimestamp(), jobModelList.get(position).getUID());
            }
        });

        TimesSinceAgo timesSinceAgo = new TimesSinceAgo();
        String Time = timesSinceAgo.getTimeAgo(jobModelList.get(position).getTimestamp(), holder.itemView.getContext());
        holder.binding.Time.setText(Time);

        String MCurrency = jobModelList.get(position).getCurrency();
        if(MCurrency.equals(DataManager.India_Rupee)){
            Locale locale=new Locale(DataManager.EN, DataManager.IN);
            java.util.Currency currency= Currency.getInstance(locale);
            String symbol = currency.getSymbol();
            holder.binding.CurrencyName.setText(symbol);
        }if(MCurrency.equals(DataManager.Pakistan_Rupee)){
            Locale locale=new Locale(DataManager.EN, DataManager.PK);
            java.util.Currency currency=Currency.getInstance(locale);
            String symbol = currency.getSymbol();
            holder.binding.CurrencyName.setText(symbol);
        }if(MCurrency.equals(DataManager.AED)){
            Locale locale=new Locale(DataManager.EN, DataManager.AE);
            java.util.Currency currency=Currency.getInstance(locale);
            String symbol = currency.getSymbol();
            holder.binding.CurrencyName.setText(symbol);
        }if(MCurrency.equals(DataManager.BDT)){
            Locale locale=new Locale(DataManager.EN, DataManager.BD);
            java.util.Currency currency=Currency.getInstance(locale);
            String symbol = currency.getSymbol();
            holder.binding.CurrencyName.setText(symbol);
        }if(MCurrency.equals(DataManager.Saudi_Riyal)){
            Locale locale=new Locale(DataManager.EN, DataManager.SA);
            java.util.Currency currency=Currency.getInstance(locale);
            String symbol = currency.getSymbol();
            holder.binding.CurrencyName.setText(symbol);
        }
    }

    @Override
    public int getItemCount() {
       if(jobModelList == null){
           return 0;
       }else {
           return jobModelList.size();
       }
    }

    public interface SetOnclick{
        void Onclick(long key, String UID);
    }

    public void SetOnClickLisiner(SetOnclick SetOnclick){
        this.SetOnclick = SetOnclick;
    }

}
