package com.mustaqeem.cometogetherendproblemadmin.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mustaqeem.cometogetherendproblemadmin.Model.NotificationModel;
import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.ViewHolder.SendFeedbackViewHolder;

import java.util.List;

public class SendNotificationAdapter extends RecyclerView.Adapter<SendFeedbackViewHolder> {

    private List<NotificationModel> notificationModelList;

    public void setNotificationModelList(List<NotificationModel> notificationModelList) {
        this.notificationModelList = notificationModelList;
    }

    @NonNull
    @Override
    public SendFeedbackViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SendFeedbackViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.sendfeedback_iteam, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull SendFeedbackViewHolder holder, int position) {
        holder.Message.setText(notificationModelList.get(position).getMessage());

        Toast.makeText(holder.itemView.getContext(), notificationModelList.get(position).getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getItemCount() {
        if(notificationModelList == null){
            return 0;
        }else {
            return notificationModelList.size();
        }
    }
}
