package com.mustaqeem.cometogetherendproblemadmin.Adapter;

import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mustaqeem.cometogetherendproblemadmin.Model.NotificationModel;
import com.mustaqeem.cometogetherendproblemadmin.ViewHolder.ReceivedFeedbackViewHolder;

import java.util.List;

public class ReceivedNotifactionAdapter extends RecyclerView.Adapter<ReceivedFeedbackViewHolder> {

    private List<NotificationModel> notificationModelList;

    public void setNotificationModelList(List<NotificationModel> notificationModelList) {
        this.notificationModelList = notificationModelList;
    }

    @NonNull
    @Override
    public ReceivedFeedbackViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull ReceivedFeedbackViewHolder holder, int position) {
        Toast.makeText(holder.itemView.getContext(), notificationModelList.get(position).getSendBy(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getItemCount() {
        if(notificationModelList == null){
            return 0;
        }else {
            return notificationModelList.size();
        }
    }
}
