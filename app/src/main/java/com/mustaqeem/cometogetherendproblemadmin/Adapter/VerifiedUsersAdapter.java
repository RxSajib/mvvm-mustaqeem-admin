package com.mustaqeem.cometogetherendproblemadmin.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mustaqeem.cometogetherendproblemadmin.Model.UsersModel;
import com.mustaqeem.cometogetherendproblemadmin.ViewHolder.VerifiedUsersHolder;
import com.mustaqeem.cometogetherendproblemadmin.databinding.UservarifiedSinglelayoutBinding;
import com.squareup.picasso.Picasso;

import java.util.List;

public class VerifiedUsersAdapter extends RecyclerView.Adapter<VerifiedUsersHolder> {

    public List<UsersModel> list;
    private LayoutInflater layoutInflater;

    public void setList(List<UsersModel> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public VerifiedUsersHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        layoutInflater = LayoutInflater.from(parent.getContext());
        UservarifiedSinglelayoutBinding binding = UservarifiedSinglelayoutBinding.inflate(layoutInflater, parent, false);
        return new VerifiedUsersHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull VerifiedUsersHolder holder, int position) {
        Picasso.get().load(list.get(position).getProfileImage()).into(holder.binding.UserImage);
        holder.binding.UserName.setText(list.get(position).getName());
        holder.binding.UserEmailID.setText(list.get(position).getEmail());
        holder.binding.ID.setText(list.get(position).getCardNumber());
        holder.binding.IssueDate.setText(list.get(position).getDateOfIssue());
        holder.binding.ExpDate.setText(list.get(position).getDateOFExp());
    }

    @Override
    public int getItemCount() {
        if(list != null){
            return list.size();
        }else {
            return 0;
        }
    }
}
