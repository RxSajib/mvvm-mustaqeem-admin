package com.mustaqeem.cometogetherendproblemadmin.Network.GET;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.mustaqeem.cometogetherendproblemadmin.Model.LengthOFPendingHair;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

public class LengthOFPendingHairGET {

    private Application application;
    private MutableLiveData<LengthOFPendingHair> data;
    private LengthOFPendingHair lengthOFPendingHair;
    private CollectionReference MPendingHairRef;

    public LengthOFPendingHairGET(Application application){
        this.application = application;
        lengthOFPendingHair = new LengthOFPendingHair();
        MPendingHairRef = FirebaseFirestore.getInstance().collection(DataManager.HireRequest);
    }

    public LiveData<LengthOFPendingHair> getLengthofPendingHair(){
        data =  new MutableLiveData<>();
        MPendingHairRef
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                        if(error != null){
                            return;
                        }
                        if(value.isEmpty()){
                            lengthOFPendingHair.setLength(00);
                            data.setValue(lengthOFPendingHair);
                        }else {
                            lengthOFPendingHair.setLength(value.size());
                            data.setValue(lengthOFPendingHair);
                        }
                    }
                });

        return data;
    }
}
