package com.mustaqeem.cometogetherendproblemadmin.Network.GET;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.mustaqeem.cometogetherendproblemadmin.Model.NotificationModel;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

import java.util.List;

public class NotifactionReceivedGET {

    private Application application;
    private MutableLiveData<List<NotificationModel>> data;
    private CollectionReference MNotifactionRef;

    public NotifactionReceivedGET(Application application){
        this.application = application;
        MNotifactionRef = FirebaseFirestore.getInstance().collection(DataManager.NotificationRoot);
    }

    public LiveData<List<NotificationModel>> getreceiver_botifaction(long Limit){
        data = new MutableLiveData<>();
        Query query  = MNotifactionRef.whereEqualTo(DataManager.SendBy, DataManager.User).limit(Limit);

        MNotifactionRef
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                        if(error != null){
                            return;
                        }
                        if(!value.isEmpty()){
                            for(DocumentChange ds : value.getDocumentChanges()){
                                if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                                    data.setValue(value.toObjects(NotificationModel.class));
                                }
                            }
                        }else {
                            data.setValue(null);
                        }
                    }
                });

        return data;
    }
}
