package com.mustaqeem.cometogetherendproblemadmin.Network.GET;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.mustaqeem.cometogetherendproblemadmin.Model.LengthOfPendingJob;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

public class LengthOFJobPendingGET {

   private Application application;
   private MutableLiveData<LengthOfPendingJob> data;
   private LengthOfPendingJob ofPendingJob;
   private CollectionReference MPendingJobRef;

   public LengthOFJobPendingGET(Application application){
      this.application = application;
      ofPendingJob = new LengthOfPendingJob();
      MPendingJobRef  = FirebaseFirestore.getInstance().collection(DataManager.JobRoot);
   }

   public LiveData<LengthOfPendingJob> getLengthofPendingJob(){
      data = new MutableLiveData<>();
      MPendingJobRef
              .addSnapshotListener(new EventListener<QuerySnapshot>() {
                 @Override
                 public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                    if(error != null){
                       return;
                    }
                    if(value.isEmpty()){
                       ofPendingJob.setLength(00);
                       data.setValue(ofPendingJob);
                    }else {
                       ofPendingJob.setLength(value.size());
                       data.setValue(ofPendingJob);
                    }
                 }
              });

      return data;
   }

}
