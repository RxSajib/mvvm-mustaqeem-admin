package com.mustaqeem.cometogetherendproblemadmin.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.mustaqeem.cometogetherendproblemadmin.Model.IsLoginModel;

public class IsLoginRepository {

    private FirebaseAuth Mauth;
    private Application application;
    private MutableLiveData<IsLoginModel> data;
    private IsLoginModel isLoginModel;

    public IsLoginRepository(Application application) {
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        isLoginModel = new IsLoginModel();
    }

    public LiveData<IsLoginModel> islogin() {
        data = new MutableLiveData<>();
        FirebaseUser Muser = Mauth.getCurrentUser();
        if (Muser == null) {
            isLoginModel.setLogin(false);
            data.setValue(isLoginModel);
        } else {
            isLoginModel.setLogin(true);
            isLoginModel.setFirebaseAuth(Muser.getUid());
            data.setValue(isLoginModel);
        }
        return data;
    }
}
