package com.mustaqeem.cometogetherendproblemadmin.Network.POST;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

import java.util.HashMap;
import java.util.Map;

public class VerificationUserPOST {

    private Application application;
    private CollectionReference MuserRef;
    private MutableLiveData<Boolean> data;

    public VerificationUserPOST(Application application){
        this.application = application;
        MuserRef = FirebaseFirestore.getInstance().collection(DataManager.Users);
    }

    public LiveData<Boolean> Verification(String UID, String DateOFExp, String DateOFIssue, boolean CardActiveStatus, String CardNumber){
        data = new MutableLiveData<>();

        Map<String, Object> map = new HashMap<>();
        map.put(DataManager.DateOFExp, DateOFExp);
        map.put(DataManager.DateOfIssue, DateOFIssue);
        map.put(DataManager.CardActiveStatus, CardActiveStatus);
        map.put(DataManager.CardNumber, CardNumber);

        MuserRef.document(UID).update(map)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            data.setValue(true);
                        }else {
                            data.setValue(false);
                            Toast.makeText(application, "Error "+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                data.setValue(false);
                Toast.makeText(application, "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return data;

    }

}
