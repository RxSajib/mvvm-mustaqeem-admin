package com.mustaqeem.cometogetherendproblemadmin.Network.POST;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mustaqeem.cometogetherendproblemadmin.Model.JobModel;
import com.mustaqeem.cometogetherendproblemadmin.Model.ResponseCode;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

import java.util.HashMap;
import java.util.Map;

public class RejectJobPOST {

    private Application application;
    private MutableLiveData<ResponseCode> data;
    private CollectionReference MJobCollectionRef, RejectJobs;
    private ResponseCode responseCode;

    public RejectJobPOST(Application application){
        this.application = application;
        MJobCollectionRef = FirebaseFirestore.getInstance().collection(DataManager.JobRoot);
        RejectJobs = FirebaseFirestore.getInstance().collection(DataManager.JobRejected);
        responseCode = new ResponseCode();
    }

    public LiveData<ResponseCode> getreject_jobpost(JobModel jobModel){
        data = new MutableLiveData<>();
        if(jobModel != null){
            long Timestamp = System.currentTimeMillis()/1000;
            String TimestampString = String.valueOf(Timestamp);

            Map<String , Object> postmap = new HashMap<>();
            postmap.put(DataManager.JobType, jobModel.getJobType());
            postmap.put(DataManager.StartSalary, jobModel.getStartSalary());
            postmap.put(DataManager.EndSalary, jobModel.getEndSalary());
            postmap.put(DataManager.Experience, jobModel.getExperience());
            postmap.put(DataManager.Rewards, jobModel.getRewards());
            postmap.put(DataManager.Location, jobModel.getLocation());
            postmap.put(DataManager.Contract, jobModel.getContract());
            postmap.put(DataManager.Industry, jobModel.getIndustry());
            postmap.put(DataManager.Timestamp, Timestamp);
            postmap.put(DataManager.UID, jobModel.getUID());
            postmap.put(DataManager.Currency, jobModel.getCurrency());

            RejectJobs.document(TimestampString).set(postmap)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                MJobCollectionRef.document(String.valueOf(jobModel.getTimestamp()))
                                        .delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()){
                                            responseCode.setCode(DataManager.SuccessCode);
                                            data.setValue(responseCode);
                                        }else {
                                            Toast.makeText(application, "Error", Toast.LENGTH_SHORT).show();
                                            responseCode.setCode(DataManager.ErrorCode);
                                            data.setValue(responseCode);
                                        }
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(application, "Error", Toast.LENGTH_SHORT).show();
                                        responseCode.setCode(DataManager.ErrorCode);
                                        data.setValue(responseCode);
                                    }
                                });

                            }else {
                                responseCode.setCode(DataManager.ErrorCode);
                                data.setValue(responseCode);
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(application, "Error", Toast.LENGTH_SHORT).show();
                            responseCode.setCode(DataManager.ErrorCode);
                            data.setValue(responseCode);
                        }
                    });
        }
        return data;
    }
}
