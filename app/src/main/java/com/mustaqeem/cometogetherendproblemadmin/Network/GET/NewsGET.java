package com.mustaqeem.cometogetherendproblemadmin.Network.GET;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.mustaqeem.cometogetherendproblemadmin.Model.UpdateModel;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

import java.util.List;

public class NewsGET {

    private Application application;
    private MutableLiveData<List<UpdateModel>> data;
    private FirebaseFirestore MupdateDatabase;
    private CollectionReference MupdateCollectionRef;

    public NewsGET(Application application){
        this.application = application;
        MupdateDatabase = FirebaseFirestore.getInstance();
    }

    public LiveData<List<UpdateModel>> getupdate(long Limit){
        data = new MutableLiveData<>();
        MupdateCollectionRef = MupdateDatabase.collection(DataManager.UpdateRoot);
        Query FirebaseQuary = MupdateCollectionRef.orderBy(DataManager.Timestamp, Query.Direction.DESCENDING).limit(Limit);

        FirebaseQuary.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                if(error != null){
                    return;
                }
                if(!value.isEmpty()){
                    for(DocumentChange ds : value.getDocumentChanges()){
                        if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                            data.setValue(value.toObjects(UpdateModel.class));
                        }
                    }
                }else {
                    data.setValue(null);
                }
            }
        });

        return data;
    }
}
