package com.mustaqeem.cometogetherendproblemadmin.Network;

import android.app.Application;
import android.net.Uri;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.anstrontechnologies.corehelper.AnstronCoreHelper;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.iceteck.silicompressorr.FileUtils;
import com.iceteck.silicompressorr.SiliCompressor;
import com.mustaqeem.cometogetherendproblemadmin.Model.ImageUploadResponse;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

import org.jetbrains.annotations.NotNull;

import java.io.File;

public class UpdateImageUploadRepository {

    private Application application;
    private MutableLiveData<ImageUploadResponse> data;
    private ImageUploadResponse imageUploadResponse;
    private StorageReference UpdateImageStores;
    private AnstronCoreHelper anstronCoreHelper;

    public UpdateImageUploadRepository(Application application){
        this.application = application;
        imageUploadResponse = new ImageUploadResponse();
        UpdateImageStores = FirebaseStorage.getInstance().getReference().child(DataManager.UpdateImageStores);
        anstronCoreHelper = new AnstronCoreHelper(application);
    }

    public LiveData<ImageUploadResponse> imageupload(Uri imageurl){
        data = new MutableLiveData<>();

        File MyFile = new File(SiliCompressor.with(application)
        .compress(FileUtils.getPath(application, imageurl), new File(application.getFilesDir(), "temp")));


        Uri from_file = Uri.fromFile(MyFile);
        UpdateImageStores.child(anstronCoreHelper.getFileNameFromUri(from_file))
                .putFile(from_file)
               .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                   @Override
                   public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                       if(taskSnapshot.getMetadata() != null){
                           if(taskSnapshot.getMetadata().getReference() != null){
                                Task<Uri> result_task = taskSnapshot.getStorage().getDownloadUrl();
                                result_task.addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                        if(uri != null){
                                            imageUploadResponse.setImageUri(uri.toString());
                                            imageUploadResponse.setUpload(true);
                                            data.setValue(imageUploadResponse);
                                        }else {
                                            Toast.makeText(application, "", Toast.LENGTH_SHORT).show();
                                            imageUploadResponse.setUpload(false);
                                            data.setValue(imageUploadResponse);
                                        }
                                    }
                                })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull @NotNull Exception e) {
                                                Toast.makeText(application, e.getMessage(), Toast.LENGTH_SHORT).show();
                                                imageUploadResponse.setUpload(false);
                                                data.setValue(imageUploadResponse);
                                            }
                                        });
                           }
                       }
                   }
               })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull @NotNull Exception e) {
                        Toast.makeText(application, e.getMessage(), Toast.LENGTH_SHORT).show();
                        imageUploadResponse.setUpload(false);
                        data.setValue(imageUploadResponse);
                    }
                });


        return data;
    }

}
