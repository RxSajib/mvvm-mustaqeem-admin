package com.mustaqeem.cometogetherendproblemadmin.Network.GET;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.mustaqeem.cometogetherendproblemadmin.Model.JobModel;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

public class JobRemoveDetailsGET {

    private Application application;
    private MutableLiveData<JobModel> data;
    private CollectionReference MRemoveRef;

    public JobRemoveDetailsGET(Application application){
        this.application = application;
        MRemoveRef = FirebaseFirestore.getInstance().collection(DataManager.JobRejected);
    }

    public LiveData<JobModel> getRejectedJobDetails(long Key){
        data = new MutableLiveData<>();
        MRemoveRef.document(String.valueOf(Key))
                .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot value, @Nullable FirebaseFirestoreException error) {
                        if(error != null){
                            return;
                        }
                        if(value.exists()){
                            data.setValue(value.toObject(JobModel.class));
                        }else {
                            data.setValue(null);
                        }
                    }
                });

        return data;
    }
}
