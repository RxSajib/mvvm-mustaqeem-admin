package com.mustaqeem.cometogetherendproblemadmin.Network.POST;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

import java.util.HashMap;
import java.util.Map;

public class TeamMemberUpdatePOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference MTeamRef;

    public TeamMemberUpdatePOST(Application application){
        this.application = application;
        MTeamRef = FirebaseFirestore.getInstance().collection(DataManager.TeamRoot);
    }

    public LiveData<Boolean> UpdateTeamMember(String PersonID, String ImageUri, String Name, String Des){
        data = new MutableLiveData<>();


        Map<String, Object> teammap = new HashMap<>();
        teammap.put(DataManager.Member_name, Name);
        teammap.put(DataManager.MemberDesignation, Des);
        teammap.put(DataManager.UpdatePosterPath, ImageUri);

        MTeamRef.document(PersonID)
                .update(teammap)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            data.setValue(true);
                            Toast.makeText(application, "Success", Toast.LENGTH_SHORT).show();
                        }else {
                            data.setValue(false);
                            Toast.makeText(application, "Error "+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                data.setValue(false);
                Toast.makeText(application, "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        return data;
    }
}
