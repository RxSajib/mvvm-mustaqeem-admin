package com.mustaqeem.cometogetherendproblemadmin.Network.GET;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.mustaqeem.cometogetherendproblemadmin.Model.HairModel;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

public class PendingHairDetailsGET {

    private Application application;
    private MutableLiveData<HairModel> data;
    private CollectionReference MHairCollectionRef;

    public PendingHairDetailsGET(Application application){
        this.application = application;
        MHairCollectionRef = FirebaseFirestore.getInstance().collection(DataManager.HireRequest);
    }

    public LiveData<HairModel> getpendinghair_data(long Key){
        data = new MutableLiveData<>();
        MHairCollectionRef.document(String.valueOf(Key))
                .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot value, @Nullable FirebaseFirestoreException error) {
                        if(error != null){
                            data.setValue(null);
                            return;
                        }
                        if(value.exists()){
                            data.setValue(value.toObject(HairModel.class));
                        }else {
                            data.setValue(null);
                        }
                    }
                });

        return data;
    }
}
