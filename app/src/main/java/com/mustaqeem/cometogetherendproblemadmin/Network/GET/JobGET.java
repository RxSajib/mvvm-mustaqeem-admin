package com.mustaqeem.cometogetherendproblemadmin.Network.GET;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.mustaqeem.cometogetherendproblemadmin.Model.JobModel;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

import java.util.List;

public class JobGET {

    private Application application;
    private MutableLiveData<List<JobModel>> data;
    private CollectionReference MJobCollection;


    public JobGET(Application application) {
        this.application = application;
        MJobCollection = FirebaseFirestore.getInstance().collection(DataManager.Job);
    }

    public LiveData<List<JobModel>> getjobdata(long Limit) {
        data = new MutableLiveData<>();
        Query FirebaseSortAss = MJobCollection.orderBy(DataManager.Timestamp, Query.Direction.DESCENDING)
                .limit(Limit);

        FirebaseSortAss
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                        if(error != null){
                            return;
                        }
                        if(!value.isEmpty()){
                            for(DocumentChange ds : value.getDocumentChanges()){
                                if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                                    data.setValue(value.toObjects(JobModel.class));
                                }
                            }
                        }else {
                            data.setValue(null);
                        }
                    }
                });

        return data;
    }
}
