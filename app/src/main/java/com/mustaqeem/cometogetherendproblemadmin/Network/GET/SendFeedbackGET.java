package com.mustaqeem.cometogetherendproblemadmin.Network.GET;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.mustaqeem.cometogetherendproblemadmin.Model.NotificationModel;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

import java.util.List;

public class SendFeedbackGET {

    private Application application;
    private MutableLiveData<List<NotificationModel>> data;
    private CollectionReference MNotifactionRef;

    public SendFeedbackGET(Application application){
        this.application = application;
        MNotifactionRef = FirebaseFirestore.getInstance().collection(DataManager.NotificationRoot);
    }

    public LiveData<List<NotificationModel>> getAdminSendNotifaction(long Limit){
        data = new MutableLiveData<>();
        Query MAdminData = MNotifactionRef.whereEqualTo(DataManager.SendBy, DataManager.Admin).orderBy(DataManager.Timestamp, Query.Direction.DESCENDING)
                .limit(Limit);

        MNotifactionRef
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                        if(error != null){
                            return;
                        }
                        if(value.isEmpty()){
                            data.setValue(null);
                        }else {
                            data.setValue(value.toObjects(NotificationModel.class));
                        }
                    }
                });

        return data;
    }
}
