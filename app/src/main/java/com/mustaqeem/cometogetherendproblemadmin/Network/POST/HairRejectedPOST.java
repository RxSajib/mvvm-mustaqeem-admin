package com.mustaqeem.cometogetherendproblemadmin.Network.POST;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mustaqeem.cometogetherendproblemadmin.Model.HairModel;
import com.mustaqeem.cometogetherendproblemadmin.Response.Response;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

import java.util.HashMap;
import java.util.Map;

public class HairRejectedPOST {

    private Application application;
    private MutableLiveData<Response> data;
    private CollectionReference MRejectedHairRef, MPendingHairRef;
    private Response response;

    public HairRejectedPOST(Application application){
        this.application = application;
        MRejectedHairRef  = FirebaseFirestore.getInstance().collection(DataManager.HairRejected);
        MPendingHairRef = FirebaseFirestore.getInstance().collection(DataManager.HireRequest);
        response = new Response();

    }

    public LiveData<Response> haire_rejectedget(HairModel hairModel){
        data = new MutableLiveData<>();
        if(hairModel != null){
            long Timestamp = System.currentTimeMillis() / 1000;
            String TimestampString = String.valueOf(Timestamp);

            Map<String, Object> postmap = new HashMap<String, Object>();
            postmap.put(DataManager.Name, hairModel.getName());
            postmap.put(DataManager.Location, hairModel.getLocation());
            postmap.put(DataManager.Phone, hairModel.getPhone());
            postmap.put(DataManager.Email, hairModel.getEmail());
            postmap.put(DataManager.Work, hairModel.getWork());
            postmap.put(DataManager.CVLink, hairModel.getCVLink());
            postmap.put(DataManager.CVPosterPath, hairModel.getCVPosterPath());
            postmap.put(DataManager.Timestamp, Timestamp);
            postmap.put(DataManager.UID, hairModel.getUID());

            MRejectedHairRef.document(TimestampString)
                    .set(postmap)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                MPendingHairRef.document(String.valueOf(hairModel.getTimestamp()))
                                        .delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()){
                                            response.setResponse(true);
                                            data.setValue(response);
                                        }else {
                                            response.setResponse(false);
                                            data.setValue(response);
                                            Toast.makeText(application, "Error", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        response.setResponse(false);
                                        data.setValue(response);
                                        Toast.makeText(application, "Error", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }else {
                                response.setResponse(false);
                                data.setValue(response);
                                Toast.makeText(application, "Error", Toast.LENGTH_SHORT).show();
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(application, "Error", Toast.LENGTH_SHORT).show();
                            response.setResponse(false);
                            data.setValue(response);
                        }
                    });
        }
        return data;
    }
}
