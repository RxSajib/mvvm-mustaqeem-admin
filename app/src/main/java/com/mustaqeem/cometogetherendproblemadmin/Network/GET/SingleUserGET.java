package com.mustaqeem.cometogetherendproblemadmin.Network.GET;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.mustaqeem.cometogetherendproblemadmin.Model.UserModel;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

public class SingleUserGET {

    private Application application;
    private MutableLiveData<UserModel> data;
    private CollectionReference MuserDatabase;

    public SingleUserGET(Application application){
        this.application = application;
        MuserDatabase = FirebaseFirestore.getInstance().collection(DataManager.Users);
    }

    public LiveData<UserModel> getsingleuser(String UID){
        data = new MutableLiveData<>();
        MuserDatabase.document(UID)
                .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot value, @Nullable FirebaseFirestoreException error) {
                        if(error != null){
                            return;
                        }
                        if(value.exists()){
                            data.setValue(value.toObject(UserModel.class));
                        }else {
                            data.setValue(null);
                        }
                    }
                });

        return data;
    }
}
