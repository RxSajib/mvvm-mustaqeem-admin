package com.mustaqeem.cometogetherendproblemadmin.Network.GET;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.mustaqeem.cometogetherendproblemadmin.Model.LengthOFCharity;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

public class LengthOFCharityGET {

    private Application application;
    private MutableLiveData<LengthOFCharity> data;
    private CollectionReference MCharityRef;
    private LengthOFCharity lengthOFCharity;

    public LengthOFCharityGET(Application application){
        this.application = application;
        MCharityRef = FirebaseFirestore.getInstance().collection(DataManager.Charity);
        lengthOFCharity = new LengthOFCharity();
    }

    public LiveData<LengthOFCharity> getLengthCharity(){
        data = new MutableLiveData<>();
        MCharityRef.
                addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                        if(error != null){
                            return;
                        }
                        if(!value.isEmpty()){
                            lengthOFCharity.setLength(value.size());
                            data.setValue(lengthOFCharity);
                        }else {
                            lengthOFCharity.setLength(00);
                            data.setValue(lengthOFCharity);
                        }
                    }
                });

        return data;
    }

}
