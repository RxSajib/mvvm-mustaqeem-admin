package com.mustaqeem.cometogetherendproblemadmin.Network.GET;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.mustaqeem.cometogetherendproblemadmin.Model.JobModel;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

import java.util.List;

public class RejectJobGET {

    private Application application;
    private MutableLiveData<List<JobModel>> data;
    private CollectionReference RejectedRef;

    public RejectJobGET(Application application){
        this.application = application;
        RejectedRef = FirebaseFirestore.getInstance().collection(DataManager.JobRejected);
    }

    public LiveData<List<JobModel>> getrejecteddata(long Limit){
        data = new MutableLiveData<>();
        Query FirebaseSortAss = RejectedRef.orderBy(DataManager.Timestamp, Query.Direction.DESCENDING)
                .limit(Limit);

        FirebaseSortAss
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                        if(error != null){
                            return;
                        }
                        if(value.isEmpty()){
                            data.setValue(null);
                        }else {
                            data.setValue(value.toObjects(JobModel.class));
                        }
                    }
                });

        return data;
    }
}
