package com.mustaqeem.cometogetherendproblemadmin.Network.GET;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.mustaqeem.cometogetherendproblemadmin.Model.Charity;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

import java.util.List;

public class CharityGET {

    private Application application;
    private MutableLiveData<List<Charity>> data;
    private FirebaseFirestore McharityRef;
    private CollectionReference MCharityCollection;

    public CharityGET(Application application){
        this.application = application;
        McharityRef = FirebaseFirestore.getInstance();
        MCharityCollection = McharityRef.collection(DataManager.CharityRoot);
    }

    public LiveData<List<Charity>> getcharity(long Limit){
        data = new MutableLiveData<>();
        Query MQry = MCharityCollection.orderBy(DataManager.Timestamp, Query.Direction.DESCENDING).limit(Limit);
        MQry.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                if(error != null){
                    return;
                }
                if(!value.isEmpty()){
                    for(DocumentChange ds : value.getDocumentChanges()){
                        if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                            data.setValue(value.toObjects(Charity.class));
                        }
                    }
                }else {
                    data.setValue(null);
                }
            }
        });

        return data;
    }
}
