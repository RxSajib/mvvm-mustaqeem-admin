package com.mustaqeem.cometogetherendproblemadmin.Network.GET;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.mustaqeem.cometogetherendproblemadmin.Model.LengthOFRejectedJob;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

public class LengthOFRejectedJobGET {

    private Application application;
    private MutableLiveData<LengthOFRejectedJob> data;
    private LengthOFRejectedJob lengthOFRejectedJob;
    private CollectionReference MLengthOFRejectedRef;

    public LengthOFRejectedJobGET(Application application){
        this.application = application;
        lengthOFRejectedJob = new LengthOFRejectedJob();
        MLengthOFRejectedRef = FirebaseFirestore.getInstance().collection(DataManager.JobRejected);
    }

    public LiveData<LengthOFRejectedJob> getLengthOFRejectedJob(){
        data = new MutableLiveData<>();
        MLengthOFRejectedRef
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                        if(error != null){
                            return;
                        }
                        if(value.isEmpty()){
                            lengthOFRejectedJob.setLength(00);
                            data.setValue(lengthOFRejectedJob);
                        }
                        else {
                            lengthOFRejectedJob.setLength(value.size());
                            data.setValue(lengthOFRejectedJob);
                        }
                    }
                });

        return data;
    }
}
