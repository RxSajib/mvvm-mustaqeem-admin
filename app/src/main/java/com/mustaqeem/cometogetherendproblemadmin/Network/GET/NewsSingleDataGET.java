package com.mustaqeem.cometogetherendproblemadmin.Network.GET;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.mustaqeem.cometogetherendproblemadmin.Model.UpdateModel;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

public class NewsSingleDataGET {

    private Application application;
    private MutableLiveData<UpdateModel> data;
    private CollectionReference MNewsRef;


    public NewsSingleDataGET(Application application){
        this.application = application;
        MNewsRef = FirebaseFirestore.getInstance().collection(DataManager.UpdateRoot);
    }

    public LiveData<UpdateModel> NewsSingleData(String NewsID){
        data = new MutableLiveData<>();

        MNewsRef.document(NewsID)
                .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot value, @Nullable FirebaseFirestoreException error) {

                        if(error != null){
                            return;
                        }
                        if(value.exists()){
                            data.setValue(value.toObject(UpdateModel.class));
                        }else {
                            data.setValue(null);
                        }

                    }
                });

        return data;
    }

}
