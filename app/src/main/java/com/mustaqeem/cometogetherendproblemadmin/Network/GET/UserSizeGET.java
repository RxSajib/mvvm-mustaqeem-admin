package com.mustaqeem.cometogetherendproblemadmin.Network.GET;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

public class UserSizeGET {

    private Application application;
    private MutableLiveData<Integer> data;
    private CollectionReference MUserRef;

    public UserSizeGET(Application application){
        this.application = application;
        MUserRef = FirebaseFirestore.getInstance().collection(DataManager.Users);
    }

    public LiveData<Integer> UserSize(){
        data = new MutableLiveData<>();
        MUserRef.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                if(error != null){
                    return;
                }
                if(!value.isEmpty()){
                    data.setValue(value.size());
                }else {
                    data.setValue(null);
                }
            }
        });
        return data;
    }

}
