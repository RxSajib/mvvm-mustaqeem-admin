package com.mustaqeem.cometogetherendproblemadmin.Network.POST;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.mustaqeem.cometogetherendproblemadmin.FCM.FCM_Api;
import com.mustaqeem.cometogetherendproblemadmin.FCM.FcmClint;
import com.mustaqeem.cometogetherendproblemadmin.Model.ResponseCode;
import com.mustaqeem.cometogetherendproblemadmin.Response.FCMResponse;
import com.mustaqeem.cometogetherendproblemadmin.Response.NotifactionResponse;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

import retrofit2.Call;
import retrofit2.Callback;

public class NotifactionAPIPOST {

    private Application application;
    private MutableLiveData<ResponseCode> data;
    private ResponseCode myresponse;
    public FCM_Api api;

    public NotifactionAPIPOST(Application application){
        this.application = application;
        api = new FcmClint().getRetrofit().create(FCM_Api.class);
        myresponse = new ResponseCode();
    }

    public LiveData<ResponseCode> send_notifaction(NotifactionResponse notifactionResponse){

        data = new MutableLiveData<>();
        api.SendNotifaction(notifactionResponse).enqueue(new Callback<FCMResponse>() {
            @Override
            public void onResponse(@Nullable Call<FCMResponse> call,@Nullable retrofit2.Response<FCMResponse> response) {
                if(response.isSuccessful()){
                    myresponse.setCode(DataManager.SuccessCode);
                    data.setValue(myresponse);
                }else {
                    myresponse.setCode(DataManager.ErrorCode);
                    data.setValue(myresponse);
                    Toast.makeText(application, "Error sending", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@Nullable Call<FCMResponse> call,@Nullable Throwable t) {
                Toast.makeText(application, "Error sending", Toast.LENGTH_SHORT).show();
                myresponse.setCode(DataManager.ErrorCode);
                data.setValue(myresponse);
            }
        });
        return data;
    }

}
