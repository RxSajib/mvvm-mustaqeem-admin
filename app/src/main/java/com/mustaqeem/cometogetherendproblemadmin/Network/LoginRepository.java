package com.mustaqeem.cometogetherendproblemadmin.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.mustaqeem.cometogetherendproblemadmin.Model.LoginModel;

import org.jetbrains.annotations.NotNull;

public class LoginRepository {

    private Application application;
    private FirebaseAuth Mauth;
    private MutableLiveData<LoginModel> data;
    private LoginModel loginModel;

    public LoginRepository(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        loginModel = new LoginModel();
    }

    public LiveData<LoginModel> loginaccount(String email, String password){
        data = new MutableLiveData<>();
        if(email.isEmpty()){
            Toast.makeText(application, "Email require", Toast.LENGTH_SHORT).show();
        }
        else if(password.isEmpty()){
            Toast.makeText(application, "Password require", Toast.LENGTH_SHORT).show();
        }
        else {
            Mauth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull @NotNull Task<AuthResult> task) {
                            if(task.isSuccessful()){
                                loginModel.setLogin(true);
                                data.setValue(loginModel);
                                Toast.makeText(application, "Login Success", Toast.LENGTH_SHORT).show();
                            }
                            else {
                                loginModel.setLogin(false);
                                data.setValue(loginModel);
                                Toast.makeText(application, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull @NotNull Exception e) {
                            loginModel.setLogin(false);
                            data.setValue(loginModel);
                            Toast.makeText(application, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
        }

        return data;
    }
}
