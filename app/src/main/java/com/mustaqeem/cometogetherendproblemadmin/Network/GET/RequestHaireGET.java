package com.mustaqeem.cometogetherendproblemadmin.Network.GET;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.mustaqeem.cometogetherendproblemadmin.Model.HairModel;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

import java.util.List;

public class RequestHaireGET {

    private Application application;
    private MutableLiveData<List<HairModel>> data;
    private CollectionReference MHairCollectionRef;

    public RequestHaireGET(Application application) {
        this.application = application;
        MHairCollectionRef = FirebaseFirestore.getInstance().collection(DataManager.HireRequest);
    }

    public LiveData<List<HairModel>> getRequestHairData(long Limit) {
        data = new MutableLiveData<>();
        Query FirebaseQry = MHairCollectionRef.orderBy(DataManager.Timestamp, Query.Direction.DESCENDING).limit(Limit);
        FirebaseQry
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                        if(error != null){
                            return;
                        }
                        if(value.isEmpty()){
                            data.setValue(null);
                        }else {
                            data.setValue(value.toObjects(HairModel.class));
                        }
                    }
                });

        return data;
    }
}
