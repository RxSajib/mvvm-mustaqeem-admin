package com.mustaqeem.cometogetherendproblemadmin.Network.GET;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.mustaqeem.cometogetherendproblemadmin.Model.LengthOfAceptJob;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

public class LengthOfAcceptJobGET {

    private Application application;
    private MutableLiveData<LengthOfAceptJob> data;
    private CollectionReference MAcceptJobCollectionRef;
    private LengthOfAceptJob lengthObject;

    public LengthOfAcceptJobGET(Application application){
        this.application = application;
        MAcceptJobCollectionRef = FirebaseFirestore.getInstance().collection(DataManager.Job);
        lengthObject = new LengthOfAceptJob();
    }

    public LiveData<LengthOfAceptJob> getLengthAcceptJob(){
        data = new MutableLiveData<>();
        MAcceptJobCollectionRef
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                        if(error != null){
                            return;
                        }
                        if(value.isEmpty()){
                            lengthObject.setLength(00);
                            data.setValue(lengthObject);
                        }else {
                            lengthObject.setLength(value.size());
                            data.setValue(lengthObject);
                        }
                    }
                });

        return data;
    }
}
