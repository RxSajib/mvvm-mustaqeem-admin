package com.mustaqeem.cometogetherendproblemadmin.Network.GET;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.mustaqeem.cometogetherendproblemadmin.Model.UsersModel;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

import java.util.List;

public class DeactiveUserGET {

    private Application application;
    private MutableLiveData<List<UsersModel>> data;
    private CollectionReference MuserRef;

    public DeactiveUserGET(Application application){
        this.application = application;
        MuserRef = FirebaseFirestore.getInstance().collection(DataManager.Users);
    }

    public LiveData<List<UsersModel>> getDeactiveUser(boolean Val){

        Query FirebaseQuery = MuserRef.whereEqualTo(DataManager.CardActiveStatus, Val);
        data = new MutableLiveData<>();
        FirebaseQuery.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                if(error != null){
                    return;
                }

                if(!value.isEmpty()){
                    for(DocumentChange ds : value.getDocumentChanges()){
                        if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                            data.setValue(value.toObjects(UsersModel.class));
                        }
                    }
                }else {
                    data.setValue(null);
                }
            }
        });

        return data;
    }
}
