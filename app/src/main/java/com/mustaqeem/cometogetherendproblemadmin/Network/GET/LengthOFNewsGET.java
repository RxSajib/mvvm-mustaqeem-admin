package com.mustaqeem.cometogetherendproblemadmin.Network.GET;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.mustaqeem.cometogetherendproblemadmin.Model.LengthOFNews;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

public class LengthOFNewsGET {

    private Application application;
    private MutableLiveData<LengthOFNews> data;
    private LengthOFNews lengthOFNews;
    private CollectionReference MNewsCollectionRef;

    public LengthOFNewsGET(Application application){
        this.application = application;
        lengthOFNews = new LengthOFNews();
        MNewsCollectionRef = FirebaseFirestore.getInstance().collection(DataManager.UpdateRoot);
    }

    public LiveData<LengthOFNews> getLengthOfNews(){
        data = new MutableLiveData<>();
        MNewsCollectionRef
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                        if(error != null){
                            return;
                        }
                        if(value.isEmpty()){
                            lengthOFNews.setLength(00);
                            data.setValue(lengthOFNews);
                        }else {
                            lengthOFNews.setLength(value.size());
                            data.setValue(lengthOFNews);
                        }
                    }
                });

        return data;
    }
}
