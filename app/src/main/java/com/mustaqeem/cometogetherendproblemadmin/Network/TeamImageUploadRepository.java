package com.mustaqeem.cometogetherendproblemadmin.Network;

import android.app.Application;
import android.net.Uri;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.anstrontechnologies.corehelper.AnstronCoreHelper;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.iceteck.silicompressorr.FileUtils;
import com.iceteck.silicompressorr.SiliCompressor;
import com.mustaqeem.cometogetherendproblemadmin.Model.ImageUploadResponse;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

import org.jetbrains.annotations.NotNull;

import java.io.File;

public class TeamImageUploadRepository {

    private Application application;
    private MutableLiveData<ImageUploadResponse> data;
    private ImageUploadResponse imageuploadresponse;
    private StorageReference MPosterPathRef;
    private AnstronCoreHelper anstronCoreHelper;

    public TeamImageUploadRepository(Application application){
        this.application = application;
        imageuploadresponse = new ImageUploadResponse();
        MPosterPathRef = FirebaseStorage.getInstance().getReference().child(DataManager.TeamStores);
        anstronCoreHelper = new AnstronCoreHelper(application);
    }

    public LiveData<ImageUploadResponse> uploadimage(Uri imageuri){
        data = new MutableLiveData<>();
        File file = new File(SiliCompressor.with(application)
        .compress(FileUtils.getPath(application, imageuri), new File(application.getFilesDir(), "mydemo")));

        Uri imagepath = Uri.fromFile(file);
        MPosterPathRef.child(anstronCoreHelper.getFileNameFromUri(imagepath))
                .putFile(imagepath)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        if(taskSnapshot.getMetadata() != null){
                            if(taskSnapshot.getMetadata().getReference() != null){
                                Task<Uri> result_task = taskSnapshot.getStorage().getDownloadUrl();
                                result_task.addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                        imageuploadresponse.setUpload(true);
                                        imageuploadresponse.setImageUri(uri.toString());
                                        data.setValue(imageuploadresponse);
                                    }
                                })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull @NotNull Exception e) {
                                                Toast.makeText(application, e.getMessage(), Toast.LENGTH_SHORT).show();
                                                imageuploadresponse.setUpload(false);
                                                data.setValue(imageuploadresponse);
                                            }
                                        });
                            }
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull @NotNull Exception e) {
                        Toast.makeText(application, e.getMessage(), Toast.LENGTH_SHORT).show();
                        imageuploadresponse.setUpload(false);
                        data.setValue(imageuploadresponse);
                    }
                });


        return data;




    }
}
