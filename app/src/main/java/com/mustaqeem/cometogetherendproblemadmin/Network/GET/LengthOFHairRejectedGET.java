package com.mustaqeem.cometogetherendproblemadmin.Network.GET;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.mustaqeem.cometogetherendproblemadmin.Model.LengthOFHairRejected;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

public class LengthOFHairRejectedGET {

    private Application application;
    private MutableLiveData<LengthOFHairRejected> data;
    private LengthOFHairRejected lengthOFHairRejected;
    private CollectionReference MHairRejectedRef;

    public LengthOFHairRejectedGET(Application application){
        this.application = application;
        lengthOFHairRejected = new LengthOFHairRejected();
        MHairRejectedRef = FirebaseFirestore.getInstance().collection(DataManager.HairRejected);
    }

    public LiveData<LengthOFHairRejected> getLengthOFHairRejected(){
        data = new MutableLiveData<>();
        MHairRejectedRef
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                        if(error != null){
                            return;
                        }
                        if(value.isEmpty()){
                            lengthOFHairRejected.setLength(00);
                            data.setValue(lengthOFHairRejected);
                        }else {
                            lengthOFHairRejected.setLength(value.size());
                            data.setValue(lengthOFHairRejected);
                        }
                    }
                });


        return data;
    }
}
