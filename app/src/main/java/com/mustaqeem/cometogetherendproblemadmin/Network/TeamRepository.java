package com.mustaqeem.cometogetherendproblemadmin.Network;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.mustaqeem.cometogetherendproblemadmin.Model.TeamModel;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

import java.util.List;

public class TeamRepository {

    private Application application;
    private FirebaseFirestore MTeamFireStore;
    private MutableLiveData<List<TeamModel>> data;

    public TeamRepository(Application application){
        this.application = application;
        MTeamFireStore = FirebaseFirestore.getInstance();
    }

    public LiveData<List<TeamModel>> getteamdata(){
        data = new MutableLiveData<>();

        Query Order = MTeamFireStore.collection(DataManager.TeamRoot).orderBy(DataManager.Timestamp, Query.Direction.DESCENDING);
        Order.addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                        if(error != null){
                            return;
                        }
                        if(!value.isEmpty()){
                            for(DocumentChange ds : value.getDocumentChanges()){
                                switch (ds.getType()){
                                    case ADDED:
                                        data.setValue(value.toObjects(TeamModel.class));
                                        break;

                                    case MODIFIED:
                                        data.setValue(value.toObjects(TeamModel.class));
                                        break;

                                    case REMOVED:
                                        data.setValue(value.toObjects(TeamModel.class));
                                        break;
                                }

                            }
                        }else {
                            data.setValue(null);
                        }
                    }
                });

        return data;
    }
}
