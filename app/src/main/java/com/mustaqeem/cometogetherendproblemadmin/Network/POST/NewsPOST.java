package com.mustaqeem.cometogetherendproblemadmin.Network.POST;

import android.app.Application;
import android.webkit.URLUtil;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mustaqeem.cometogetherendproblemadmin.Model.UpdatePostResponse;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

public class NewsPOST {

    private Application application;
    private MutableLiveData<UpdatePostResponse> data;
    public UpdatePostResponse updatePostResponse;
    private FirebaseFirestore MUpdateDatabase;

    public NewsPOST(Application application) {
        this.application = application;
        updatePostResponse = new UpdatePostResponse();
        MUpdateDatabase = FirebaseFirestore.getInstance();
    }

    public LiveData<UpdatePostResponse> uploadupdate(String Type, String Title, String Details, String Uri, String PosterPath, String AuthorBy) {
        data = new MutableLiveData<>();

            long Timestamp = System.currentTimeMillis() / 1000;
            String TimestampString = String.valueOf(Timestamp);

            Map<String, Object> postmap = new HashMap<>();

            postmap.put(DataManager.UpdateType, Type);
            postmap.put(DataManager.UpdateTitle, Title);
            postmap.put(DataManager.UpdateDetails, Details);
            postmap.put(DataManager.UpdateAuthorBy, AuthorBy);
            if(URLUtil.isValidUrl(Uri)){
                postmap.put(DataManager.UpdateWebUri, Uri);
            }
            postmap.put(DataManager.Timestamp, Timestamp);

            if (PosterPath == null) {
                postmap.put(DataManager.Type, DataManager.Text);
            } else {
                postmap.put(DataManager.Type, DataManager.Image);
                postmap.put(DataManager.UpdatePosterPath, PosterPath);
            }

            MUpdateDatabase.collection(DataManager.UpdateRoot)
                    .document(TimestampString)
                    .set(postmap).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull @NotNull Task<Void> task) {
                    if(task.isSuccessful()){
                        updatePostResponse.setSuccess(true);
                        data.setValue(updatePostResponse);
                    }else {
                        updatePostResponse.setSuccess(false);
                        data.setValue(updatePostResponse);
                        Toast.makeText(application, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull @NotNull Exception e) {
                            updatePostResponse.setSuccess(false);
                            data.setValue(updatePostResponse);
                            Toast.makeText(application, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });

        return data;
    }
}
