package com.mustaqeem.cometogetherendproblemadmin.Network.GET;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

public class TeamSizeGET {

    private Application application;
    private CollectionReference MTeam;
    private MutableLiveData<Integer> data;


    public TeamSizeGET(Application application){
        this.application = application;
        MTeam = FirebaseFirestore.getInstance().collection(DataManager.Team);
    }

    public LiveData<Integer> SizeOFTeam(){
        data = new MutableLiveData<>();
        MTeam.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                if(error != null){
                    return;
                }
                if(value.isEmpty()){
                    data.setValue(00);
                }else {
                    data.setValue(value.size());
                }
            }
        });
        return data;
    }

}
