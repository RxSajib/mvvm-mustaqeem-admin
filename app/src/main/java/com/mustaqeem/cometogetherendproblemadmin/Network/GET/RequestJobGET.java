package com.mustaqeem.cometogetherendproblemadmin.Network.GET;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.mustaqeem.cometogetherendproblemadmin.Model.JobModel;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

import java.util.List;

public class RequestJobGET {

    private Application application;
    private MutableLiveData<List<JobModel>> data;
    private CollectionReference MJobCollection;

    public RequestJobGET(Application application){
        this.application = application;
        MJobCollection = FirebaseFirestore.getInstance().collection(DataManager.JobRoot);
    }

    public LiveData<List<JobModel>> getjondata(long Limit){
        data = new MutableLiveData<>();
        Query FirebaseSortAss = MJobCollection.orderBy(DataManager.Timestamp, Query.Direction.DESCENDING)
                .limit(Limit);

        FirebaseSortAss
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                        if(error != null){
                            return;
                        }
                        if(value.isEmpty()){
                            data.setValue(null);
                        }else {
                            data.setValue(value.toObjects(JobModel.class));
                        }
                    }
                });

        return data;

    }
}
