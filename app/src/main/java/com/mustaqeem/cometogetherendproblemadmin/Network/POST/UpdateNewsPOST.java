package com.mustaqeem.cometogetherendproblemadmin.Network.POST;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

import java.util.HashMap;
import java.util.Map;

public class UpdateNewsPOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference MNewsRef;

    public UpdateNewsPOST(Application application){
        this.application = application;
        MNewsRef = FirebaseFirestore.getInstance().collection(DataManager.UpdateRoot);
    }


    public LiveData<Boolean> UpdateNews(String NewsID, String Type, String Title, String Details, String AuthorBy, String WebUri){
        data = new MutableLiveData<>();

        Map<String, Object> postmap = new HashMap<>();

        postmap.put(DataManager.UpdateType, Type);
        postmap.put(DataManager.UpdateTitle, Title);
        postmap.put(DataManager.UpdateDetails, Details);
        postmap.put(DataManager.UpdateAuthorBy, AuthorBy);
        postmap.put(DataManager.UpdateWebUri, WebUri);

        MNewsRef.document(NewsID)
                .update(postmap)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            data.setValue(true);
                            Toast.makeText(application, "Update Success", Toast.LENGTH_SHORT).show();
                        }else {
                            data.setValue(false);
                            Toast.makeText(application, "Error "+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                data.setValue(false);
                Toast.makeText(application, "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        return data;
    }
}
