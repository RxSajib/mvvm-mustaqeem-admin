package com.mustaqeem.cometogetherendproblemadmin.Network.POST;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

public class DeleteTeamMemberPOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference MTeamMemberRef;


    public DeleteTeamMemberPOST(Application application){
        this.application = application;
        MTeamMemberRef = FirebaseFirestore.getInstance().collection(DataManager.Team);
    }

    public LiveData<Boolean> DeleteTeamMember(String TeamID){
        data = new MutableLiveData<>();

        MTeamMemberRef.document(TeamID)
                .delete().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    data.setValue(true);
                    Toast.makeText(application, "Delete Success", Toast.LENGTH_SHORT).show();
                }else {
                    data.setValue(false);
                    Toast.makeText(application, "Error "+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                data.setValue(false);
                Toast.makeText(application, "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        return data;
    }

}
