package com.mustaqeem.cometogetherendproblemadmin.Network.GET;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.mustaqeem.cometogetherendproblemadmin.Model.LengthOFHair;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

public class LengthOFAcceptHairGET {

    private Application application;
    private MutableLiveData<LengthOFHair> data;
    private LengthOFHair lengthOFHair;
    private CollectionReference MAcceptCharityRef;

    public LengthOFAcceptHairGET(Application application){
        this.application = application;
        lengthOFHair = new LengthOFHair();
        MAcceptCharityRef = FirebaseFirestore.getInstance().collection(DataManager.HairRoot);
    }

    public LiveData<LengthOFHair> getLengthAcceptOFCharity(){
        data = new MutableLiveData<>();
        MAcceptCharityRef
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                        if(error != null){
                            return;
                        }
                        if(value.isEmpty()){
                            lengthOFHair.setLength(00);
                            data.setValue(lengthOFHair);
                        }else {
                            lengthOFHair.setLength(value.size());
                            data.setValue(lengthOFHair);
                        }
                    }
                });
        
        return data;
    }
}
