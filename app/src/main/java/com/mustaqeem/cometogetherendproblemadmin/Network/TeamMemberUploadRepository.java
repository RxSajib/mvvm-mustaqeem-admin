package com.mustaqeem.cometogetherendproblemadmin.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mustaqeem.cometogetherendproblemadmin.Model.UpdatePostResponse;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

import java.util.HashMap;
import java.util.Map;

public class TeamMemberUploadRepository {

    private Application application;
    private MutableLiveData<UpdatePostResponse> data;
    private UpdatePostResponse updatePostResponse;
    private FirebaseFirestore MTeamDatabase;


    public TeamMemberUploadRepository(Application application){
        this.application = application;
        updatePostResponse = new UpdatePostResponse();
        MTeamDatabase = FirebaseFirestore.getInstance();
    }

    public LiveData<UpdatePostResponse> update_team_member(String PosterPath, String Name, String Designation){
        data = new MutableLiveData<>();
        long Timestamp = System.currentTimeMillis() / 1000;

            Map<String, Object> teammap = new HashMap<>();
            teammap.put(DataManager.Member_name, Name);
            teammap.put(DataManager.MemberDesignation, Designation);
            teammap.put(DataManager.Timestamp, Timestamp);
            teammap.put(DataManager.UpdatePosterPath, PosterPath);

            MTeamDatabase.collection(DataManager.TeamRoot)
                    .document(String.valueOf(Timestamp))
                    .set(teammap)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                updatePostResponse.setSuccess(true);
                                data.setValue(updatePostResponse);
                            }else {
                                updatePostResponse.setSuccess(false);
                                data.setValue(updatePostResponse);
                                Toast.makeText(application, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    updatePostResponse.setSuccess(false);
                    data.setValue(updatePostResponse);
                    Toast.makeText(application, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });


        return data;
    }

}
