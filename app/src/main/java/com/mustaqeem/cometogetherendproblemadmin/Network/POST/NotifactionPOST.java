package com.mustaqeem.cometogetherendproblemadmin.Network.POST;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mustaqeem.cometogetherendproblemadmin.Model.ResponseCode;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

import java.util.HashMap;
import java.util.Map;

public class NotifactionPOST {

    private Application application;
    private MutableLiveData<ResponseCode> data;
    private ResponseCode response;
    private CollectionReference MNotifactionRef;
    private CollectionReference MuserInfoRef;

    public NotifactionPOST(Application application){
        this.application = application;
        response = new ResponseCode();
        MNotifactionRef = FirebaseFirestore.getInstance().collection(DataManager.NotificationRoot);
        MuserInfoRef = FirebaseFirestore.getInstance().collection(DataManager.Users);

    }

    public LiveData<ResponseCode> sendnotifaction(String UID, String Message, String Type){
        data = new MutableLiveData<>();
        if(UID != null){

            MuserInfoRef.document(UID).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if(task.isSuccessful()){

                        String ProfileImageUri = task.getResult().getString(DataManager.ProfileImage);
                        String Name = task.getResult().getString(DataManager.Name);
                        String Email = task.getResult().getString(DataManager.Email);
                        String Phone = task.getResult().getString(DataManager.Phone);

                        long Timestamp = System.currentTimeMillis();
                        String TimestampString = String.valueOf(Timestamp);

                        Map<String, Object> map = new HashMap<>();
                        map.put(DataManager.UID, UID);
                        map.put(DataManager.Message, Message);
                        map.put(DataManager.Timestamp, Timestamp);
                        map.put(DataManager.Type, Type);
                        map.put(DataManager.ProfileImageUri, ProfileImageUri);
                        map.put(DataManager.Name, Name);
                        map.put(DataManager.Email, Email);
                        map.put(DataManager.Phone, Phone);
                        map.put(DataManager.SendBy, DataManager.Admin);



                        MNotifactionRef.document(TimestampString).set(map)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()){
                                            response.setCode(DataManager.SuccessCode);
                                            data.setValue(response);

                                        }else {
                                            response.setCode(DataManager.ErrorCode);
                                            data.setValue(response);
                                        }
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(application, "Error", Toast.LENGTH_SHORT).show();
                                response.setCode(DataManager.ErrorCode);
                                data.setValue(response);
                            }
                        });

                    }else {
                        response.setCode(DataManager.ErrorCode);
                        data.setValue(response);
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    response.setCode(DataManager.ErrorCode);
                    data.setValue(response);
                }
            });


        }
        return data;
    }
}
