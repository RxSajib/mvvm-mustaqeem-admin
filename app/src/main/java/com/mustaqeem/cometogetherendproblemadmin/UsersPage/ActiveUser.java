package com.mustaqeem.cometogetherendproblemadmin.UsersPage;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mustaqeem.cometogetherendproblemadmin.R;


public class ActiveUser extends Fragment {

    private RecyclerView recyclerView;

    public ActiveUser() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.active_user, container, false);

        init_view(view);
        return view;
    }

    private void init_view(View view){
        recyclerView = view.findViewById(R.id.UserRecylerViewID);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
    }
}