package com.mustaqeem.cometogetherendproblemadmin.UsersPage;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mustaqeem.cometogetherendproblemadmin.Adapter.UserAdapter;
import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.ViewModel.ViewModel;

public class DeActiveUser extends Fragment {

    private RecyclerView recyclerView;
    private ViewModel viewModel;
    private UserAdapter userAdapter;

    public DeActiveUser() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.de_active_user, container, false);

        Log.d("call", "call");

        init_view(view);
        return view;
    }

    private void init_view(View view){
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        recyclerView = view.findViewById(R.id.DeActiveView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        userAdapter = new UserAdapter();
        recyclerView.setAdapter(userAdapter);
    }

}