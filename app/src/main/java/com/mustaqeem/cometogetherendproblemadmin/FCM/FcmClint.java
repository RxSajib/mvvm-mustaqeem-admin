package com.mustaqeem.cometogetherendproblemadmin.FCM;

import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FcmClint {

    public static Retrofit retrofit = null;
    public Retrofit getRetrofit(){
        if(retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(DataManager.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

}
