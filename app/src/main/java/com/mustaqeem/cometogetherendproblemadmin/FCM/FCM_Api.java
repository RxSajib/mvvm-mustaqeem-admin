package com.mustaqeem.cometogetherendproblemadmin.FCM;

import com.mustaqeem.cometogetherendproblemadmin.Response.FCMResponse;
import com.mustaqeem.cometogetherendproblemadmin.Response.NotifactionResponse;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface FCM_Api {

    @Headers(
            {
                    DataManager.MSG_CONTENT_TYPE+":"+DataManager.MSG_CONTENT_TYPE_VAL,
                    DataManager.MSG_AUTHORIZATION+":"+DataManager.AuthorizationKey
            }
    )

    @POST("fcm/send")
    Call<FCMResponse> SendNotifaction(
            @Body NotifactionResponse response

    );
}
