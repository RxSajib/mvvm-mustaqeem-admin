package com.mustaqeem.cometogetherendproblemadmin.JobPage;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.mustaqeem.cometogetherendproblemadmin.Adapter.JobAdapter;
import com.mustaqeem.cometogetherendproblemadmin.Model.JobModel;
import com.mustaqeem.cometogetherendproblemadmin.Page.AcceptJobDetails;
import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.ViewModel.ViewModel;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;
import com.mustaqeem.cometogetherendproblemadmin.databinding.FragmentAcceptJobBinding;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class AcceptJob extends Fragment {

    private FragmentAcceptJobBinding binding;
    private JobAdapter jobAdapter;
    private ViewModel viewModel;
    private int Limit = 20;

    public AcceptJob() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_accept_job, container, false);


        init_view();
        return binding.getRoot();


    }

    private void init_view(){
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        binding.RecyclerView.setHasFixedSize(true);
        binding.SwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getdata(Limit);
            }
        });
        jobAdapter = new JobAdapter();
        binding.RecyclerView.setAdapter(jobAdapter);

        binding.RecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull @NotNull RecyclerView recyclerView, int dx, int dy) {
                if (!recyclerView.canScrollVertically(1)) {
                    Limit = Limit + 20;
                    getdata(Limit);
                }
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        getdata(Limit);
    }

    private void getdata(int Limit){
        binding.SwipeRefreshLayout.setRefreshing(true);
        viewModel.getallJobs(Limit).observe(getViewLifecycleOwner(), new Observer<List<JobModel>>() {
            @Override
            public void onChanged(List<JobModel> jobModels) {
                if(jobModels != null){
                    binding.MessageText.setVisibility(View.GONE);
                    binding.MessageIcon.setVisibility(View.GONE);
                    binding.ShimmerViewID.setVisibility(View.GONE);
                    jobAdapter.setJobModelList(jobModels);
                    jobAdapter.notifyDataSetChanged();
                    binding.SwipeRefreshLayout.setRefreshing(false);

                    jobAdapter.SetOnClickLisiner(new JobAdapter.SetOnclick() {
                        @Override
                        public void Onclick(long key, String UID) {
                            goto_accepectjob(key, UID);
                        }
                    });
                }
                else {
                    binding.SwipeRefreshLayout.setRefreshing(false);
                    jobAdapter.setJobModelList(jobModels);
                    jobAdapter.notifyDataSetChanged();
                    binding.ShimmerViewID.setVisibility(View.GONE);
                    binding.MessageText.setVisibility(View.VISIBLE);
                    binding.MessageIcon.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void goto_accepectjob(long Key, String UID){
        Intent intent = new Intent(getActivity(), AcceptJobDetails.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(DataManager.Key, Key);
        intent.putExtra(DataManager.UID, UID);
        intent.putExtra(DataManager.Type, DataManager.Accept);
        startActivity(intent);
        Animatoo.animateSlideLeft(getActivity());
    }
}