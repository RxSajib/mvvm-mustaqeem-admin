package com.mustaqeem.cometogetherendproblemadmin.data;

public class DataManager {

    public static final String UpdateImageStores = "Update";
    public static final String Type = "PostType";
    public static final String Text = "Text";
    public static final String Image = "Image";
    public static final String UpdateType  = "Type";
    public static final String UpdateTitle = "Title";
    public static final String UpdateDetails = "Details";
    public static final String UpdateWebUri = "WebUri";
    public static final String UpdateAuthorBy = "AuthorBy";
    public static final String UpdatePosterPath = "PosterPath";
    public static final String UpdateRoot = "Update";
    public static final String Timestamp  = "Timestamp";
    public static final String TeamRoot = "Team";
    public static final String Member_name = "Name";
    public static final String MemberDesignation = "Designation";
    public static final String TeamStores = "Team";
    public static final String Uploading = "Uploading";
    public static final String CharityRoot = "Charity";

    public static final String Home = "Home";
    public static final String News = "News";
    public static final String Charity = "Charity";
    public static final String Team = "Team";
    public static final String Users = "Users";
    public static final String JobRoot = "JobRequest";
    public static final String TimeFormat = "HH:mm";
    public static final String DateFormat = "yyyy-MM-dd";
    public static final String Today = "Today";
    public static final String UAE = "UAE";
    public static final String Par_month = "par_month";

    public static final String BASE_URL = "https://fcm.googleapis.com/";
    public static final String MSG_CONTENT_TYPE = "Content-Type";
    public static final String MSG_CONTENT_TYPE_VAL = "application/json";
    public static final String MSG_AUTHORIZATION  = "Authorization";
    public static final String AuthorizationKey = "key=AAAAeuii8Xg:APA91bG9O-iQuA4Gs1Qp6sAPwRlVNZxVinA4WYpYWaXnsWpaj8gNw_xituTwZC_vBK8x5qxN0SUtRruPQWYdgyyIw_FlKTciXV6EE_XplumfADYu0J0y9NSXkjlRo-rLaOwPdUBnWtNa";
    public static final String Key = "Key";
    public static final String AED = "AED";
    public static final String Job = "Job";

    public static final String JobType = "JobType";
    public static final String StartSalary = "StartSalary";
    public static final String EndSalary = "EndSalary";
    public static final String Experience = "Experience";
    public static final String Rewards = "Rewards";
    public static final String Location = "Location";
    public static final String ContactInfo = "ContactInfo";
    public static final String Industry = "Industry";
    public static final String UID = "UID";
    public static final String Contract = "Contract";
    public static final String JobRejected = "RejectJobs";
    public static final String Rejected = "Rejected";
    public static final String Accept = "Accept";
    public static final String HireRequest = "HireRequest";
    public static final String HairRoot = "Hairs";
    public static final String HairRejected = "HairRejected";
    public static final String Pending = "Pending";
    public static final String Name = "Name";
    public static final String Designation = "Designation";
    public static final String NameUri = "NameUri";
    public static final String Message = "Message";
    public static final String Time = "Time";
    public static final String Phone = "Phone";
    public static final String Email = "Email";
    public static final String Work = "Work";
    public static final String CVLink = "CVLink";
    public static final String CVPosterPath = "CVPosterPath";
    public static final String NotificationRoot = "Notification";
    public static final String ProfileImageUri = "ProfileImageUri";
    public static final String Notification = "Notification";
    public static final String ProfileImage = "ProfileImage";
    public static final String SendBy = "SendBy";
    public static final String Admin = "Admin";
    public static final String User = "User";
    public static final String Information  = "Information";
    public static final String Success = "Success";


    public static final int SuccessCode = 200;
    public static final int ErrorCode = 201;
    public static final int DefaultCode = 203;

    public static final String All_Users = "All Users";
    public static final String Our_Team = "Our Team";
    public static final String Feedback = "Feedback";
    public static final String Support = "Support";
    public static final String Data = "Data";
    public static final String CardActiveStatus = "CardActiveStatus";
    public static final String Month = "MM";
    public static final String Day = "dd";
    public static final String Year = "yyyy";

    public static final String Birthday = "Birthday";
    public static final String CardNumber = "CardNumber";
    public static final String DateOFExp = "DateOFExp";
    public static final String DateOfIssue = "DateOfIssue";
    public static final String Currency = "Currency";

    public static final String Pakistan_Rupee = "Pakistan Rupee";
    public static final String India_Rupee = "India Rupee";
    public static final String BDT = "BDT";
    public static final String Saudi_Riyal = "Saudi Riyal";

    public static final String EN = "en";
    public static final String IN = "IN";
    public static final String PK = "PK";
    public static final String AE = "AE";
    public static final String BD = "BD";
    public static final String SA = "SA";

    public static final String SuccessUri = "https://cdn-icons-png.flaticon.com/512/845/845646.png";
    public static final String ErrorIconUri = "https://cdn-icons-png.flaticon.com/512/6659/6659895.png";
}
