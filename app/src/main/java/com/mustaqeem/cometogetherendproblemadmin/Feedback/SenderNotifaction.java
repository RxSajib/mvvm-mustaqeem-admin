package com.mustaqeem.cometogetherendproblemadmin.Feedback;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mustaqeem.cometogetherendproblemadmin.Adapter.SendNotificationAdapter;
import com.mustaqeem.cometogetherendproblemadmin.Model.NotificationModel;
import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.ViewModel.ViewModel;
import com.mustaqeem.cometogetherendproblemadmin.databinding.FragmentSenderNotifactionBinding;

import java.util.List;


public class SenderNotifaction extends Fragment {

    private FragmentSenderNotifactionBinding binding;
    private SendNotificationAdapter adapter;
    private ViewModel viewModel;

    public SenderNotifaction() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sender_notifaction, container, false);

        init_view();
        return binding.getRoot();
    }

    private void init_view(){
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        binding.RecyclerView.setHasFixedSize(true);
        adapter = new SendNotificationAdapter();
        binding.RecyclerView.setAdapter(adapter);

        getdata_fromserver();
    }

    private void getdata_fromserver(){
        viewModel.get_adminFeedback(10).observe(getViewLifecycleOwner(), new Observer<List<NotificationModel>>() {
            @Override
            public void onChanged(List<NotificationModel> notificationModels) {
                if(notificationModels != null){
                    adapter.setNotificationModelList(notificationModels);
                    adapter.notifyDataSetChanged();


                }else {
                    binding.FeedbackMessage.setVisibility(View.VISIBLE);
                }
            }
        });
    }
}