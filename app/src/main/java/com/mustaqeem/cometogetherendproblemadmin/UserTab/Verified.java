package com.mustaqeem.cometogetherendproblemadmin.UserTab;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mustaqeem.cometogetherendproblemadmin.Adapter.VerifiedUsersAdapter;
import com.mustaqeem.cometogetherendproblemadmin.Model.UsersModel;
import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.ViewModel.ViewModel;
import com.mustaqeem.cometogetherendproblemadmin.databinding.VerifiedBinding;

import java.util.List;


public class Verified extends Fragment {

    private VerifiedBinding binding;
    private VerifiedUsersAdapter adapter;
    private ViewModel viewModel;

    public Verified() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.verified, container, false);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        getdata_fromserver();
        return binding.getRoot();
    }

    private void getdata_fromserver(){
        adapter = new VerifiedUsersAdapter();
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(adapter);

        viewModel.getDeactiveUser(true)
                .observe(getActivity(), new Observer<List<UsersModel>>() {
                    @Override
                    public void onChanged(List<UsersModel> usersModels) {
                        if(usersModels != null){
                            binding.MessageIcon.setVisibility(View.GONE);
                            binding.MessageText.setVisibility(View.GONE);
                            adapter.setList(usersModels);
                            adapter.notifyDataSetChanged();

                        }else {
                            binding.MessageIcon.setVisibility(View.VISIBLE);
                            binding.MessageText.setVisibility(View.VISIBLE);
                            adapter.setList(usersModels);
                            adapter.notifyDataSetChanged();
                        }
                    }
                });
    }

}