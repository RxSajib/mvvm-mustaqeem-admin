package com.mustaqeem.cometogetherendproblemadmin.UserTab;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.gmail.samehadar.iosdialog.IOSDialog;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.mustaqeem.cometogetherendproblemadmin.Adapter.UserAdapter;
import com.mustaqeem.cometogetherendproblemadmin.Model.Data;
import com.mustaqeem.cometogetherendproblemadmin.Model.Priority;
import com.mustaqeem.cometogetherendproblemadmin.Model.ResponseCode;
import com.mustaqeem.cometogetherendproblemadmin.Model.UsersModel;
import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.Response.NotifactionResponse;
import com.mustaqeem.cometogetherendproblemadmin.ViewModel.ViewModel;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;
import com.mustaqeem.cometogetherendproblemadmin.databinding.CarddialogBinding;
import com.mustaqeem.cometogetherendproblemadmin.databinding.PendingBinding;
import com.suke.widget.SwitchButton;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Pending extends Fragment {

    private PendingBinding binding;
    private ViewModel viewModel;
    private UserAdapter userAdapter;
    private IOSDialog iosDialog;

    public Pending() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.pending, container, false);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        sendnotification();
        getdata_server();
        return binding.getRoot();
    }

    private void sendnotification(){
        Data data = new Data("congraluction your request is successfully verified", "hello Sajib", DataManager.SuccessUri);
        Priority priority = new Priority();
        priority.setPriority("high");
        NotifactionResponse response = new NotifactionResponse(data, "fvjRfaZ_Q-K01h2f5wShtV:APA91bE4_PN9BbapMCM0N8PZ9mom2xzqKzPjgDQrZsTpjoItlLZ0fw3Ba_3yh1_byqib3brK7l4PKtpEg7Pyzs6ZJUT5SQt5ZMGzGzHzAPkOE7fwWq4EiAoth_SQAb5_xIEViUAOKOZC", priority);
        viewModel.sending_notifaction(response).observe(getActivity(), new Observer<ResponseCode>() {
            @Override
            public void onChanged(ResponseCode responseCode) {
                Toast.makeText(getActivity(), "send", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getdata_server() {
        userAdapter = new UserAdapter();
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(userAdapter);
        viewModel.getDeactiveUser(false).observe(getViewLifecycleOwner(), new Observer<List<UsersModel>>() {
            @Override
            public void onChanged(List<UsersModel> userModels) {

                if (userModels != null) {
                    userAdapter.setUserModelList(userModels);
                    userAdapter.notifyDataSetChanged();
                    userAdapter.SetOnClickLisiner(new UserAdapter.Click() {
                        @RequiresApi(api = Build.VERSION_CODES.O)
                        @Override
                        public void Click(String Name, String UID) {
                            open_dialog(Name, UID);
                        }
                    });

                    binding.MessageIcon.setVisibility(View.GONE);
                    binding.MessageText.setVisibility(View.GONE);
                } else {
                    userAdapter.setUserModelList(userModels);
                    userAdapter.notifyDataSetChanged();

                    binding.MessageText.setVisibility(View.VISIBLE);
                    binding.MessageIcon.setVisibility(View.VISIBLE);
                }

            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void open_dialog(String Name, String UID) {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getActivity());
        CarddialogBinding binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.carddialog, null, false);
        bottomSheetDialog.setContentView(binding.getRoot());
        bottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        bottomSheetDialog.show();

        long Timestamp = System.currentTimeMillis();
        binding.OwnerName.setText(Name);
        Random rnd = new Random();
        int IDNumber = 1000000000 + rnd.nextInt(999999999);

        String IDNumberString = String.valueOf(IDNumber);

        String FirstThree = IDNumberString.substring(0, 3);
        String SecondThree = IDNumberString.substring(3, 6);
        String ThirdNumber = IDNumberString.substring(6, 9);

        binding.CardIDNumberFirst.setText("PAK");
        binding.CardIDSecond.setText(FirstThree);
        binding.CardIDThired.setText(SecondThree);
        binding.CardIDFour.setText(ThirdNumber);

        binding.Refresh.setOnClickListener(view -> {
            binding.OwnerName.setText(Name);
            Random random = new Random();
            int IDNumberRnd = 1000000000 + random.nextInt(999999999);

            String IDNumberStringRnd = String.valueOf(IDNumberRnd);

            String FirstThreeRnd = IDNumberStringRnd.substring(0, 3);
            String SecondThreeRnd = IDNumberStringRnd.substring(3, 6);
            String ThirdNumberRnd = IDNumberStringRnd.substring(6, 9);

            binding.CardIDNumberFirst.setText("PAK");
            binding.CardIDSecond.setText(FirstThreeRnd);
            binding.CardIDThired.setText(SecondThreeRnd);
            binding.CardIDFour.setText(ThirdNumberRnd);
        });
        binding.SwitchButton.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                if (isChecked) {
                    binding.ActiveButton.setVisibility(View.VISIBLE);
                } else {
                    binding.ActiveButton.setVisibility(View.GONE);
                }
            }
        });

        Calendar calendardate = Calendar.getInstance(Locale.ENGLISH);
        calendardate.setTimeInMillis(Timestamp);

        String CurrentMonth = DateFormat.format(DataManager.Month, calendardate).toString();
        String CurrentDay  = DateFormat.format(DataManager.Day, calendardate).toString();
        String CurrentYear = DateFormat.format(DataManager.Year, calendardate).toString();

        binding.IssueDate.setText(CurrentDay+"/"+CurrentMonth+"/"+CurrentYear);


        long AfterOneYear = Timestamp + TimeUnit.DAYS.toHours(8760);
        Calendar calendaryear = Calendar.getInstance(Locale.ENGLISH);
        calendaryear.setTimeInMillis(AfterOneYear);

        String NextYear = DateFormat.format(DataManager.Year, calendaryear).toString();
        Log.d("NextYear" , NextYear);

        String input = CurrentDay+"/"+CurrentMonth+"/"+CurrentYear;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern (DataManager.Day+"/"+DataManager.Month+"/"+DataManager.Year);
        LocalDate localDate = LocalDate.parse( input , formatter );
        LocalDate yearLater = localDate.plusYears (1);

        binding.ExpDate.setText(yearLater.toString());


        binding.ActiveButton.setOnClickListener(view -> {
            bottomSheetDialog.dismiss();
            progressdialoag("Card Activating");
            viewModel.Verification(UID, binding.ExpDate.getText().toString(), binding.IssueDate.getText().toString(), true, binding.CardIDNumberFirst.getText().toString() + binding.CardIDSecond.getText().toString() + binding.CardIDThired.getText().toString() + binding.CardIDFour.getText().toString())
            .observe(this, new Observer<Boolean>() {
                        @Override
                        public void onChanged(Boolean aBoolean) {
                            if(aBoolean){
                                iosDialog.dismiss();
                            }else {
                                iosDialog.dismiss();
                            }
                        }
                    });
        });
    }
    private void progressdialoag(String Message) {
        iosDialog = new IOSDialog.Builder(getActivity())
                .setTitle("loading")
                .setDimAmount(3)
                .setSpinnerDuration(120)
                .setMessageContentGravity(Gravity.END)
                .setCancelable(false)
                .setMessageContent(Message)
                .build();

        iosDialog.show();
    }
}