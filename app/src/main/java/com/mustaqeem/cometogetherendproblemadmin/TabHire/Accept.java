package com.mustaqeem.cometogetherendproblemadmin.TabHire;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.mustaqeem.cometogetherendproblemadmin.Adapter.HairAdapter;
import com.mustaqeem.cometogetherendproblemadmin.Model.HairModel;
import com.mustaqeem.cometogetherendproblemadmin.Page.AcceptHairPage;
import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.ViewModel.ViewModel;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;
import com.mustaqeem.cometogetherendproblemadmin.databinding.FragmentAcceptPageBinding;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class Accept extends Fragment {

    private FragmentAcceptPageBinding binding;
    private HairAdapter hairAdapter;
    private ViewModel viewModel;
    private int Limit = 20;

    public Accept() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_accept_page, container, false);

        init_view();

        return binding.getRoot();
    }

    private void init_view(){
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        binding.RecyclerView.setHasFixedSize(true);
        hairAdapter = new HairAdapter();
        binding.RecyclerView.setAdapter(hairAdapter);

        binding.SwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getdata(Limit);
            }
        });
        binding.RecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull @NotNull RecyclerView recyclerView, int dx, int dy) {
                if (!recyclerView.canScrollVertically(1)) {
                    Limit = Limit + 20;
                    getdata(Limit);
                }
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        getdata(Limit);
    }

    private void getdata(int Limit){
        viewModel.getAcceptHairdata(Limit).observe(getViewLifecycleOwner(), new Observer<List<HairModel>>() {
            @Override
            public void onChanged(List<HairModel> hairModels) {
                if(hairModels != null){
                    binding.SwipeRefreshLayout.setRefreshing(false);
                    binding.MessageText.setVisibility(View.GONE);
                    binding.MessageIcon.setVisibility(View.GONE);
                    binding.ShimmerViewID.setVisibility(View.GONE);
                    hairAdapter.setHairModelList(hairModels);
                    hairAdapter.notifyDataSetChanged();

                    hairAdapter.SetOnclickLisiner(new HairAdapter.SetOnclick() {
                        @Override
                        public void Click(long Timestamp) {
                            goto_detailspage(Timestamp);
                        }
                    });

                }else {
                    binding.SwipeRefreshLayout.setRefreshing(false);
                    hairAdapter.setHairModelList(hairModels);
                    hairAdapter.notifyDataSetChanged();
                    binding.ShimmerViewID.setVisibility(View.GONE);
                    binding.MessageIcon.setVisibility(View.VISIBLE);
                    binding.MessageText.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void goto_detailspage(long Timestamp){
        Intent intent = new Intent(getActivity(), AcceptHairPage.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(DataManager.Type, DataManager.Accept);
        intent.putExtra(DataManager.Key, Timestamp);
        startActivity(intent);
        Animatoo.animateSlideLeft(getActivity());
    }


}