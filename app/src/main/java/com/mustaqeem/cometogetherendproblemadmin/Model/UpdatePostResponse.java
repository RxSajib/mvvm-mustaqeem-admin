package com.mustaqeem.cometogetherendproblemadmin.Model;

public class UpdatePostResponse {

    private boolean IsSuccess;

    public boolean isSuccess() {
        return IsSuccess;
    }

    public void setSuccess(boolean success) {
        IsSuccess = success;
    }
}
