package com.mustaqeem.cometogetherendproblemadmin.Model;

public class ImageUploadResponse {

    private String ImageUri;
    private boolean IsUpload;

    public String getImageUri() {
        return ImageUri;
    }

    public void setImageUri(String imageUri) {
        ImageUri = imageUri;
    }

    public boolean isUpload() {
        return IsUpload;
    }

    public void setUpload(boolean upload) {
        IsUpload = upload;
    }
}
