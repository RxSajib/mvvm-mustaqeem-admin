package com.mustaqeem.cometogetherendproblemadmin.Model;

import com.google.gson.annotations.SerializedName;

public class Priority {

    @SerializedName("priority")
    private String priority;


    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }
}
