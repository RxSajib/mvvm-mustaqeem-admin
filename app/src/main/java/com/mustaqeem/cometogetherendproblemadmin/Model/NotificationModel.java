package com.mustaqeem.cometogetherendproblemadmin.Model;

public class NotificationModel {

    private String Email;
    private String Message;
    private String Phone;
    private String PostType;
    private String ProfileImageUri;
    private String SendBy;
    private long Timestamp;
    private String UID;


    public NotificationModel(){

    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getPostType() {
        return PostType;
    }

    public void setPostType(String postType) {
        PostType = postType;
    }

    public String getProfileImageUri() {
        return ProfileImageUri;
    }

    public void setProfileImageUri(String profileImageUri) {
        ProfileImageUri = profileImageUri;
    }

    public String getSendBy() {
        return SendBy;
    }

    public void setSendBy(String sendBy) {
        SendBy = sendBy;
    }

    public long getTimestamp() {
        return Timestamp;
    }

    public void setTimestamp(long timestamp) {
        Timestamp = timestamp;
    }

    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }
}
