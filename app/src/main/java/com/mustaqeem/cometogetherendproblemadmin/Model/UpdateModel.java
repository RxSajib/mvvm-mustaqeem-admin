package com.mustaqeem.cometogetherendproblemadmin.Model;

public class UpdateModel {

    private String Type, PostType, Title, Details, WebUri, AuthorBy, PosterPath;
    private long Timestamp;

    public UpdateModel(){

    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getPostType() {
        return PostType;
    }

    public void setPostType(String postType) {
        PostType = postType;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDetails() {
        return Details;
    }

    public void setDetails(String details) {
        Details = details;
    }

    public String getWebUri() {
        return WebUri;
    }

    public void setWebUri(String webUri) {
        WebUri = webUri;
    }

    public String getAuthorBy() {
        return AuthorBy;
    }

    public void setAuthorBy(String authorBy) {
        AuthorBy = authorBy;
    }

    public String getPosterPath() {
        return PosterPath;
    }

    public void setPosterPath(String posterPath) {
        PosterPath = posterPath;
    }

    public long getTimestamp() {
        return Timestamp;
    }

    public void setTimestamp(long timestamp) {
        Timestamp = timestamp;
    }
}
