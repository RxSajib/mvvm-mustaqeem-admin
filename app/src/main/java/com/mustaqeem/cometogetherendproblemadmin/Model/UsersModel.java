package com.mustaqeem.cometogetherendproblemadmin.Model;

public class UsersModel {

    private String Email;
    private String Name, Phone, ProfileImage;
    private String UID;
    private String Birthday, CardNumber, DateOFExp, DateOfIssue, Gender;
    private String Location, Owner, Token;
    private long Timestamp;
    private Boolean CardActiveStatus;

    public UsersModel() {
    }

    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getProfileImage() {
        return ProfileImage;
    }

    public void setProfileImage(String profileImage) {
        ProfileImage = profileImage;
    }

    public String getBirthday() {
        return Birthday;
    }

    public void setBirthday(String birthday) {
        Birthday = birthday;
    }

    public String getCardNumber() {
        return CardNumber;
    }

    public void setCardNumber(String cardNumber) {
        CardNumber = cardNumber;
    }

    public String getDateOFExp() {
        return DateOFExp;
    }

    public void setDateOFExp(String dateOFExp) {
        DateOFExp = dateOFExp;
    }

    public String getDateOfIssue() {
        return DateOfIssue;
    }

    public void setDateOfIssue(String dateOfIssue) {
        DateOfIssue = dateOfIssue;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getOwner() {
        return Owner;
    }

    public void setOwner(String owner) {
        Owner = owner;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public long getTimestamp() {
        return Timestamp;
    }

    public void setTimestamp(long timestamp) {
        Timestamp = timestamp;
    }

    public Boolean getCardActiveStatus() {
        return CardActiveStatus;
    }

    public void setCardActiveStatus(Boolean cardActiveStatus) {
        CardActiveStatus = cardActiveStatus;
    }
}
