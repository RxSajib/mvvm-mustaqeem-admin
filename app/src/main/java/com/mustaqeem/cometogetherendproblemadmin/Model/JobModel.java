package com.mustaqeem.cometogetherendproblemadmin.Model;

public class JobModel {

    private String JobType;
    private String StartSalary;
    private String EndSalary;
    private String Experience;
    private String Rewards;
    private String Location;
    private String Contract;
    private String Industry;
    private String UID;
    private long Timestamp;
    private String Currency;

    private long AcceptJobSize;


    public JobModel(){

    }

    public String getCurrency() {
        return Currency;
    }

    public void setCurrency(String currency) {
        Currency = currency;
    }

    public String getJobType() {
        return JobType;
    }

    public void setJobType(String jobType) {
        JobType = jobType;
    }

    public String getStartSalary() {
        return StartSalary;
    }

    public void setStartSalary(String startSalary) {
        StartSalary = startSalary;
    }

    public String getEndSalary() {
        return EndSalary;
    }

    public void setEndSalary(String endSalary) {
        EndSalary = endSalary;
    }

    public String getExperience() {
        return Experience;
    }

    public void setExperience(String experience) {
        Experience = experience;
    }

    public String getRewards() {
        return Rewards;
    }

    public void setRewards(String rewards) {
        Rewards = rewards;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getContract() {
        return Contract;
    }

    public void setContract(String contract) {
        Contract = contract;
    }

    public String getIndustry() {
        return Industry;
    }

    public void setIndustry(String industry) {
        Industry = industry;
    }

    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }

    public long getTimestamp() {
        return Timestamp;
    }

    public void setTimestamp(long timestamp) {
        Timestamp = timestamp;
    }


    public long getAcceptJobSize() {
        return AcceptJobSize;
    }

    public void setAcceptJobSize(long acceptJobSize) {
        AcceptJobSize = acceptJobSize;
    }
}
