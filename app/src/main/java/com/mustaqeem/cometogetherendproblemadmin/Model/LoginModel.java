package com.mustaqeem.cometogetherendproblemadmin.Model;

public class LoginModel {

    private boolean isLogin;

    public boolean isLogin() {
        return isLogin;
    }

    public void setLogin(boolean login) {
        isLogin = login;
    }
}
