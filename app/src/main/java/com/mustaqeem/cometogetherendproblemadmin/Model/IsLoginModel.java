package com.mustaqeem.cometogetherendproblemadmin.Model;

public class IsLoginModel {

    private boolean IsLogin;
    private String FirebaseAuth;

    public boolean isLogin() {
        return IsLogin;
    }

    public void setLogin(boolean login) {
        IsLogin = login;
    }

    public String getFirebaseAuth() {
        return FirebaseAuth;
    }

    public void setFirebaseAuth(String firebaseAuth) {
        FirebaseAuth = firebaseAuth;
    }
}
