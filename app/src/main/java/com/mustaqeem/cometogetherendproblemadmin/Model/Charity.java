package com.mustaqeem.cometogetherendproblemadmin.Model;

public class Charity {

    private String DownloadUri, Location, Name, Phone;
    private long Timestamp;
    private String UID;

    public Charity(){

    }

    public String getDownloadUri() {
        return DownloadUri;
    }

    public void setDownloadUri(String downloadUri) {
        DownloadUri = downloadUri;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public long getTimestamp() {
        return Timestamp;
    }

    public void setTimestamp(long timestamp) {
        Timestamp = timestamp;
    }

    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }
}
