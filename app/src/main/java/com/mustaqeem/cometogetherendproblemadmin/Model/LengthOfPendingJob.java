package com.mustaqeem.cometogetherendproblemadmin.Model;

public class LengthOfPendingJob {

    private int length;

    public LengthOfPendingJob(){

    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}
