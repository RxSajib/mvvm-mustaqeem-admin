package com.mustaqeem.cometogetherendproblemadmin.Model;

public class NavigationIconModel {

    private int Icon;
    private String Title;
    private String Size;

    public NavigationIconModel(int icon, String title) {
        Icon = icon;
        Title = title;
    }

    public NavigationIconModel(int icon, String title, String size) {
        Icon = icon;
        Title = title;
        Size = size;
    }

    public String getSize() {
        return Size;
    }

    public void setSize(String size) {
        Size = size;
    }

    public int getIcon() {
        return Icon;
    }

    public void setIcon(int icon) {
        Icon = icon;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }
}
