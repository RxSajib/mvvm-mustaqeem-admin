package com.mustaqeem.cometogetherendproblemadmin.Model;

public class Data {

    private String body;
    private String title;
    private String image;

    public Data(){}

    public Data(String body, String title, String image) {
        this.body = body;
        this.title = title;
        this.image = image;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
