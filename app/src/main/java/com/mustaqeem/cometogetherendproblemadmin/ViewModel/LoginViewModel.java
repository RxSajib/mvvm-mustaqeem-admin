package com.mustaqeem.cometogetherendproblemadmin.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.mustaqeem.cometogetherendproblemadmin.Model.IsLoginModel;
import com.mustaqeem.cometogetherendproblemadmin.Model.LoginModel;
import com.mustaqeem.cometogetherendproblemadmin.Network.IsLoginRepository;
import com.mustaqeem.cometogetherendproblemadmin.Network.LoginRepository;

import org.jetbrains.annotations.NotNull;

public class LoginViewModel extends AndroidViewModel {

    private LoginRepository loginRepository;
    private IsLoginRepository isLoginRepository;

    public LoginViewModel(@NonNull @NotNull Application application) {
        super(application);
        loginRepository = new LoginRepository(application);
        isLoginRepository = new IsLoginRepository(application);
    }

    public LiveData<LoginModel> setloginaccount(String email, String password){
      return loginRepository.loginaccount(email, password);
    }

    public LiveData<IsLoginModel> isloginaccount(){
        return isLoginRepository.islogin();
    }
}
