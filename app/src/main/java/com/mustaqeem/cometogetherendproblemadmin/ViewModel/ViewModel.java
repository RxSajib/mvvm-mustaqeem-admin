package com.mustaqeem.cometogetherendproblemadmin.ViewModel;

import android.app.Application;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.mustaqeem.cometogetherendproblemadmin.Model.Charity;
import com.mustaqeem.cometogetherendproblemadmin.Model.HairModel;
import com.mustaqeem.cometogetherendproblemadmin.Model.ImageUploadResponse;
import com.mustaqeem.cometogetherendproblemadmin.Model.JobModel;
import com.mustaqeem.cometogetherendproblemadmin.Model.LengthOFCharity;
import com.mustaqeem.cometogetherendproblemadmin.Model.LengthOFHair;
import com.mustaqeem.cometogetherendproblemadmin.Model.LengthOFHairRejected;
import com.mustaqeem.cometogetherendproblemadmin.Model.LengthOFNews;
import com.mustaqeem.cometogetherendproblemadmin.Model.LengthOFPendingHair;
import com.mustaqeem.cometogetherendproblemadmin.Model.LengthOFRejectedJob;
import com.mustaqeem.cometogetherendproblemadmin.Model.LengthOfAceptJob;
import com.mustaqeem.cometogetherendproblemadmin.Model.LengthOfPendingJob;
import com.mustaqeem.cometogetherendproblemadmin.Model.NotificationModel;
import com.mustaqeem.cometogetherendproblemadmin.Model.ResponseCode;
import com.mustaqeem.cometogetherendproblemadmin.Model.TeamModel;
import com.mustaqeem.cometogetherendproblemadmin.Model.UpdateModel;
import com.mustaqeem.cometogetherendproblemadmin.Model.UpdatePostResponse;
import com.mustaqeem.cometogetherendproblemadmin.Model.UserModel;
import com.mustaqeem.cometogetherendproblemadmin.Model.UsersModel;
import com.mustaqeem.cometogetherendproblemadmin.Network.GET.AcceptHairDetailsGET;
import com.mustaqeem.cometogetherendproblemadmin.Network.GET.AcceptHairGET;
import com.mustaqeem.cometogetherendproblemadmin.Network.GET.NewsSingleDataGET;
import com.mustaqeem.cometogetherendproblemadmin.Network.GET.UserSizeGET;
import com.mustaqeem.cometogetherendproblemadmin.Network.POST.AcceptHairPOST;
import com.mustaqeem.cometogetherendproblemadmin.Network.GET.AcceptJobDetailsGET;
import com.mustaqeem.cometogetherendproblemadmin.Network.POST.AcceptPOST;
import com.mustaqeem.cometogetherendproblemadmin.Network.GET.AccpetHiredataGET;
import com.mustaqeem.cometogetherendproblemadmin.Network.GET.CharityGET;
import com.mustaqeem.cometogetherendproblemadmin.Network.GET.DeactiveUserGET;
import com.mustaqeem.cometogetherendproblemadmin.Network.POST.CharutyDeletePOST;
import com.mustaqeem.cometogetherendproblemadmin.Network.POST.DeleteNewsPOST;
import com.mustaqeem.cometogetherendproblemadmin.Network.POST.DeleteTeamMemberPOST;
import com.mustaqeem.cometogetherendproblemadmin.Network.GET.HairRejectedDetailsGET;
import com.mustaqeem.cometogetherendproblemadmin.Network.POST.ForgotPasswordPOST;
import com.mustaqeem.cometogetherendproblemadmin.Network.POST.HairRejectedPOST;
import com.mustaqeem.cometogetherendproblemadmin.Network.GET.JobPendingDetailsGET;
import com.mustaqeem.cometogetherendproblemadmin.Network.GET.JobGET;
import com.mustaqeem.cometogetherendproblemadmin.Network.GET.JobRemoveDetailsGET;
import com.mustaqeem.cometogetherendproblemadmin.Network.GET.LengthOFAcceptHairGET;
import com.mustaqeem.cometogetherendproblemadmin.Network.GET.LengthOFCharityGET;
import com.mustaqeem.cometogetherendproblemadmin.Network.GET.LengthOFHairRejectedGET;
import com.mustaqeem.cometogetherendproblemadmin.Network.GET.LengthOFJobPendingGET;
import com.mustaqeem.cometogetherendproblemadmin.Network.GET.LengthOFNewsGET;
import com.mustaqeem.cometogetherendproblemadmin.Network.GET.LengthOFPendingHairGET;
import com.mustaqeem.cometogetherendproblemadmin.Network.GET.LengthOFRejectedJobGET;
import com.mustaqeem.cometogetherendproblemadmin.Network.GET.LengthOfAcceptJobGET;
import com.mustaqeem.cometogetherendproblemadmin.Network.GET.NewsSizeGET;
import com.mustaqeem.cometogetherendproblemadmin.Network.POST.LogOutAccountPOST;
import com.mustaqeem.cometogetherendproblemadmin.Network.POST.NotifactionPOST;
import com.mustaqeem.cometogetherendproblemadmin.Network.GET.NotifactionReceivedGET;
import com.mustaqeem.cometogetherendproblemadmin.Network.GET.PendingHairDetailsGET;
import com.mustaqeem.cometogetherendproblemadmin.Network.GET.RejectJobGET;
import com.mustaqeem.cometogetherendproblemadmin.Network.POST.RegisterAccountPOST;
import com.mustaqeem.cometogetherendproblemadmin.Network.POST.RejectJobPOST;
import com.mustaqeem.cometogetherendproblemadmin.Network.GET.RejectedHairGET;
import com.mustaqeem.cometogetherendproblemadmin.Network.GET.RequestHaireGET;
import com.mustaqeem.cometogetherendproblemadmin.Network.GET.RequestJobGET;
import com.mustaqeem.cometogetherendproblemadmin.Network.POST.NotifactionAPIPOST;
import com.mustaqeem.cometogetherendproblemadmin.Network.GET.SendFeedbackGET;
import com.mustaqeem.cometogetherendproblemadmin.Network.GET.SingleUserGET;
import com.mustaqeem.cometogetherendproblemadmin.Network.POST.UpdateNewsPOST;
import com.mustaqeem.cometogetherendproblemadmin.Network.POST.VerificationUserPOST;
import com.mustaqeem.cometogetherendproblemadmin.Network.TeamImageUploadRepository;
import com.mustaqeem.cometogetherendproblemadmin.Network.POST.TeamMemberUpdatePOST;
import com.mustaqeem.cometogetherendproblemadmin.Network.TeamMemberUploadRepository;
import com.mustaqeem.cometogetherendproblemadmin.Network.TeamRepository;
import com.mustaqeem.cometogetherendproblemadmin.Network.GET.TeamSizeGET;
import com.mustaqeem.cometogetherendproblemadmin.Network.UpdateImageUploadRepository;
import com.mustaqeem.cometogetherendproblemadmin.Network.GET.NewsGET;
import com.mustaqeem.cometogetherendproblemadmin.Network.POST.NewsPOST;
import com.mustaqeem.cometogetherendproblemadmin.Response.NotifactionResponse;
import com.mustaqeem.cometogetherendproblemadmin.Response.Response;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class ViewModel extends AndroidViewModel {

    private UpdateImageUploadRepository updateImageUploadRepository;
    private NewsPOST updateUploadRepository;
    private NewsGET newsGET;
    private TeamMemberUploadRepository teamMemberUploadRepository;
    private TeamImageUploadRepository teamImageUploadRepository;
    private TeamRepository teamRepository;
    private CharityGET charityGET;
    private DeactiveUserGET deactiveUserGET;
    private RequestJobGET requestJobGET;
    private NotifactionAPIPOST notifactionPOST;
    private JobPendingDetailsGET jobDetailsGET;
    private SingleUserGET singleUserGET;
    private AcceptPOST acceptPOST;
    private JobGET jobGET;
    private RejectJobPOST rejectJobPOST;
    private RejectJobGET rejectJobGET;
    private JobRemoveDetailsGET jobRemoveDetailsGET;
    private AcceptJobDetailsGET acceptJobDetailsGET;
    private RequestHaireGET requestHaireGET;
    public RejectedHairGET rejectedHairGET;
    private AccpetHiredataGET accpetHiredata;
    private PendingHairDetailsGET pendingHairDetailsGET;
    private AcceptHairPOST acceptHairPOST;
    private AcceptHairGET acceptHairGET;
    private HairRejectedPOST hairRejectedPOST;
    private HairRejectedDetailsGET hairRejectedDetailsGET;
    private AcceptHairDetailsGET acceptHairDetailsGET;
    private LengthOfAcceptJobGET lengthOfAcceptJobGET;
    private LengthOFJobPendingGET lengthOFJobPendingGET;
    private LengthOFRejectedJobGET lengthOFRejectedJobGET;
    private LengthOFNewsGET lengthOFNewsGET;
    private LengthOFCharityGET lengthOFCharityGET;
    private LengthOFAcceptHairGET lengthOFAcceptHairGET;
    private LengthOFPendingHairGET lengthOFPendingHairGET;
    private LengthOFHairRejectedGET lengthOFhairRejectedGET;
    private NotifactionPOST notifactionhistorypost;
    public SendFeedbackGET sendFeedbackGET;
    private NotifactionReceivedGET notifactionReceivedGET;


    private NewsSizeGET newsSizeGET;
    private TeamSizeGET teamSizeGET;
    private DeleteTeamMemberPOST deleteTeamMemberPOST;
    private TeamMemberUpdatePOST teamMemberUpdatePOST;

    private DeleteNewsPOST deleteNewsPOST;
    private UpdateNewsPOST updateNewsPOST;
    private NewsSingleDataGET newsSingleDataGET;
    private UserSizeGET userSizeGET;

    private VerificationUserPOST verificationUserPOST;
    private LogOutAccountPOST logOutAccountPOST;
    private CharutyDeletePOST charutyDeletePOST;
    private RegisterAccountPOST registerAccountPOST;
    private ForgotPasswordPOST forgotPasswordPOST;

    public ViewModel(@NonNull @NotNull Application application) {
        super(application);

        updateImageUploadRepository = new UpdateImageUploadRepository(application);
        updateUploadRepository = new NewsPOST(application);
        newsGET = new NewsGET(application);
        teamMemberUploadRepository = new TeamMemberUploadRepository(application);
        teamImageUploadRepository = new TeamImageUploadRepository(application);
        teamRepository = new TeamRepository(application);
        charityGET = new CharityGET(application);
        deactiveUserGET = new DeactiveUserGET(application);
        requestJobGET = new RequestJobGET(application);
        notifactionPOST = new NotifactionAPIPOST(application);
        jobDetailsGET = new JobPendingDetailsGET(application);
        singleUserGET = new SingleUserGET(application);
        acceptPOST = new AcceptPOST(application);
        jobGET = new JobGET(application);
        requestJobGET = new RequestJobGET(application);
        rejectJobPOST = new RejectJobPOST(application);
        rejectJobGET = new RejectJobGET(application);
        jobRemoveDetailsGET = new JobRemoveDetailsGET(application);
        acceptJobDetailsGET = new AcceptJobDetailsGET(application);
        requestHaireGET = new RequestHaireGET(application);
        rejectedHairGET = new RejectedHairGET(application);
        accpetHiredata = new AccpetHiredataGET(application);
        pendingHairDetailsGET = new PendingHairDetailsGET(application);
        acceptHairPOST = new AcceptHairPOST(application);
        acceptHairGET = new AcceptHairGET(application);
        hairRejectedPOST = new HairRejectedPOST(application);
        hairRejectedDetailsGET = new HairRejectedDetailsGET(application);
        acceptHairDetailsGET = new AcceptHairDetailsGET(application);


        lengthOfAcceptJobGET = new LengthOfAcceptJobGET(application);
        lengthOFJobPendingGET = new LengthOFJobPendingGET(application);
        lengthOFRejectedJobGET = new LengthOFRejectedJobGET(application);

        lengthOFNewsGET = new LengthOFNewsGET(application);
        lengthOFCharityGET = new LengthOFCharityGET(application);

        lengthOFAcceptHairGET = new LengthOFAcceptHairGET(application);
        lengthOFPendingHairGET = new LengthOFPendingHairGET(application);
        lengthOFhairRejectedGET = new LengthOFHairRejectedGET(application);
        notifactionhistorypost = new NotifactionPOST(application);

        sendFeedbackGET = new SendFeedbackGET(application);
        notifactionReceivedGET = new NotifactionReceivedGET(application);

        newsSizeGET = new NewsSizeGET(application);
        teamSizeGET = new TeamSizeGET(application);
        deleteTeamMemberPOST = new DeleteTeamMemberPOST(application);
        teamMemberUpdatePOST = new TeamMemberUpdatePOST(application);

        deleteNewsPOST = new DeleteNewsPOST(application);
        updateNewsPOST = new UpdateNewsPOST(application);
        newsSingleDataGET = new NewsSingleDataGET(application);

        userSizeGET = new UserSizeGET(application);
        verificationUserPOST = new VerificationUserPOST(application);
        logOutAccountPOST = new LogOutAccountPOST(application);
        charutyDeletePOST = new CharutyDeletePOST(application);
        registerAccountPOST = new RegisterAccountPOST(application);

        forgotPasswordPOST = new ForgotPasswordPOST(application);
    }

    public LiveData<ImageUploadResponse> setuploadimage(Uri imageuri){
        return updateImageUploadRepository.imageupload(imageuri);
    }

    public LiveData<UpdatePostResponse> uploadpost(String Type, String Title, String Details, String Uri, String PosterPath, String AuthorBy){
        return updateUploadRepository.uploadupdate(Type, Title, Details, Uri, PosterPath, AuthorBy);
    }

    public LiveData<List<UpdateModel>> getupdate(long Limit){
        return newsGET.getupdate(Limit);
    }

    public LiveData<UpdatePostResponse> uploadteam_member(String PosterPath, String Name, String Designation){
        return teamMemberUploadRepository.update_team_member(PosterPath, Name, Designation);
    }

    public LiveData<ImageUploadResponse> uploadtem_memberimage(Uri imageuri){
        return teamImageUploadRepository.uploadimage(imageuri);
    }

    public LiveData<List<TeamModel>> getteamdata(){
        return teamRepository.getteamdata();
    }

    public LiveData<List<Charity>> getcharity(long Limit){
        return charityGET.getcharity(Limit);
    }

    public LiveData<List<UsersModel>> getDeactiveUser(boolean Val){
        return deactiveUserGET.getDeactiveUser(Val);
    }

    public LiveData<List<JobModel>> getpendingjob(long Limit){
        return requestJobGET.getjondata(Limit);
    }

    public LiveData<ResponseCode> sending_notifaction(NotifactionResponse notifactionResponse){
        return notifactionPOST.send_notifaction(notifactionResponse);
    }

    public LiveData<JobModel> getjobdetails(long Key){
        return jobDetailsGET.getjobdetails(Key);
    }

    public LiveData<UserModel> getsingleuser(String UID){
        return singleUserGET.getsingleuser(UID);
    }

    public LiveData<ResponseCode> getaccepectpost(JobModel jobModel){
        return acceptPOST.accepectjob(jobModel);
    }

    public LiveData<List<JobModel>> getallJobs(long Limit){
        return jobGET.getjobdata(Limit);
    }

    public LiveData<ResponseCode> rejectedjob(JobModel jobModel){
        return rejectJobPOST.getreject_jobpost(jobModel);
    }

    public LiveData<List<JobModel>> getrejectedjob(long Limit){
        return rejectJobGET.getrejecteddata(Limit);
    }

    public LiveData<JobModel> getRejectedJobDetails(long Key){
        return jobRemoveDetailsGET.getRejectedJobDetails(Key);
    }

    public LiveData<JobModel> getAcceptJobDetails(long Key){
        return acceptJobDetailsGET.getAcceptJobsDetails(Key);
    }

    public LiveData<List<HairModel>> getallrequest_hairdata(long Limit){
        return requestHaireGET.getRequestHairData(Limit);
    }

    public LiveData<List<HairModel>> getRejectedHaire(long Limit){
        return rejectedHairGET.getRejectedHairData(Limit);
    }

    public LiveData<List<HairModel>> getAcceptHair(long Limit){
        return accpetHiredata.getAcceptHairedata(Limit);
    }

    public LiveData<HairModel> getPendingHairDetails(long Key){
        return pendingHairDetailsGET.getpendinghair_data(Key);
    }

    public LiveData<Response> getAccepthairRequest(HairModel hairModel){
        return acceptHairPOST.post_hairpost_Accept(hairModel);
    }

    public LiveData<List<HairModel>> getAcceptHairdata(long Limit){
        return acceptHairGET.getAccepthairData(Limit);
    }

    public LiveData<Response> getHaireRejectedPost(HairModel hairModel){
        return hairRejectedPOST.haire_rejectedget(hairModel);
    }

    public LiveData<HairModel> getHairRejectedDetails(long Key){
        return hairRejectedDetailsGET.getHairRejectedDetails(Key);
    }

    public LiveData<HairModel> getHairDetails(long Key){
        return acceptHairDetailsGET.gethairAcceptdata(Key);
    }

    public LiveData<LengthOfAceptJob> getAcceptJobLength(){
        return lengthOfAcceptJobGET.getLengthAcceptJob();
    }

    public LiveData<LengthOfPendingJob> getlengthPendingJobs(){
        return lengthOFJobPendingGET.getLengthofPendingJob();
    }

    public LiveData<LengthOFRejectedJob> getLengthRejectedJobs(){
        return lengthOFRejectedJobGET.getLengthOFRejectedJob();
    }

    public LiveData<LengthOFNews> getLengthOFNews(){
        return lengthOFNewsGET.getLengthOfNews();
    }

    public LiveData<LengthOFCharity> getLengthofCharity(){
        return lengthOFCharityGET.getLengthCharity();
    }

    public LiveData<LengthOFHair> getLengthHair(){
        return lengthOFAcceptHairGET.getLengthAcceptOFCharity();
    }

    public LiveData<LengthOFPendingHair> getLengthOFPendingHair(){
        return lengthOFPendingHairGET.getLengthofPendingHair();
    }

    public LiveData<LengthOFHairRejected> getLengthOFHairRejected(){
        return lengthOFhairRejectedGET.getLengthOFHairRejected();
    }

    public LiveData<ResponseCode> send_notifactionhistory(String UID, String Message, String Type){
        return notifactionhistorypost.sendnotifaction(UID, Message, Type);
    }

    public LiveData<List<NotificationModel>> get_adminFeedback(long Limit){
        return sendFeedbackGET.getAdminSendNotifaction(Limit);
    }

    public LiveData<List<NotificationModel>> getuserFeedback(long Limit){
        return notifactionReceivedGET.getreceiver_botifaction(Limit);
    }

    public LiveData<Integer> GetNewsSize(){
        return newsSizeGET.GetSizeOFNews();
    }

    public LiveData<Integer> SizeOFTeam(){
        return teamSizeGET.SizeOFTeam();
    }

    public LiveData<Boolean> DeleteTeamMember(String TeamID){
        return deleteTeamMemberPOST.DeleteTeamMember(TeamID);
    }

    public LiveData<Boolean> TeamMemberUpdate(String PersonID, String ImageUri, String Name, String Des){
        return teamMemberUpdatePOST.UpdateTeamMember(PersonID, ImageUri, Name, Des);
    }

    public LiveData<Boolean> DeleteNews(String NewsID){
        return deleteNewsPOST.DeleteNews(NewsID);
    }

    public LiveData<Boolean> UpdateNews(String NewsID, String Type, String Title, String Details, String AuthorBy, String WebUri){
        return updateNewsPOST.UpdateNews(NewsID, Type, Title, Details, AuthorBy,  WebUri);
    }

    public LiveData<UpdateModel> NewsSingleData(String NewsID){
        return newsSingleDataGET.NewsSingleData(NewsID);
    }

    public LiveData<Integer> UserSize(){
        return userSizeGET.UserSize();
    }

    public LiveData<Boolean> Verification(String UID, String DateOFExp, String DateOFIssue, boolean CardActiveStatus, String CardNumber){
        return verificationUserPOST.Verification(UID, DateOFExp, DateOFIssue, CardActiveStatus, CardNumber);
    }

    public LiveData<Boolean> LogOutAccount(){
        return logOutAccountPOST.LogOutAccount();
    }

    public LiveData<Boolean> CharityDeletePost(String CharityID){
        return charutyDeletePOST.DeleteCharity(CharityID);
    }

    public LiveData<Boolean> RegisterAccount(String Email, String Password){
        return registerAccountPOST.RegisterAccount(Email, Password);
    }

    public LiveData<Boolean> ForGotPassword(String Email){
        return forgotPasswordPOST.ForGotPassword(Email);
    }
}
