package com.mustaqeem.cometogetherendproblemadmin.Response;

import com.google.gson.annotations.SerializedName;
import com.mustaqeem.cometogetherendproblemadmin.Model.Data;
import com.mustaqeem.cometogetherendproblemadmin.Model.Priority;

public class NotifactionResponse {

    @SerializedName("notification")
    public Data data;

    @SerializedName("to")
    public String to;

    @SerializedName("android")
    private Priority priority;

    public NotifactionResponse(){}

    public NotifactionResponse(Data data, String to, Priority priority) {
        this.data = data;
        this.to = to;
        this.priority = priority;
    }

    public NotifactionResponse(Data data, String to) {
        this.data = data;
        this.to = to;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }
}
