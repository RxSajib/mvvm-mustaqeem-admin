package com.mustaqeem.cometogetherendproblemadmin.Page;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.mustaqeem.cometogetherendproblemadmin.Model.HairModel;
import com.mustaqeem.cometogetherendproblemadmin.Model.UserModel;
import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.ViewModel.ViewModel;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;
import com.mustaqeem.cometogetherendproblemadmin.databinding.ActivityAcceptHairPageBinding;

import dmax.dialog.SpotsDialog;

public class AcceptHairPage extends AppCompatActivity {

    private ActivityAcceptHairPageBinding binding;
    private long Key;
    private String Type;
    private ViewModel viewModel;
    private android.app.AlertDialog spotsDialog;
    private String Token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_accept_hair_page);

        statusbarcolor();
        init_view();
    }

    private void statusbarcolor(){
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_white));
        }else{
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_white));
        }
    }

    private void init_view(){

        binding.BackButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Animatoo.animateSlideRight(AcceptHairPage.this);
            }
        });
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        Key = getIntent().getLongExtra(DataManager.Key, 0);
        Type = getIntent().getStringExtra(DataManager.Type);
        getdata_server(Key, Type);
    }

    private void getdata_server(long Key, String Type){
        if(Type.equals(DataManager.Rejected)){
            viewModel.getHairRejectedDetails(Key)
                    .observe(this, new Observer<HairModel>() {
                        @Override
                        public void onChanged(HairModel hairModel) {
                            if(hairModel != null){
                                setSupportActionBar(binding.Toolbar);
                                binding.MainviewID.setVisibility(View.VISIBLE);
                                binding.MessageIcon.setVisibility(View.GONE);
                                binding.MessageText.setVisibility(View.GONE);

                                getuserInfo(hairModel);
                                binding.ToolbarTitle.setText(hairModel.getWork());
                                binding.Work.setText(hairModel.getWork());
                                binding.Name.setText(hairModel.getName());
                                binding.PhoneNumber.setText(hairModel.getPhone());
                                binding.Email.setText(hairModel.getEmail());
                                binding.CvLink.setText(hairModel.getCVLink());
                                binding.Location.setText(hairModel.getLocation());

                            }else {
                                binding.MainviewID.setVisibility(View.GONE);
                                binding.MessageText.setVisibility(View.VISIBLE);
                                binding.MessageIcon.setVisibility(View.VISIBLE);
                            }
                        }
                    });
        }

        if(Type.equals(DataManager.Accept)){
            viewModel.getHairDetails(Key)
                    .observe(this, new Observer<HairModel>() {
                        @Override
                        public void onChanged(HairModel hairModel) {
                            if(hairModel != null){
                                setSupportActionBar(binding.Toolbar);
                                binding.MainviewID.setVisibility(View.VISIBLE);
                                binding.MessageIcon.setVisibility(View.GONE);
                                binding.MessageText.setVisibility(View.GONE);

                                getuserInfo(hairModel);
                                binding.ToolbarTitle.setText(hairModel.getWork());
                                binding.Work.setText(hairModel.getWork());
                                binding.Name.setText(hairModel.getName());
                                binding.PhoneNumber.setText(hairModel.getPhone());
                                binding.Email.setText(hairModel.getEmail());
                                binding.CvLink.setText(hairModel.getCVLink());
                                binding.Location.setText(hairModel.getLocation());

                            }else {
                                binding.MainviewID.setVisibility(View.GONE);
                                binding.MessageText.setVisibility(View.VISIBLE);
                                binding.MessageIcon.setVisibility(View.VISIBLE);
                            }
                        }
                    });
        }
    }

    private void getuserInfo(HairModel hairModel) {
        viewModel.getsingleuser(hairModel.getUID()).observe(this, new Observer<UserModel>() {
            @Override
            public void onChanged(UserModel userModel) {
                Token = userModel.getToken();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(AcceptHairPage.this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.feedback_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == R.id.FeedbackID){
            open_feedbackdialoag();
        }
        return true;
    }

    private void open_feedbackdialoag(){
        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(AcceptHairPage.this, R.style.PauseDialog);
        View Mview = LayoutInflater.from(getApplicationContext()).inflate(R.layout.feedback_notifaction, null, false);
        Mbuilder.setView(Mview);

        AlertDialog alertDialog = Mbuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show();

        EditText Input = Mview.findViewById(R.id.FeedbackInput);
        RelativeLayout SendButton = Mview.findViewById(R.id.SendButton);
        Input.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(Input.getWindowToken(), 0);

        SendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Input.getText().toString().trim().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Input your message", Toast.LENGTH_SHORT).show();
                } else {
                    spotsDialog = new SpotsDialog.Builder()
                            .setContext(AcceptHairPage.this)
                            .setMessage("Sending")
                            .build();
                    spotsDialog.show();
                   // sending_notication(alertDialog, Input.getText().toString().trim());
                }
            }
        });
    }

  /*  private void sending_notication(AlertDialog alertDialog, String Message) {
        if (Token != null) {
            alertDialog.dismiss();

            Data data = new Data(Message, "Feedback", "https://www.learndash.com/wp-content/uploads/Notification-Add-on.png");
            NotifactionResponse response = new NotifactionResponse(data, Token);
            viewModel.sending_notifaction(response)
                    .observe(this, new Observer<Response>() {
                        @Override
                        public void onChanged(Response response) {
                            if (response.isResponse()) {
                                spotsDialog.dismiss();
                            } else {
                                spotsDialog.dismiss();
                            }
                        }
                    });
        }
    }*/
}