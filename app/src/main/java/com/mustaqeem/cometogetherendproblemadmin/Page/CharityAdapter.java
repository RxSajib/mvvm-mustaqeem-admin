package com.mustaqeem.cometogetherendproblemadmin.Page;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mustaqeem.cometogetherendproblemadmin.Model.Charity;
import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.ViewHolder.CharityHolder;
import com.mustaqeem.cometogetherendproblemadmin.databinding.CharitySingleLayoutBinding;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class CharityAdapter extends RecyclerView.Adapter<CharityHolder>{

    private List<Charity> charityList;
    public void setCharityList(List<Charity> charityList) {
        this.charityList = charityList;
    }
    private OnclickCharity OnclickCharity;
    private LayoutInflater layoutInflater;


    @NonNull
    @NotNull
    @Override
    public CharityHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        layoutInflater = LayoutInflater.from(parent.getContext());
        CharitySingleLayoutBinding binding = CharitySingleLayoutBinding.inflate(layoutInflater, parent, false);
        return new CharityHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull CharityHolder holder, @SuppressLint("RecyclerView") int position) {

        holder.binding.NameID.setText(charityList.get(position).getName());
        holder.binding.AddressID.setText(charityList.get(position).getLocation());
        holder.binding.PhoneID.setText(charityList.get(position).getPhone());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnclickCharity.Link(charityList.get(position).getDownloadUri(), charityList.get(position).getTimestamp());
            }
        });


    }

    @Override
    public int getItemCount() {
        if(charityList == null){
            return 0;
        }
        else {
            return charityList.size();
        }
    }

    public interface OnclickCharity{
        void Link(String Uri, long UID);
    }

    public void SetOnclickLisiner(OnclickCharity OnclickCharity){
        this.OnclickCharity = OnclickCharity;
    }
}
