package com.mustaqeem.cometogetherendproblemadmin.Page;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Build;
import android.os.Bundle;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.UserTab.Expired;
import com.mustaqeem.cometogetherendproblemadmin.UserTab.Pending;
import com.mustaqeem.cometogetherendproblemadmin.UserTab.Verified;
import com.mustaqeem.cometogetherendproblemadmin.databinding.UserPageBinding;

import me.ibrahimsn.lib.OnItemSelectedListener;

public class UserPage extends AppCompatActivity {

    private UserPageBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.user_page);

        init_view();
        statusbarcolor();
    }

    private void init_view(){

        binding.BackButtonID.setOnClickListener(v -> {
            finish();
            Animatoo.animateSlideRight(UserPage.this);
        });

        goto_pending(new Pending());
        binding.SmoothBottomBar.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public boolean onItemSelect(int i) {
                switch (i){
                    case 0:
                        goto_pending(new Pending());
                        break;

                    case 1:
                        goto_verified(new Verified());
                        break;

                    case 2:
                        goto_expired(new Expired());
                        break;

                }
                return false;
            }
        });
    }

    private void statusbarcolor(){
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_white));
        }else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_white));
        }
    }

    private void goto_pending(Fragment fragment){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.FrameLayout, fragment);
        transaction.commit();
    }

    private void goto_verified(Fragment fragment){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.FrameLayout, fragment);
        transaction.commit();
    }

    private void goto_expired(Fragment fragment){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.FrameLayout, fragment);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(UserPage.this);
    }
}