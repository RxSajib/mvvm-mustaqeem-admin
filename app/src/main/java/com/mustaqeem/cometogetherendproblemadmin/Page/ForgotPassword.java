package com.mustaqeem.cometogetherendproblemadmin.Page;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.ViewModel.ViewModel;
import com.mustaqeem.cometogetherendproblemadmin.databinding.ForgotpasswordBinding;

import dmax.dialog.SpotsDialog;

public class ForgotPassword extends AppCompatActivity {

    private ForgotpasswordBinding binding;
    private ViewModel viewModel;
    private AlertDialog spotsDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.forgotpassword);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        statusbar_color();
        inti_view();
        ForGotPassword();
    }

    private void statusbar_color(){
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_white));
        } else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_white));
        }
    }

    private void inti_view(){
        binding.BackButtonID.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(ForgotPassword.this);
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Animatoo.animateSlideRight(ForgotPassword.this);
    }

    private void ForGotPassword(){
        binding.ForGotPasswordButton.setOnClickListener(view -> {
            String Email = binding.EmailInput.getText().toString().trim();
            if(Email.isEmpty()){
                Toast.makeText(this, "Email require", Toast.LENGTH_SHORT).show();
            }else {
                spotsDialog = new SpotsDialog.Builder()
                        .setContext(ForgotPassword.this)
                        .setMessage("Please wait")
                        .build();
                spotsDialog.show();
                viewModel.ForGotPassword(Email).observe(this, new Observer<Boolean>() {
                    @Override
                    public void onChanged(Boolean aBoolean) {
                        if(aBoolean){
                            spotsDialog.dismiss();
                        }else {
                            spotsDialog.dismiss();
                        }
                    }
                });
            }
        });


    }
}