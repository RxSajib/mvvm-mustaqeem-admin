package com.mustaqeem.cometogetherendproblemadmin.Page;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.ViewModel.ViewModel;
import com.mustaqeem.cometogetherendproblemadmin.databinding.RegisterBinding;

import dmax.dialog.SpotsDialog;

public class Register extends AppCompatActivity {

    private RegisterBinding binding;
    private ViewModel viewModel;
    private SpotsDialog spotsDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.register);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        statusbarcolor();
        registeraccount();
        init_view();
    }

    private void registeraccount(){
        binding.RegisterButtonID.setOnClickListener(v -> {
            String Email = binding.EmailInputID.getText().toString().trim();
            String Password = binding.PasswordInputID.getText().toString().trim();
            String ConfirmPassword = binding.ConfirmPasswordInput.getText().toString().trim();
            
            if(Email.isEmpty()){
                Toast.makeText(this, "Email require", Toast.LENGTH_SHORT).show();
            }else if(Password.isEmpty()){
                Toast.makeText(this, "Password require", Toast.LENGTH_SHORT).show();
            }else if(ConfirmPassword.isEmpty()){
                Toast.makeText(this, "Confirm Password require", Toast.LENGTH_SHORT).show();
            }else if(!Password.equals(ConfirmPassword)){
                Toast.makeText(this, "Password not match", Toast.LENGTH_SHORT).show();
            }else if(Password.length() <=7 ){
                Toast.makeText(this, "Password must be 8 char", Toast.LENGTH_SHORT).show();
            }else {
                spotsDialog = (SpotsDialog) new SpotsDialog.Builder()
                        .setContext(Register.this)
                        .setMessage("Register account")
                        .build();
                spotsDialog.show();
                viewModel.RegisterAccount(Email, Password).observe(this, new Observer<Boolean>() {
                    @Override
                    public void onChanged(Boolean aBoolean) {
                        if(aBoolean){
                            spotsDialog.dismiss();
                            gotohome();
                        }else {
                            spotsDialog.dismiss();
                        }
                    }
                });
            }
        });
    }

    private void gotohome(){
        Intent intent = new Intent(getApplicationContext(), HomeContiner.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
        Animatoo.animateSlideLeft(Register.this);
    }

    private void init_view(){
        binding.LoginNowText.setOnClickListener(v -> {
            Intent intent = new Intent(Register.this, Login.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            Animatoo.animateSlideRight(Register.this);
        });
    }

    private void statusbarcolor(){
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_white));
        }else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_white));
        }
    }
}