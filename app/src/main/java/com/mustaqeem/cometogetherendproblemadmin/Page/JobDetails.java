package com.mustaqeem.cometogetherendproblemadmin.Page;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.mustaqeem.cometogetherendproblemadmin.Model.Data;
import com.mustaqeem.cometogetherendproblemadmin.Model.JobModel;
import com.mustaqeem.cometogetherendproblemadmin.Model.ResponseCode;
import com.mustaqeem.cometogetherendproblemadmin.Model.UserModel;
import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.Response.NotifactionResponse;
import com.mustaqeem.cometogetherendproblemadmin.ViewModel.ViewModel;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;
import com.mustaqeem.cometogetherendproblemadmin.data.TimesSinceAgo;
import com.mustaqeem.cometogetherendproblemadmin.databinding.ActivityPendingPageBinding;
import com.mustaqeem.cometogetherendproblemadmin.databinding.DeclineDialoagBinding;
import com.mustaqeem.cometogetherendproblemadmin.databinding.FeedbackErrorSenddialoagBinding;
import com.mustaqeem.cometogetherendproblemadmin.databinding.FeedbackNotifactionBinding;
import com.mustaqeem.cometogetherendproblemadmin.databinding.FeedbackSendSuccessdialoagBinding;

import java.util.Currency;
import java.util.Locale;

import dmax.dialog.SpotsDialog;

public class JobDetails extends AppCompatActivity {

    private ActivityPendingPageBinding binding;
    private ViewModel viewModel;
    private long Key;
    private String Token;
    private android.app.AlertDialog spotsDialog;
    private String MyMessage;
    private String UID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_pending_page);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        statusbarcolor();
        init_view();

        myno();
    }

    private void myno(){
        Data data = new Data("ssss", "Feedback", "https://www.learndash.com/wp-content/uploads/Notification-Add-on.png");
        NotifactionResponse response = new NotifactionResponse(data, "fvjRfaZ_Q-K01h2f5wShtV:APA91bE4_PN9BbapMCM0N8PZ9mom2xzqKzPjgDQrZsTpjoItlLZ0fw3Ba_3yh1_byqib3brK7l4PKtpEg7Pyzs6ZJUT5SQt5ZMGzGzHzAPkOE7fwWq4EiAoth_SQAb5_xIEViUAOKOZC");
        viewModel.sending_notifaction(response).observe(this, new Observer<ResponseCode>() {
            @Override
            public void onChanged(ResponseCode responseCode) {

            }
        });
    }

    private void statusbarcolor(){
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
            getWindow().setStatusBarColor(getResources().getColor(R.color.white));
        }else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_white));
        }
    }

    private void init_view() {

        binding.BackButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Animatoo.animateSlideRight(JobDetails.this);
            }
        });
        Key = getIntent().getLongExtra(DataManager.Key, 0);
        UID = getIntent().getStringExtra(DataManager.UID);





        getdata(Key);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.feedback_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.FeedbackID) {
            open_feedbackdialoag();
        }
        return true;
    }

    private void open_feedbackdialoag() {
        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(JobDetails.this, R.style.PauseDialog);
        FeedbackNotifactionBinding binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.feedback_notifaction, null, false);
        Mbuilder.setView(binding.getRoot());

        AlertDialog alertDialog = Mbuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show();


        binding.FeedbackInput.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(binding.FeedbackInput.getWindowToken(), 0);

        binding.SendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.FeedbackInput.getText().toString().trim().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Input your message", Toast.LENGTH_SHORT).show();
                } else {

                    MyMessage = binding.FeedbackInput.getText().toString().trim();
                    sending_notication(alertDialog, binding.FeedbackInput.getText().toString().trim());
                }
            }
        });
    }

    private void sending_notication(AlertDialog alertDialog, String Message) {
        if (Token != null) {
            alertDialog.dismiss();
            spotsDialog = new SpotsDialog.Builder()
                    .setContext(JobDetails.this)
                    .setMessage("Sending")
                    .build();
            spotsDialog.show();

            Data data = new Data(Message, "Feedback", "https://www.learndash.com/wp-content/uploads/Notification-Add-on.png");
            NotifactionResponse response = new NotifactionResponse(data, Token);

            Log.d("Token", Token);


            viewModel.sending_notifaction(response).observe(this, new Observer<ResponseCode>() {
                @Override
                public void onChanged(ResponseCode responseCode) {
                    if(responseCode.getCode() == DataManager.SuccessCode){
                        responseCode.setCode(DataManager.DefaultCode);

                    }else if(responseCode.getCode() == DataManager.ErrorCode){
                        responseCode.setCode(DataManager.DefaultCode);
                        spotsDialog.dismiss();
                    }
                }
            });

        }
    }


    private void getdata(long Key) {
        viewModel.getjobdetails(Key).observe(this, new Observer<JobModel>() {
            @Override
            public void onChanged(JobModel jobModel) {
                if (jobModel != null) {
                    setSupportActionBar(binding.ToolbarID);
                  //  binding.JobShimmer.setVisibility(View.GONE);
                    binding.MainviewID.setVisibility(View.VISIBLE);
                    binding.MessageIcon.setVisibility(View.GONE);
                    binding.MessageText.setVisibility(View.GONE);

                    getuserInfo(jobModel);

                    String MCurrency = jobModel.getCurrency();
                    if(MCurrency.equals(DataManager.India_Rupee)){
                        Locale locale=new Locale(DataManager.EN, DataManager.IN);
                        java.util.Currency currency= Currency.getInstance(locale);
                        String symbol = currency.getSymbol();
                        binding.Currency.setText(symbol);
                    }if(MCurrency.equals(DataManager.Pakistan_Rupee)){
                        Locale locale=new Locale(DataManager.EN, DataManager.PK);
                        java.util.Currency currency=Currency.getInstance(locale);
                        String symbol = currency.getSymbol();
                        binding.Currency.setText(symbol);
                    }if(MCurrency.equals(DataManager.AED)){
                        Locale locale=new Locale(DataManager.EN, DataManager.AE);
                        java.util.Currency currency=Currency.getInstance(locale);
                        String symbol = currency.getSymbol();
                        binding.Currency.setText(symbol);
                    }if(MCurrency.equals(DataManager.BDT)){
                        Locale locale=new Locale(DataManager.EN, DataManager.BD);
                        java.util.Currency currency=Currency.getInstance(locale);
                        String symbol = currency.getSymbol();
                        binding.Currency.setText(symbol);
                    }if(MCurrency.equals(DataManager.Saudi_Riyal)){
                        Locale locale=new Locale(DataManager.EN, DataManager.SA);
                        java.util.Currency currency=Currency.getInstance(locale);
                        String symbol = currency.getSymbol();
                        binding.Currency.setText(symbol);
                    }



                    binding.Title.setText(jobModel.getIndustry());
                    binding.Location.setText(jobModel.getLocation());
                    binding.JobType.setText(jobModel.getJobType());
                    binding.Price.setText(jobModel.getStartSalary() + " - " + jobModel.getEndSalary()+" par month");

                    binding.Industry.setText(jobModel.getIndustry());
                    binding.Experiences.setText(jobModel.getExperience());
                    binding.Reward.setText(jobModel.getRewards());

                    TimesSinceAgo timesSinceAgo = new TimesSinceAgo();
                    String Time = timesSinceAgo.getTimeAgo(jobModel.getTimestamp(), getApplicationContext());
                    binding.Times.setText(Time);

                    binding.AcceptButtonID.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            open_accept_dialoag(jobModel);
                        }
                    });
                    binding.RejectButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            open_reject_dialoag(jobModel);
                        }
                    });
                }else {
                    binding.MainviewID.setVisibility(View.GONE);
                    binding.MessageIcon.setVisibility(View.VISIBLE);
                    binding.MessageText.setVisibility(View.VISIBLE);
                }


            }
        });
    }

    private void getuserInfo(JobModel jobModel) {
        viewModel.getsingleuser(jobModel.getUID()).observe(this, new Observer<UserModel>() {
            @Override
            public void onChanged(UserModel userModel) {
                Token = userModel.getToken();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(JobDetails.this);
    }

    private void open_accept_dialoag(JobModel jobModel) {
        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(JobDetails.this, R.style.PauseDialog);
        com.mustaqeem.cometogetherendproblemadmin.databinding.AcceptDiloagBinding binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.accept_diloag, null, false);
        Mbuilder.setView(binding.getRoot());

        AlertDialog alertDialog = Mbuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show();


        binding.AcceptButtonID.setOnClickListener(view1 -> {
            accept_work(alertDialog, jobModel);
        });
    }

    private void open_reject_dialoag(JobModel jobModel) {
        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(JobDetails.this, R.style.PauseDialog);
        DeclineDialoagBinding binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.decline_dialoag, null, false);
        Mbuilder.setView(binding.getRoot());

        AlertDialog alertDialog = Mbuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show();

        binding.RejectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rejected_work(alertDialog, jobModel);
            }
        });
    }

    private void rejected_work(AlertDialog alertDialog, JobModel jobModel) {
        alertDialog.dismiss();
        spotsDialog = new SpotsDialog.Builder()
                .setContext(this)
                .setMessage("Rejecting ...")
                .build();
        spotsDialog.show();

        viewModel.rejectedjob(jobModel).observe(this, new Observer<ResponseCode>() {
            @Override
            public void onChanged(ResponseCode responseCode) {
                if(responseCode.getCode() == DataManager.SuccessCode){
                    responseCode.setCode(DataManager.DefaultCode);
                    send_notifaction_history(jobModel.getUID(), jobModel.getExperience(), DataManager.Rejected);
                    spotsDialog.dismiss();
                    finish();
                    Animatoo.animateSlideRight(JobDetails.this);
                }else if(responseCode.getCode() == DataManager.ErrorCode){
                    responseCode.setCode(DataManager.DefaultCode);
                    spotsDialog.dismiss();
                }
            }
        });

    }


    private void accept_work(AlertDialog alertDialog, JobModel jobModel) {
        alertDialog.dismiss();
        spotsDialog = new SpotsDialog.Builder()
                .setContext(this)
                .setMessage("Accepting ...")
                .build();
        spotsDialog.show();
        viewModel.getaccepectpost(jobModel).observe(this, new Observer<ResponseCode>() {
            @Override
            public void onChanged(ResponseCode responseCode) {
                if(responseCode.getCode() == DataManager.SuccessCode){
                    responseCode.setCode(DataManager.DefaultCode);
                    send_notifaction_history(jobModel.getUID(), jobModel.getExperience(), DataManager.Success);
                    spotsDialog.dismiss();
                    finish();
                    Animatoo.animateSlideRight(JobDetails.this);
                }else if(responseCode.getCode() == DataManager.ErrorCode){
                    responseCode.setCode(DataManager.DefaultCode);
                    spotsDialog.dismiss();
                }
            }
        });


    }

    private void send_notifaction_history(String UID, String Message, String Type){
        viewModel.send_notifactionhistory(UID, Message, Type).observe(this, new Observer<ResponseCode>() {
            @Override
            public void onChanged(ResponseCode responseCode) {
                if(responseCode.getCode() == DataManager.SuccessCode){
                    responseCode.setCode(DataManager.DefaultCode);
                }else if(responseCode.getCode() == DataManager.ErrorCode){
                    responseCode.setCode(DataManager.DefaultCode);
                }
            }
        });
    }

    private void succsee_dialoag(){
        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(JobDetails.this, R.style.PauseDialog);
        FeedbackSendSuccessdialoagBinding binding = DataBindingUtil.inflate(LayoutInflater.from(JobDetails.this) , R.layout.feedback_send_successdialoag, null, false);
        Mbuilder.setView(binding.getRoot());

        AlertDialog alertDialog = Mbuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();

        binding.SendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Animatoo.animateSlideRight(JobDetails.this);
            }
        });
    }

    private void unsuccess_dialoag(){
        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(JobDetails.this, R.style.PauseDialog);
        FeedbackErrorSenddialoagBinding binding = DataBindingUtil.inflate(LayoutInflater.from(JobDetails.this), R.layout.feedback_error_senddialoag, null, false);

        Mbuilder.setView(binding.getRoot());

        AlertDialog alertDialog = Mbuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();

        binding.SendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Animatoo.animateSlideRight(JobDetails.this);
            }
        });
    }

}