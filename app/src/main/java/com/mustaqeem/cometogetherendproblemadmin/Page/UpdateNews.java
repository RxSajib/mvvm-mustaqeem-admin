package com.mustaqeem.cometogetherendproblemadmin.Page;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.Gravity;
import android.webkit.URLUtil;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.gmail.samehadar.iosdialog.IOSDialog;
import com.mustaqeem.cometogetherendproblemadmin.Model.UpdateModel;
import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.ViewModel.ViewModel;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;
import com.mustaqeem.cometogetherendproblemadmin.databinding.UpdatenewsBinding;

public class UpdateNews extends AppCompatActivity {

    private UpdatenewsBinding binding;
    private ViewModel viewModel;
    private long NewsID;
    private IOSDialog iosDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.updatenews);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        NewsID = getIntent().getLongExtra(DataManager.Data, 0);

        viewclick();
        getdata();
        update_data();
    }

    private void update_data(){
        binding.UpdateButton.setOnClickListener(view -> {
            String NewsType = binding.TypeInput.getText().toString().trim();
            String NewsTitle = binding.TitleInput.getText().toString().trim();
            String NewsDetails = binding.DetailsInput.getText().toString().trim();
            String NewsUri = binding.UrlInput.getText().toString().trim();
            String NewsAuthorBy = binding.AuthorByInput.getText().toString().trim();


            if(NewsType.isEmpty()){
                Toast.makeText(getApplicationContext(), "Type require", Toast.LENGTH_SHORT).show();
            } else if(NewsTitle.isEmpty()){
                Toast.makeText(getApplicationContext(), "Title require", Toast.LENGTH_SHORT).show();
            }else if(NewsDetails.isEmpty()){
                Toast.makeText(getApplicationContext(), "Details require", Toast.LENGTH_SHORT).show();
            }else if(NewsUri.isEmpty()){
                Toast.makeText(getApplicationContext(), "Uri require", Toast.LENGTH_SHORT).show();
            }else if(!URLUtil.isValidUrl(binding.UrlInput.getText().toString())){
                Toast.makeText(getApplicationContext(), "Uri is not valid", Toast.LENGTH_SHORT).show();
            }else if(NewsAuthorBy.isEmpty()){
                Toast.makeText(getApplicationContext(), "Author by require", Toast.LENGTH_SHORT).show();
            }
            else {
                progressdialoag("Updating");
                viewModel.UpdateNews(String.valueOf(NewsID), NewsType, NewsTitle, NewsDetails, NewsAuthorBy, NewsUri)
                        .observe(this, new Observer<Boolean>() {
                            @Override
                            public void onChanged(Boolean aBoolean) {
                                if(aBoolean){
                                    iosDialog.dismiss();
                                }else {
                                    iosDialog.dismiss();
                                }
                            }
                        });
            }
        });
    }

    private void viewclick(){
        binding.BackButtonID.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideLeft(UpdateNews.this);
        });
    }

    private void getdata(){
        viewModel.NewsSingleData(String.valueOf(NewsID))
                .observe(this, new Observer<UpdateModel>() {
                    @Override
                    public void onChanged(UpdateModel updateModel) {
                        if(updateModel != null){
                            binding.TypeInput.setText(updateModel.getType());
                            binding.TitleInput.setText(updateModel.getTitle());
                            binding.DetailsInput.setText(updateModel.getDetails());
                            binding.UrlInput.setText(updateModel.getWebUri());
                            binding.AuthorByInput.setText(updateModel.getAuthorBy());
                        }
                    }
                });
    }

    private void progressdialoag(String Message) {
        iosDialog = new IOSDialog.Builder(UpdateNews.this)
                .setTitle("loading")
                .setDimAmount(3)
                .setSpinnerDuration(120)
                .setMessageContentGravity(Gravity.END)
                .setCancelable(false)
                .setMessageContent(Message)
                .build();

        iosDialog.show();
    }
}