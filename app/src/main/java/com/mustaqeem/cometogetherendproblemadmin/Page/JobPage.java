package com.mustaqeem.cometogetherendproblemadmin.Page;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.mustaqeem.cometogetherendproblemadmin.JobPage.AcceptJob;
import com.mustaqeem.cometogetherendproblemadmin.JobPage.PendingJob;
import com.mustaqeem.cometogetherendproblemadmin.JobPage.RejectJob;
import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.databinding.ActivityJobPageBinding;

import me.ibrahimsn.lib.OnItemSelectedListener;

public class JobPage extends AppCompatActivity {

    private ActivityJobPageBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_job_page);


        statusbarcolor();
        set_tabs();
        init_view();
    }

    private void statusbarcolor(){
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_white));
        }else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_white));
        }
    }

    private void init_view(){
        binding.BackButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Animatoo.animateSlideRight(JobPage.this);
            }
        });
    }

    private void set_tabs(){
        goto_pendingjob(new PendingJob());
        binding.SmoothBottomBar.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public boolean onItemSelect(int i) {
                return true;
            }
        });
        binding.SmoothBottomBar.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public boolean onItemSelect(int i) {
                switch (i){
                    case 0:
                        goto_pendingjob(new PendingJob());
                        break;

                    case 1:
                        goto_acceptjob(new AcceptJob());
                        break;

                    case 2:
                        goto_rejectjob(new RejectJob());
                        break;
                }
                return true;
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(JobPage.this);
    }

    private void goto_pendingjob(Fragment fragment){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.FrameLayout, fragment);
        transaction.commit();
    }
    private void goto_acceptjob(Fragment fragment){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.FrameLayout, fragment);
        transaction.commit();
    }
    private void goto_rejectjob(Fragment fragment){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.FrameLayout, fragment);
        transaction.commit();
    }

}