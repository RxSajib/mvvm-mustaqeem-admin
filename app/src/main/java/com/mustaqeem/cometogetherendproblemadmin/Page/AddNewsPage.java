package com.mustaqeem.cometogetherendproblemadmin.Page;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.webkit.URLUtil;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.mustaqeem.cometogetherendproblemadmin.Model.ImageUploadResponse;
import com.mustaqeem.cometogetherendproblemadmin.Model.UpdatePostResponse;
import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.ViewModel.ViewModel;
import com.mustaqeem.cometogetherendproblemadmin.databinding.ActivityAddUpdatePageBinding;


import dmax.dialog.SpotsDialog;

public class AddNewsPage extends AppCompatActivity {

    private static final int  IMAGECODE = 100;
    private static final int PERMISSIONCODE = 1;
    private ViewModel viewModel;
    private String ImageDownloadUri;
    private AlertDialog spotsDialog;
    private ActivityAddUpdatePageBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_update_page);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        statusbar_color();
        init_view();
    }

    private void init_view(){

        binding.BackButtonID.setOnClickListener(v -> {
            finish();
            Animatoo.animateSlideRight(AddNewsPage.this);
        });


        binding.UploadImageButton.setOnClickListener(v -> {
            if (storespermission()) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, IMAGECODE);
            }

        });


        binding.UploadButton.setOnClickListener(v -> uploadpost());
    }

    private void statusbar_color(){
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_white));
        }else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_white));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == IMAGECODE && resultCode == RESULT_OK){

            spotsDialog = new SpotsDialog.Builder()
                    .setContext(this)
                    .setMessage("Uploading")
                    .build();
            spotsDialog.show();

            viewModel.setuploadimage(data.getData()).observe(this, new Observer<ImageUploadResponse>() {
                @Override
                public void onChanged(ImageUploadResponse imageUploadResponse) {
                    if(imageUploadResponse.isUpload()){
                        Toast.makeText(AddNewsPage.this, "Upload Success", Toast.LENGTH_SHORT).show();
                        ImageDownloadUri = imageUploadResponse.getImageUri();
                        spotsDialog.dismiss();
                        binding.UploadImageStatus.setText("Upload image success");
                    }
                    else {
                        spotsDialog.dismiss();
                        Toast.makeText(AddNewsPage.this, "Error uploading file", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private boolean storespermission(){
        if(ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
            return true;
        }else {
            ActivityCompat.requestPermissions(AddNewsPage.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSIONCODE);
            return false;
        }
    }

    private void uploadpost(){

        String Type = binding.TypeInput.getText().toString().trim();
        String Title = binding.TitleInput.getText().toString().trim();
        String Details = binding.DetailsInput.getText().toString().trim();
        String Url = binding.UrlInput.getText().toString().trim();
        String AuthorBy = binding.AuthorByInput.getText().toString().trim();

        if(Type.isEmpty()){
            Toast.makeText(getApplicationContext(), "Type require", Toast.LENGTH_SHORT).show();
        } else if(Title.isEmpty()){
            Toast.makeText(getApplicationContext(), "Title require", Toast.LENGTH_SHORT).show();
        }else if(Details.isEmpty()){
            Toast.makeText(getApplicationContext(), "Details require", Toast.LENGTH_SHORT).show();
        }else if(Url.isEmpty()){
            Toast.makeText(getApplicationContext(), "Uri require", Toast.LENGTH_SHORT).show();
        }else if(!URLUtil.isValidUrl(binding.UrlInput.getText().toString())){
            Toast.makeText(getApplicationContext(), "Uri is not valid", Toast.LENGTH_SHORT).show();
        }else if(AuthorBy.isEmpty()){
            Toast.makeText(getApplicationContext(), "Author by require", Toast.LENGTH_SHORT).show();
        }
        else {
            spotsDialog = new SpotsDialog.Builder()
                    .setContext(this)
                    .setMessage("Uploading")
                    .build();
            spotsDialog.show();
            viewModel.uploadpost(Type, Title, Details, Url, ImageDownloadUri,
                    AuthorBy)
                    .observe(this, new Observer<UpdatePostResponse>() {
                        @Override
                        public void onChanged(UpdatePostResponse updatePostResponse) {
                            if(updatePostResponse.isSuccess()){
                                spotsDialog.dismiss();
                                Toast.makeText(AddNewsPage.this, "Success", Toast.LENGTH_SHORT).show();
                                finish();
                                Animatoo.animateSlideLeft(AddNewsPage.this);
                            }else {
                                spotsDialog.dismiss();
                            }
                        }
                    });
        }


    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(AddNewsPage.this);
    }


    private int getStatusBarHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier(
                "status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);

        }else{
            height = 0;

        }

        return height;
    }
}