package com.mustaqeem.cometogetherendproblemadmin.Page;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.mustaqeem.cometogetherendproblemadmin.Model.HairModel;
import com.mustaqeem.cometogetherendproblemadmin.Model.UserModel;
import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.Response.Response;
import com.mustaqeem.cometogetherendproblemadmin.ViewModel.ViewModel;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;
import com.mustaqeem.cometogetherendproblemadmin.databinding.AcceptDiloagBinding;
import com.mustaqeem.cometogetherendproblemadmin.databinding.ActivityPendingHairDetailsBinding;
import com.mustaqeem.cometogetherendproblemadmin.databinding.ActivityPendingPageBinding;

import dmax.dialog.SpotsDialog;

public class PendingHairDetails extends AppCompatActivity {

    private ActivityPendingHairDetailsBinding binding;
    private long Key;
    private String Type;
    private ViewModel viewModel;
    private android.app.AlertDialog spotsDialog;
    private String Token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_pending_hair_details);

        statusbarcolor();
        init_view();
    }

    private void statusbarcolor() {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_white));
        } else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_white));
        }
    }

    private void init_view() {

        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        binding.BackButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Animatoo.animateSlideRight(PendingHairDetails.this);
            }
        });

        Key = getIntent().getLongExtra(DataManager.Key, 0);
        Type = getIntent().getStringExtra(DataManager.Type);
        getdata_fromserver(Key, Type);
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(PendingHairDetails.this);
    }

    private void getdata_fromserver(long Key, String Type) {
        if (Type.equals(DataManager.Pending)) {
            viewModel.getPendingHairDetails(Key)
                    .observe(this, new Observer<HairModel>() {
                        @Override
                        public void onChanged(HairModel hairModel) {
                            if (hairModel != null) {
                                binding.MainviewID.setVisibility(View.VISIBLE);
                                binding.MessageIcon.setVisibility(View.GONE);
                                binding.MessageText.setVisibility(View.GONE);

                                setSupportActionBar(binding.ToolbarID);
                                getuserInfo(hairModel);

                                binding.Work.setText(hairModel.getWork());
                                binding.Name.setText(hairModel.getName());
                                binding.PhoneNumber.setText(hairModel.getPhone());
                                binding.Location.setText(hairModel.getLocation());
                                binding.EmailID.setText(hairModel.getEmail());
                                binding.ToolbarTitle.setText(hairModel.getWork());

                                binding.AcceptButtonID.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Accepect_dialoag(hairModel);
                                    }
                                });

                                binding.RejectButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        RejectedDialoag(hairModel);
                                    }
                                });

                                if (hairModel.getCVPosterPath() != null) {
                                    binding.CVDownloadButton.setVisibility(View.VISIBLE);
                                    binding.CVDownloadButton.setOnClickListener(v -> {
                                //        Intent intent = new Intent(Intent.ACTION_VIEW);
                                    });
                                }
                                if (hairModel.getCVLink() != null) {
                                    binding.CopyLinkButton.setVisibility(View.VISIBLE);
                                    binding.CvLink.setVisibility(View.VISIBLE);
                                    binding.CvLinkText.setVisibility(View.VISIBLE);
                                    binding.CvLink.setText(hairModel.getCVLink());


                                    binding.CopyLinkButton.setOnClickListener(v -> {
                                        setClipboard(hairModel.getCVLink());
                                    });

                                }

                            } else {
                                binding.MainviewID.setVisibility(View.GONE);
                                binding.MessageIcon.setVisibility(View.VISIBLE);
                                binding.MessageText.setVisibility(View.VISIBLE);
                            }
                        }
                    });
        }

    }

    private void setClipboard(String text) {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("label", text);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(this, "copy success", Toast.LENGTH_SHORT).show();
    }

    private void getuserInfo(HairModel hairModel) {
        viewModel.getsingleuser(hairModel.getUID()).observe(this, new Observer<UserModel>() {
            @Override
            public void onChanged(UserModel userModel) {
                Token = userModel.getToken();
            }
        });
    }

    private void Accepect_dialoag(HairModel hairModel) {
        if (hairModel != null) {
            MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(PendingHairDetails.this, R.style.PauseDialog);
            AcceptDiloagBinding binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.accept_diloag, null, false);
            Mbuilder.setView(binding.getRoot());


            AlertDialog alertDialog = Mbuilder.create();
            alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            alertDialog.show();

            binding.AcceptButtonID.setOnClickListener(view -> AcceptRequest(hairModel, alertDialog));
            binding.CloseButton.setOnClickListener(view -> {
                alertDialog.dismiss();
            });
        }
    }

    private void RejectedDialoag(HairModel hairModel) {
        if (hairModel != null) {
            MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(PendingHairDetails.this, R.style.PauseDialog);
            View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.decline_dialoag, null, false);
            Mbuilder.setView(view);

            AlertDialog alertDialog = Mbuilder.create();
            alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            alertDialog.show();


            RelativeLayout RejectedButton = view.findViewById(R.id.RejectButton);
            RejectedButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    RejectedPost(alertDialog, hairModel);
                }
            });
        }
    }

    private void RejectedPost(AlertDialog alertDialog, HairModel hairModel) {
        alertDialog.dismiss();
        spotsDialog = new SpotsDialog.Builder()
                .setContext(this)
                .setMessage("Rejecting")
                .build();
        spotsDialog.show();
        viewModel.getHaireRejectedPost(hairModel)
                .observe(this, new Observer<Response>() {
                    @Override
                    public void onChanged(Response response) {
                        if (response.isResponse()) {
                            spotsDialog.dismiss();
                            finish();
                            Animatoo.animateSlideRight(PendingHairDetails.this);
                        } else {
                            spotsDialog.dismiss();
                            finish();
                            Animatoo.animateSlideRight(PendingHairDetails.this);
                        }
                    }
                });
    }

    private void AcceptRequest(HairModel hairModel, AlertDialog alertDialog) {
        alertDialog.dismiss();
        spotsDialog = new SpotsDialog.Builder()
                .setContext(this)
                .setMessage("Accepting")
                .build();
        spotsDialog.show();

        viewModel.getAccepthairRequest(hairModel)
                .observe(this, new Observer<Response>() {
                    @Override
                    public void onChanged(Response response) {
                        if (response.isResponse()) {
                            spotsDialog.dismiss();
                            finish();
                            Animatoo.animateSlideRight(PendingHairDetails.this);
                        } else {
                            spotsDialog.dismiss();
                            finish();
                            Animatoo.animateSlideRight(PendingHairDetails.this);
                        }
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.feedback_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.FeedbackID) {
            open_feedbackdialoag();
        }
        return true;
    }

    private void open_feedbackdialoag() {
        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(PendingHairDetails.this, R.style.PauseDialog);
        View Mview = LayoutInflater.from(getApplicationContext()).inflate(R.layout.feedback_notifaction, null, false);
        Mbuilder.setView(Mview);

        AlertDialog alertDialog = Mbuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show();

        EditText Input = Mview.findViewById(R.id.FeedbackInput);
        RelativeLayout SendButton = Mview.findViewById(R.id.SendButton);
        Input.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(Input.getWindowToken(), 0);

        SendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Input.getText().toString().trim().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Input your message", Toast.LENGTH_SHORT).show();
                } else {
                    spotsDialog = new SpotsDialog.Builder()
                            .setContext(PendingHairDetails.this)
                            .setMessage("Sending")
                            .build();
                    spotsDialog.show();
                    //  sending_notication(alertDialog, Input.getText().toString().trim());
                }
            }
        });
    }

  /*  private void sending_notication(AlertDialog alertDialog, String Message) {
        if (Token != null) {
            alertDialog.dismiss();

            Data data = new Data(Message, "Feedback", "https://www.learndash.com/wp-content/uploads/Notification-Add-on.png");
            NotifactionResponse response = new NotifactionResponse(data, Token);
            viewModel.sending_notifaction(response)
                    .observe(this, new Observer<Response>() {
                        @Override
                        public void onChanged(Response response) {
                            if (response.isResponse()) {
                                spotsDialog.dismiss();
                            } else {
                                spotsDialog.dismiss();
                            }
                        }
                    });
        }
    }*/
}