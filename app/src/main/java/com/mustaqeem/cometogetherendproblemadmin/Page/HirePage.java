package com.mustaqeem.cometogetherendproblemadmin.Page;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.TabHire.Accept;
import com.mustaqeem.cometogetherendproblemadmin.TabHire.Decline;
import com.mustaqeem.cometogetherendproblemadmin.TabHire.Pending;
import com.mustaqeem.cometogetherendproblemadmin.databinding.ActivityHirePageBinding;

import me.ibrahimsn.lib.OnItemSelectedListener;

public class HirePage extends AppCompatActivity {

    private ActivityHirePageBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_hire_page);


        init_view();
        statusbarcolor();
        settab();
    }

    private void settab() {
        goto_pending(new Pending());

        binding.SmoothBottomBar.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public boolean onItemSelect(int i) {
                switch (i){
                    case 0:
                        goto_pending(new Pending());
                        break;

                    case 1:
                        goto_accept(new Accept());
                        break;

                    case 2:
                        goto_decline(new Decline());
                        break;

                }
               return true;
            }
        });

    }

    private void goto_pending(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.FrameLayout, fragment);
        transaction.commit();
    }

    private void goto_accept(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.FrameLayout, fragment);
        transaction.commit();
    }

    private void goto_decline(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.FrameLayout, fragment);
        transaction.commit();
    }

    private void statusbarcolor() {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_white));
        } else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_white));
        }
    }

    private void init_view() {
        binding.BackButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Animatoo.animateSlideRight(HirePage.this);
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(HirePage.this);
    }
}