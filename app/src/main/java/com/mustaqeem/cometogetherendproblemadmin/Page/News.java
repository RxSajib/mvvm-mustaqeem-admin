package com.mustaqeem.cometogetherendproblemadmin.Page;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Build;
import android.os.Bundle;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.databinding.NewsBinding;

public class News extends AppCompatActivity {

    private NewsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.news);

        goto_news(new NewsPage());
        statusbarcolor();
        init_view();
    }
    private void statusbarcolor(){
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_white));
        }else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_white));
        }
    }

    private void init_view(){
        binding.BackButtonID.setOnClickListener(v -> {
            finish();
            Animatoo.animateSlideRight(News.this);
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(News.this);
    }

    private void goto_news(Fragment fragment){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.FrameLayout, fragment);
        transaction.commit();
    }
}