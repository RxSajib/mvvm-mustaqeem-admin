package com.mustaqeem.cometogetherendproblemadmin.Page;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.mustaqeem.cometogetherendproblemadmin.Adapter.UpdateAdapter;
import com.mustaqeem.cometogetherendproblemadmin.Model.UpdateModel;
import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.ViewModel.ViewModel;
import com.mustaqeem.cometogetherendproblemadmin.databinding.ActivityNewsPageBinding;

import java.util.List;

public class NewsPage extends Fragment {

    private ViewModel viewModel;
    private UpdateAdapter updateAdapter;
    private ActivityNewsPageBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_news_page, container, false);

        init_view();
        loaddata_server();
        return binding.getRoot();
    }

    private void init_view(){
        binding.NewsView.setHasFixedSize(true);
        updateAdapter = new UpdateAdapter();
        binding.NewsView.setAdapter(updateAdapter);

        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        binding.AddNewsButtonID.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), AddNewsPage.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            Animatoo.animateSlideLeft(getActivity());
        });
    }

    private void loaddata_server(){
        viewModel.getupdate(10).observe(getActivity(), new Observer<List<UpdateModel>>() {
            @Override
            public void onChanged(List<UpdateModel> updateModels) {
                if(updateModels != null){
                    updateAdapter.setUpdateModelList(updateModels);
                    updateAdapter.notifyDataSetChanged();

                    binding.MessageIcon.setVisibility(View.GONE);
                    binding.MessageText.setVisibility(View.GONE);

                    updateAdapter.OnDateClickEvent(new UpdateAdapter.OnDateClick() {
                        @Override
                        public void DateClick(String Uri) {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(android.net.Uri.parse(Uri));
                            startActivity(intent);
                        }
                    });
                }else {
                    binding.MessageText.setVisibility(View.VISIBLE);
                    binding.MessageIcon.setVisibility(View.VISIBLE);
                }
            }
        });
    }


}