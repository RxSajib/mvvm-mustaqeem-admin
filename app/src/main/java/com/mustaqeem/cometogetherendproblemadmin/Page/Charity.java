package com.mustaqeem.cometogetherendproblemadmin.Page;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.ViewModel.ViewModel;
import com.mustaqeem.cometogetherendproblemadmin.databinding.ActivityCharityBinding;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import dmax.dialog.SpotsDialog;

public class Charity extends Fragment {

    private ViewModel viewModel;
    private CharityAdapter adapter;
    private ActivityCharityBinding binding;
    private SpotsDialog spotsDialog;
    private int Limit = 20;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_charity_, container, false);

        init_view();
        return binding.getRoot();
    }

    private void init_view() {
        binding.ShimmerViewID.setVisibility(View.VISIBLE);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        binding.CharityView.setHasFixedSize(true);
        adapter = new CharityAdapter();
        binding.CharityView.setAdapter(adapter);

        binding.CharityView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull @NotNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(!recyclerView.canScrollVertically(1)){
                    Limit = Limit+20;
                    getdata_fromserver(Limit);
                }
            }
        });

        binding.SwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getdata_fromserver(Limit);
            }
        });

        getdata_fromserver(Limit);
    }

    private void getdata_fromserver(int Limit) {
        viewModel.getcharity(Limit).observe(getActivity(), new Observer<List<com.mustaqeem.cometogetherendproblemadmin.Model.Charity>>() {
            @Override
            public void onChanged(List<com.mustaqeem.cometogetherendproblemadmin.Model.Charity> charities) {
                if (charities != null) {
                    binding.MessageText.setVisibility(View.GONE);
                    binding.MessageIcon.setVisibility(View.GONE);
                    binding.ShimmerViewID.setVisibility(View.GONE);
                    adapter.setCharityList(charities);
                    adapter.notifyDataSetChanged();

                    adapter.SetOnclickLisiner(new CharityAdapter.OnclickCharity() {
                        @Override
                        public void Link(String Uri, long UID) {
                            opendialoag(Uri, UID);
                        }
                    });

                    binding.SwipeRefreshLayout.setRefreshing(false);
                }else {

                    adapter.setCharityList(charities);
                    adapter.notifyDataSetChanged();
                    binding.MessageIcon.setVisibility(View.VISIBLE);
                    binding.MessageText.setVisibility(View.VISIBLE);
                    binding.ShimmerViewID.setVisibility(View.GONE);
                    adapter.setCharityList(charities);
                    adapter.notifyDataSetChanged();

                    binding.SwipeRefreshLayout.setRefreshing(false);
                }
            }
        });
    }


    private void opendialoag(String MyUri, long UID){
        MaterialAlertDialogBuilder Mbuider = new MaterialAlertDialogBuilder(getActivity());
        Mbuider.setMessage("hello if u want to read the document please download after read u can remove");

        Mbuider.setPositiveButton("Download", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(MyUri));
                startActivity(intent);
            }
        });
        Mbuider.setNegativeButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleting(UID);

            }
        });
        AlertDialog alertDialog = Mbuider.create();
        alertDialog.show();

    }

    private void deleting(long uid) {
        spotsDialog = (SpotsDialog) new SpotsDialog.Builder()
                .setContext(getActivity())
                .setMessage("Deleting ...")
                .build();
        spotsDialog.show();

        viewModel.CharityDeletePost(String.valueOf(uid)).observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if(aBoolean){
                    spotsDialog.dismiss();
                }else {
                    spotsDialog.dismiss();
                }
            }
        });
    }


}