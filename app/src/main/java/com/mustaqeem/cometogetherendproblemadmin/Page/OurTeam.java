package com.mustaqeem.cometogetherendproblemadmin.Page;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Build;
import android.os.Bundle;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.mustaqeem.cometogetherendproblemadmin.Fragement.TeamPage;
import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.databinding.OurTeamBinding;
import com.mustaqeem.cometogetherendproblemadmin.databinding.TeammemberDialogBinding;

public class OurTeam extends AppCompatActivity {

    private OurTeamBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.our_team);


        init_view();
        statusbarcolor();
        goto_team(new TeamPage());
    }

    private void init_view(){
        binding.BackButtonID.setOnClickListener(v -> {
            finish();
            Animatoo.animateSlideRight(OurTeam.this);
        });
    }

    private void statusbarcolor(){
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_white));
        }else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_white));
        }
    }

    private void goto_team(Fragment fragment){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.FrameLayout, fragment);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(OurTeam.this);
    }
}