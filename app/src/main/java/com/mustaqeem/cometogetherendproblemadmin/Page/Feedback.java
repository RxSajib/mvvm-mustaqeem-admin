package com.mustaqeem.cometogetherendproblemadmin.Page;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.view.View;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.mustaqeem.cometogetherendproblemadmin.Adapter.FeedbackPagerAdapter;
import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.databinding.ActivityFeedbackBinding;

public class Feedback extends AppCompatActivity {

    private ActivityFeedbackBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_feedback);

        init_view();
    }

    private void init_view(){
        binding.BackButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Animatoo.animateSlideRight(Feedback.this);
            }
        });
        binding.ViewPager.setAdapter(new FeedbackPagerAdapter(getSupportFragmentManager(), getApplicationContext()));
        binding.TabLayout.setupWithViewPager(binding.ViewPager);

    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(Feedback.this);
    }
}