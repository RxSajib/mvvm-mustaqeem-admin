package com.mustaqeem.cometogetherendproblemadmin.Page;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.mustaqeem.cometogetherendproblemadmin.Adapter.JobAdapter;
import com.mustaqeem.cometogetherendproblemadmin.Model.Data;
import com.mustaqeem.cometogetherendproblemadmin.Model.JobModel;
import com.mustaqeem.cometogetherendproblemadmin.Model.Priority;
import com.mustaqeem.cometogetherendproblemadmin.Model.ResponseCode;
import com.mustaqeem.cometogetherendproblemadmin.Model.UserModel;
import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.Response.NotifactionResponse;
import com.mustaqeem.cometogetherendproblemadmin.ViewModel.ViewModel;
import com.mustaqeem.cometogetherendproblemadmin.data.DataManager;
import com.mustaqeem.cometogetherendproblemadmin.data.TimesSinceAgo;
import com.mustaqeem.cometogetherendproblemadmin.databinding.ActivityAcceptJobDetailsBinding;
import com.mustaqeem.cometogetherendproblemadmin.databinding.ActivityPendingPageBinding;
import com.mustaqeem.cometogetherendproblemadmin.databinding.FeedbackErrorSenddialoagBinding;
import com.mustaqeem.cometogetherendproblemadmin.databinding.FeedbackSendSuccessdialoagBinding;

import dmax.dialog.SpotsDialog;

public class AcceptJobDetails extends AppCompatActivity {

    private ActivityAcceptJobDetailsBinding binding;
    private ViewModel viewModel;
    private long Key;
    private String Type;
    private JobAdapter jobAdapter;
    private android.app.AlertDialog spotsDialog;
    private String Token;
    private String UID;
    private String MyMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(AcceptJobDetails.this, R.layout.activity_accept_job_details);

        statusbarcolor();
        init_view();
    }

    private void statusbarcolor() {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_white));
        } else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_white));
        }
    }

    private void init_view() {

        binding.BackButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Animatoo.animateSlideRight(AcceptJobDetails.this);
            }
        });

        Key = getIntent().getLongExtra(DataManager.Key, 0);
        UID = getIntent().getStringExtra(DataManager.UID);
        Type = getIntent().getStringExtra(DataManager.Type);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);


        getdata();
    }

    private void getdata() {

        if (Type.equals(DataManager.Rejected)) {
            setSupportActionBar(binding.ToolbarID);
            viewModel.getRejectedJobDetails(Key)
                    .observe(this, new Observer<JobModel>() {
                        @Override
                        public void onChanged(JobModel jobModel) {
                            if (jobModel != null) {
                                binding.MessageText.setVisibility(View.GONE);
                                binding.MessageIcon.setVisibility(View.GONE);
                                setSupportActionBar(binding.ToolbarID);
//                                    binding.JobShimmer.setVisibility(View.GONE);
                                binding.MainviewID.setVisibility(View.VISIBLE);
                                binding.Title.setText(jobModel.getIndustry());
                                getuserInfo(jobModel);
                                TimesSinceAgo timesSinceAgo = new TimesSinceAgo();
                                String Time = timesSinceAgo.getTimeAgo(jobModel.getTimestamp(), getApplicationContext());
                                binding.Times.setText(Time);

                            } else {
                                binding.MainviewID.setVisibility(View.GONE);
                                binding.MessageText.setVisibility(View.VISIBLE);
                                binding.MessageIcon.setVisibility(View.VISIBLE);
                            }
                        }
                    });
        }


        if (Type.equals(DataManager.Accept)) {
            viewModel.getAcceptJobDetails(Key)
                    .observe(this, new Observer<JobModel>() {
                        @Override
                        public void onChanged(JobModel jobModel) {
                            if (jobModel != null) {
                                setSupportActionBar(binding.ToolbarID);
                                binding.MessageIcon.setVisibility(View.GONE);
                                binding.MessageText.setVisibility(View.GONE);
                                binding.Currency.setText(jobModel.getCurrency());
//                                    binding.JobShimmer.setVisibility(View.GONE);
                                binding.MainviewID.setVisibility(View.VISIBLE);
                                binding.Title.setText(jobModel.getIndustry());
                                binding.Location.setText(jobModel.getLocation());
                                binding.JobType.setText(jobModel.getJobType());
                                binding.Price.setText(jobModel.getStartSalary() + " - " + jobModel.getEndSalary() + " par month");
                                binding.Industry.setText(jobModel.getIndustry());
                                binding.Experiences.setText(jobModel.getExperience());
                                binding.Reward.setText(jobModel.getRewards());

                                getuserInfo(jobModel);
                                TimesSinceAgo timesSinceAgo = new TimesSinceAgo();
                                String Time = timesSinceAgo.getTimeAgo(jobModel.getTimestamp(), getApplicationContext());
                                binding.Times.setText(Time);

                            } else {
                                binding.MainviewID.setVisibility(View.GONE);
                                binding.MessageIcon.setVisibility(View.VISIBLE);
                                binding.MessageText.setVisibility(View.VISIBLE);
                            }
                        }
                    });
        }


    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(AcceptJobDetails.this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.feedback_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.FeedbackID) {
            open_feedbackdialoag();
        }
        return true;
    }

    private void open_feedbackdialoag() {
        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(AcceptJobDetails.this, R.style.PauseDialog);
        View Mview = LayoutInflater.from(getApplicationContext()).inflate(R.layout.feedback_notifaction, null, false);
        Mbuilder.setView(Mview);

        AlertDialog alertDialog = Mbuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show();

        EditText Input = Mview.findViewById(R.id.FeedbackInput);
        RelativeLayout SendButton = Mview.findViewById(R.id.SendButton);
        Input.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(Input.getWindowToken(), 0);

        SendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Input.getText().toString().trim().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Input your message", Toast.LENGTH_SHORT).show();
                } else {
                    MyMessage = Input.getText().toString();
                    spotsDialog = new SpotsDialog.Builder()
                            .setContext(AcceptJobDetails.this)
                            .setMessage("Sending")
                            .build();
                    spotsDialog.show();
                    sending_notication(alertDialog, Input.getText().toString().trim());
                }
            }
        });
    }

    private void sending_notication(AlertDialog alertDialog, String Message) {
        if (Token != null) {
            alertDialog.dismiss();

            Priority priority = new Priority();
            priority.setPriority("high");

            Data data = new Data(Message, "Feedback", "https://www.learndash.com/wp-content/uploads/Notification-Add-on.png");
            NotifactionResponse response = new NotifactionResponse(data, Token, priority);

            viewModel.sending_notifaction(response).observe(this, new Observer<ResponseCode>() {
                @Override
                public void onChanged(ResponseCode responseCode) {
                    if (responseCode.getCode() == DataManager.SuccessCode) {
                        responseCode.setCode(DataManager.DefaultCode);
                        history_notifaction();
                    }
                    if (responseCode.getCode() == DataManager.ErrorCode) {
                        spotsDialog.dismiss();
                        responseCode.setCode(DataManager.DefaultCode);
                    }
                }
            });

        }
    }

    private void getuserInfo(JobModel jobModel) {
        viewModel.getsingleuser(jobModel.getUID()).observe(this, new Observer<UserModel>() {
            @Override
            public void onChanged(UserModel userModel) {
                Token = userModel.getToken();
            }
        });
    }

    private void history_notifaction() {

        viewModel.send_notifactionhistory(UID, MyMessage, DataManager.Information)
                .observe(this, new Observer<ResponseCode>() {
                    @Override
                    public void onChanged(ResponseCode responseCode) {
                        if (responseCode.getCode() == DataManager.SuccessCode) {
                            responseCode.setCode(DataManager.DefaultCode);
                            spotsDialog.dismiss();
                            succsee_dialoag();
                            Toast.makeText(getApplicationContext(), "Send success", Toast.LENGTH_SHORT).show();
                        }
                        if (responseCode.getCode() == DataManager.ErrorCode) {
                            spotsDialog.dismiss();
                            responseCode.setCode(DataManager.DefaultCode);
                            unsuccess_dialoag();
                        }
                    }
                });


    }

    private void succsee_dialoag() {
        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(AcceptJobDetails.this, R.style.PauseDialog);
        FeedbackSendSuccessdialoagBinding binding = DataBindingUtil.inflate(LayoutInflater.from(AcceptJobDetails.this), R.layout.feedback_send_successdialoag, null, false);
        Mbuilder.setView(binding.getRoot());

        AlertDialog alertDialog = Mbuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();

        binding.SendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Animatoo.animateSlideRight(AcceptJobDetails.this);
            }
        });
    }

    private void unsuccess_dialoag() {
        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(AcceptJobDetails.this, R.style.PauseDialog);
        FeedbackErrorSenddialoagBinding binding = DataBindingUtil.inflate(LayoutInflater.from(AcceptJobDetails.this), R.layout.feedback_error_senddialoag, null, false);

        Mbuilder.setView(binding.getRoot());

        AlertDialog alertDialog = Mbuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();

        binding.SendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Animatoo.animateSlideRight(AcceptJobDetails.this);
            }
        });
    }
}