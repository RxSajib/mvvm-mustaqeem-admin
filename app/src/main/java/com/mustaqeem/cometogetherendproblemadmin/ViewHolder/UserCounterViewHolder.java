package com.mustaqeem.cometogetherendproblemadmin.ViewHolder;

import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mustaqeem.cometogetherendproblemadmin.R;

import org.jetbrains.annotations.NotNull;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserCounterViewHolder extends RecyclerView.ViewHolder {

    public CircleImageView imageView;
    public RelativeLayout imagecount;

    public UserCounterViewHolder(@NonNull @NotNull View itemView) {
        super(itemView);
        imageView = itemView.findViewById(R.id.ImageView);
        imagecount = itemView.findViewById(R.id.ProfileCountID);
    }
}
