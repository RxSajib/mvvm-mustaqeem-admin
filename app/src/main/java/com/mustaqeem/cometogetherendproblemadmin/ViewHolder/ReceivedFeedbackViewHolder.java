package com.mustaqeem.cometogetherendproblemadmin.ViewHolder;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ReceivedFeedbackViewHolder extends RecyclerView.ViewHolder {

    public ReceivedFeedbackViewHolder(@NonNull View itemView) {
        super(itemView);
    }
}
