package com.mustaqeem.cometogetherendproblemadmin.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.databinding.JobsinglelayoutBinding;

public class JobViewHolder extends RecyclerView.ViewHolder {

    public JobsinglelayoutBinding binding;

    public JobViewHolder(@NonNull JobsinglelayoutBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
