package com.mustaqeem.cometogetherendproblemadmin.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.databinding.CharitySingleLayoutBinding;

import org.jetbrains.annotations.NotNull;

public class CharityHolder extends RecyclerView.ViewHolder {

    public CharitySingleLayoutBinding binding;

    public CharityHolder(@NonNull @NotNull CharitySingleLayoutBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
