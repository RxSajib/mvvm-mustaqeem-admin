package com.mustaqeem.cometogetherendproblemadmin.ViewHolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mustaqeem.cometogetherendproblemadmin.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class SendFeedbackViewHolder extends RecyclerView.ViewHolder {

    public CircleImageView Profileimage;
    public TextView Username, Time, Message;
    public ImageView FeedbackStatusIcon, FeedbackBG;

    public SendFeedbackViewHolder(@NonNull View itemView) {
        super(itemView);

        Username = itemView.findViewById(R.id.UserNameID);
        Profileimage = itemView.findViewById(R.id.Image);
        Time = itemView.findViewById(R.id.Times);
        FeedbackStatusIcon = itemView.findViewById(R.id.FeedbackIconStatus);
        Message = itemView.findViewById(R.id.MessageID);
        FeedbackBG = itemView.findViewById(R.id.FeedbackBG);
    }
}
