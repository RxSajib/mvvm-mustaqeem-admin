package com.mustaqeem.cometogetherendproblemadmin.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.databinding.UserLayoutBinding;

import org.jetbrains.annotations.NotNull;

public class UserViewHolder extends RecyclerView.ViewHolder {

    public UserLayoutBinding binding;

    public UserViewHolder(@NonNull @NotNull UserLayoutBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
