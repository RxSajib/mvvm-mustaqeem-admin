package com.mustaqeem.cometogetherendproblemadmin.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mustaqeem.cometogetherendproblemadmin.databinding.UservarifiedSinglelayoutBinding;

public class VerifiedUsersHolder extends RecyclerView.ViewHolder {

    public UservarifiedSinglelayoutBinding binding;

    public VerifiedUsersHolder(@NonNull UservarifiedSinglelayoutBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
