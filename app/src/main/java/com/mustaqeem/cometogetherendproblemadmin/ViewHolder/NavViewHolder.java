package com.mustaqeem.cometogetherendproblemadmin.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mustaqeem.cometogetherendproblemadmin.databinding.NavagationLayoutBinding;


public class NavViewHolder extends RecyclerView.ViewHolder {

    public NavagationLayoutBinding binding;

    public NavViewHolder(@NonNull NavagationLayoutBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
