package com.mustaqeem.cometogetherendproblemadmin.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.databinding.UpdateSingleIteamBinding;

import org.jetbrains.annotations.NotNull;

public class UpdateViewHolder extends RecyclerView.ViewHolder
{
    public UpdateSingleIteamBinding binding;

    public UpdateViewHolder(@NonNull @NotNull UpdateSingleIteamBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
