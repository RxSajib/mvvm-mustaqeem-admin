package com.mustaqeem.cometogetherendproblemadmin.ViewHolder;



import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mustaqeem.cometogetherendproblemadmin.databinding.TeamLayoutBinding;

import org.jetbrains.annotations.NotNull;


public class TeamViewHolder extends RecyclerView.ViewHolder {

    public TeamLayoutBinding binding;

    public TeamViewHolder(@NonNull @NotNull TeamLayoutBinding binding) {
        super(binding.getRoot());

        this.binding = binding;
    }
}
