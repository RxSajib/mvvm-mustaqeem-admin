package com.mustaqeem.cometogetherendproblemadmin.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.databinding.HairSingleItemsBinding;

public class HaireViewHolder extends RecyclerView.ViewHolder {

    public HairSingleItemsBinding binding;

    public HaireViewHolder(@NonNull HairSingleItemsBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
