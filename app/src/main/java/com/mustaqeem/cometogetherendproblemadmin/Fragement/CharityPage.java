package com.mustaqeem.cometogetherendproblemadmin.Fragement;

import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;


import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.mustaqeem.cometogetherendproblemadmin.Model.Charity;
import com.mustaqeem.cometogetherendproblemadmin.Page.CharityAdapter;
import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.ViewModel.ViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;

public class CharityPage extends Fragment {

    private RecyclerView recyclerView;
    private CharityAdapter adapter;
    private ViewModel viewModel;
    private RelativeLayout Shimmer;

    public CharityPage() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.charity_page, container, false);


        return view;
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init_view(view);
    }

    private void init_view(View view){
        Shimmer = view.findViewById(R.id.ViewCharityID);
        Shimmer.setVisibility(View.VISIBLE);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        recyclerView = view.findViewById(R.id.CharityRecylerviewID);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(3,StaggeredGridLayoutManager.VERTICAL ));
        adapter = new CharityAdapter();
        recyclerView.setAdapter(adapter);
        getcharity();
    }

    private void getcharity(){
        viewModel.getcharity(10).observe(getViewLifecycleOwner(), new Observer<List<Charity>>() {
            @Override
            public void onChanged(List<Charity> charities) {

                if(charities != null){
                    Shimmer.setVisibility(View.GONE);
                    adapter.setCharityList(charities);
                    adapter.notifyDataSetChanged();
                    adapter.SetOnclickLisiner(new CharityAdapter.OnclickCharity() {
                        @Override
                        public void Link(String Uri, long UID) {
                            MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(getActivity());
                            Mbuilder.setTitle("Do you want to download?");
                            Mbuilder.setMessage("If you want to download the file press continue");

                            Mbuilder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    download_file(Uri);
                                }
                            });
                            Mbuilder.setNeutralButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                            AlertDialog alertDialog = Mbuilder.create();
                            alertDialog.show();
                        }
                    });
                }


            }
        });
    }

    private void download_file(String Uri){
        String DownloadUrl = Uri;
        DownloadManager.Request request1 = new DownloadManager.Request(android.net.Uri.parse(DownloadUrl));
        request1.setDescription("Sample Music File");   //appears the same in Notification bar while downloading
        request1.setTitle("File1.mp3");
        request1.setVisibleInDownloadsUi(false);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            request1.allowScanningByMediaScanner();
            request1.setNotificationVisibility(DownloadManager.Request.VISIBILITY_HIDDEN);
        }
        request1.setDestinationInExternalFilesDir(getActivity(), "/File", "Question1.pdf");

        DownloadManager manager1 = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
        Objects.requireNonNull(manager1).enqueue(request1);
        if (DownloadManager.STATUS_SUCCESSFUL == 8) {

        }
    }
}