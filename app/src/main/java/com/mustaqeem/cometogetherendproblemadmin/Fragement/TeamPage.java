package com.mustaqeem.cometogetherendproblemadmin.Fragement;

import static android.app.Activity.RESULT_OK;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.gmail.samehadar.iosdialog.IOSDialog;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.mustaqeem.cometogetherendproblemadmin.Adapter.TeamAdapter;
import com.mustaqeem.cometogetherendproblemadmin.Model.ImageUploadResponse;
import com.mustaqeem.cometogetherendproblemadmin.Model.TeamModel;
import com.mustaqeem.cometogetherendproblemadmin.Model.UpdatePostResponse;
import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.ViewModel.ViewModel;
import com.mustaqeem.cometogetherendproblemadmin.databinding.TeamfragmentBinding;
import com.mustaqeem.cometogetherendproblemadmin.databinding.TeammemberDialogBinding;
import com.mustaqeem.cometogetherendproblemadmin.databinding.TeammemberOptiondialogBinding;
import com.mustaqeem.cometogetherendproblemadmin.databinding.UpdateteamLayoutBinding;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class TeamPage extends Fragment {

    private static final int IMAGEPERMISSIONCODE = 1;
    private static final int UPDATEIMAGEPERMISSIONCODE = 1;
    private ViewModel viewModel;

    private MaterialAlertDialogBuilder Mdialoag;
    private TeamAdapter teamAdapter;
    private androidx.appcompat.app.AlertDialog alertDialog;
    private TeamfragmentBinding binding;
    private TeammemberDialogBinding dialogBinding;
    private IOSDialog iosDialog;
    private String ImageDownloadUri;
    private UpdateteamLayoutBinding updatebinding;


    public TeamPage() {

    }

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.teamfragment, container, false);
        binding.TeamShimmer.setVisibility(View.VISIBLE);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init_view(view);
    }


    private void init_view(View view) {
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        binding.RecylerViewID.setHasFixedSize(true);
        teamAdapter = new TeamAdapter();

        binding.AddMemberButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                opne_dialoag();
            }
        });

        binding.RefreshlayoutID.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getdata_server();
            }
        });
        getdata_server();
    }

    private void getdata_server() {
        viewModel.getteamdata().observe(getActivity(), new Observer<List<TeamModel>>() {
            @Override
            public void onChanged(List<TeamModel> teamModels) {

                if(teamModels != null){
                    teamAdapter.setTeamModelList(teamModels);
                    binding.RecylerViewID.setAdapter(teamAdapter);
                    teamAdapter.notifyDataSetChanged();
                    binding.RefreshlayoutID.setRefreshing(false);
                    binding.TeamShimmer.setVisibility(View.GONE);

                    binding.MessageIcon.setVisibility(View.GONE);
                    binding.MessageText.setVisibility(View.GONE);

                    teamAdapter.SetOnLongClickLisiner(new TeamAdapter.SetOnClick() {
                        @Override
                        public void OnClick(long TeamPersonID, String ImageUri, String Name, String Deg) {
                            TeamDialog(TeamPersonID, ImageUri, Name, Deg);
                        }
                    });
                }else {
                    teamAdapter.setTeamModelList(teamModels);
                    binding.RecylerViewID.setAdapter(teamAdapter);
                    teamAdapter.notifyDataSetChanged();
                    binding.RefreshlayoutID.setRefreshing(false);
                    binding.TeamShimmer.setVisibility(View.GONE);

                    binding.MessageIcon.setVisibility(View.VISIBLE);
                    binding.MessageText.setVisibility(View.VISIBLE);

                }

            }
        });
    }

    private void TeamDialog(long TeamPersonID, String ImageUri, String Name, String Des){

        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(getActivity(), R.style.PauseDialog);
        TeammemberOptiondialogBinding binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.teammember_optiondialog, null, false);
        Mbuilder.setView(binding.getRoot());

        Picasso.get().load(ImageUri).into(binding.ProfileImage);
        binding.PName.setText(Name);
        binding.PDeg.setText(Des);

        AlertDialog alertDialog = Mbuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();

        binding.DeleteButton.setOnClickListener(view -> {
            delete_team(TeamPersonID, alertDialog);
        });

        binding.UpdateButton.setOnClickListener(view -> {
            update_team(alertDialog, TeamPersonID, ImageUri, Name, Des);
        });
    }

    private void update_team(AlertDialog alertDialog, long TeamPersonID, String ImageUri, String Name, String Des){
        alertDialog.dismiss();

        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(getActivity(), R.style.PauseDialog);
         updatebinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.updateteam_layout, null, false);
        Mbuilder.setView(updatebinding.getRoot());

        AlertDialog alertDialogupdate = Mbuilder.create();
        alertDialogupdate.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialogupdate.show();

        Picasso.get().load(ImageUri).into(updatebinding.PickImage);
        updatebinding.NameInput.setText(Name);
        updatebinding.DesignationInput.setText(Des);

        updatebinding.CloseButton.setOnClickListener(view -> {
            alertDialogupdate.dismiss();
        });

        updatebinding.PickImage.setOnClickListener(view -> {
            alertDialogupdate.dismiss();

            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("image/*");
            startActivityForResult(intent, UPDATEIMAGEPERMISSIONCODE);
        });

        updatebinding.UpdateButton.setOnClickListener(view -> {

            String NameText = updatebinding.NameInput.getText().toString().trim();
            String DesignationText = updatebinding.DesignationInput.getText().toString().trim();

            if(NameText.isEmpty()){
                Toast.makeText(getActivity(), "Name Require", Toast.LENGTH_SHORT).show();
            }else if(DesignationText.isEmpty()){
                Toast.makeText(getActivity(), "Designation Require", Toast.LENGTH_SHORT).show();
            }else {
                update_data(alertDialogupdate, TeamPersonID, ImageUri, NameText , DesignationText);
            }

        });
    }

    private void update_data(AlertDialog alertDialog, long TeamPersonID, String ImageUri, String Name, String Des){
        alertDialog.dismiss();
        progressdialoag("Updating");
        viewModel.TeamMemberUpdate(String.valueOf(TeamPersonID), ImageUri, Name, Des)
                .observe(getActivity(), new Observer<Boolean>() {
                    @Override
                    public void onChanged(Boolean aBoolean) {
                        if(aBoolean){
                            iosDialog.dismiss();
                        }else {
                            iosDialog.dismiss();
                        }
                    }
                });
    }

    private void delete_team(long TeamPersonID, AlertDialog alertDialog){
        alertDialog.dismiss();
        progressdialoag("Deleting");
        viewModel.DeleteTeamMember(String.valueOf(TeamPersonID))
                .observe(this, new Observer<Boolean>() {
                    @Override
                    public void onChanged(Boolean aBoolean) {
                        if(aBoolean){
                            iosDialog.dismiss();
                        }else {
                            iosDialog.dismiss();
                        }
                    }
                });

    }


    private void opne_dialoag() {
        Mdialoag = new MaterialAlertDialogBuilder(getActivity(), R.style.PauseDialog);
        dialogBinding = DataBindingUtil.inflate(getActivity().getLayoutInflater(), R.layout.teammember_dialog, null, false);
        Mdialoag.setView(dialogBinding.getRoot());

        alertDialog = Mdialoag.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();


        dialogBinding.UploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String Name = dialogBinding.NameInput.getText().toString().trim();
                String Designation = dialogBinding.DesignationInput.getText().toString().trim();

                if(Name.isEmpty()){
                    Toast.makeText(getActivity(), "Name require", Toast.LENGTH_SHORT).show();
                }else if(Designation.isEmpty()){
                    Toast.makeText(getActivity(), "Designation require", Toast.LENGTH_SHORT).show();
                }else {
                    uploadpost(Name, Designation);
                }


            }
        });

        dialogBinding.PickImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (IsExternalPermission()) {
                    Intent intent = new Intent(Intent.ACTION_PICK);
                    intent.setType("image/*");
                    startActivityForResult(intent, IMAGEPERMISSIONCODE);
                }
            }
        });


    }

    private boolean IsExternalPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, IMAGEPERMISSIONCODE);
            return false;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMAGEPERMISSIONCODE && resultCode == RESULT_OK) {

            progressdialoag("Uploading");
            dialogBinding.PickImage.setImageURI(data.getData());
            viewModel.uploadtem_memberimage(data.getData())
                    .observe(this, new Observer<ImageUploadResponse>() {
                        @Override
                        public void onChanged(ImageUploadResponse imageUploadResponse) {
                            if (imageUploadResponse.isUpload()) {
                                ImageDownloadUri = imageUploadResponse.getImageUri();
                                iosDialog.dismiss();

                            } else {
                                iosDialog.dismiss();
                            }
                        }
                    });
        } if (requestCode == UPDATEIMAGEPERMISSIONCODE && resultCode == RESULT_OK) {

            progressdialoag("Uploading");
            updatebinding.PickImage.setImageURI(data.getData());
            viewModel.uploadtem_memberimage(data.getData())
                    .observe(this, new Observer<ImageUploadResponse>() {
                        @Override
                        public void onChanged(ImageUploadResponse imageUploadResponse) {
                            if (imageUploadResponse.isUpload()) {
                                ImageDownloadUri = imageUploadResponse.getImageUri();
                                iosDialog.dismiss();

                            } else {
                                iosDialog.dismiss();
                            }
                        }
                    });
        }

    }

    private void uploadpost(String Name, String Designation) {
       progressdialoag("Uploading");
        viewModel.uploadteam_member(ImageDownloadUri, Name, Designation)
                .observe(this, new Observer<UpdatePostResponse>() {
                    @Override
                    public void onChanged(UpdatePostResponse updatePostResponse) {
                        if (updatePostResponse.isSuccess()) {
                            iosDialog.dismiss();
                            Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                            alertDialog.dismiss();
                            getdata_server();
                        } else {
                            iosDialog.dismiss();
                        }
                    }
                });
    }

    private void progressdialoag(String Message) {
        iosDialog = new IOSDialog.Builder(getActivity())
                .setTitle("loading")
                .setDimAmount(3)
                .setSpinnerDuration(120)
                .setMessageContentGravity(Gravity.END)
                .setCancelable(false)
                .setMessageContent(Message)
                .build();

        iosDialog.show();
    }

}