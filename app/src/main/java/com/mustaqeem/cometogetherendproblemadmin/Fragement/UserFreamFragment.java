package com.mustaqeem.cometogetherendproblemadmin.Fragement;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.UserTab.Expired;
import com.mustaqeem.cometogetherendproblemadmin.UserTab.Pending;
import com.mustaqeem.cometogetherendproblemadmin.UserTab.Verified;
import com.mustaqeem.cometogetherendproblemadmin.databinding.FragmentUserFreamBinding;

import me.ibrahimsn.lib.OnItemSelectedListener;

public class UserFreamFragment extends Fragment {

    private FragmentUserFreamBinding binding;

    public UserFreamFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_user_fream, container, false);

        clickevent();
        return binding.getRoot();
    }

    private void clickevent(){
        goto_pending(new Pending());
        binding.SmoothBottomBar.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public boolean onItemSelect(int i) {
                switch (i){
                    case 0:
                        goto_pending(new Pending());
                        break;

                    case 1:
                        goto_verified(new Verified());
                        break;

                    case 2:
                        goto_expired(new Expired());
                        break;

                }
                return false;
            }
        });

    }

    private void goto_pending(Fragment fragment){
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.UserFrameLayout, fragment);
        transaction.commit();
    }

    private void goto_verified(Fragment fragment){
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.UserFrameLayout, fragment);
        transaction.commit();
    }

    private void goto_expired(Fragment fragment){
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.UserFrameLayout, fragment);
        transaction.commit();
    }
}