package com.mustaqeem.cometogetherendproblemadmin.Fragement;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.mustaqeem.cometogetherendproblemadmin.Adapter.CounterUserAdapter;
import com.mustaqeem.cometogetherendproblemadmin.Model.LengthOFCharity;
import com.mustaqeem.cometogetherendproblemadmin.Model.LengthOFHair;
import com.mustaqeem.cometogetherendproblemadmin.Model.LengthOFHairRejected;
import com.mustaqeem.cometogetherendproblemadmin.Model.LengthOFNews;
import com.mustaqeem.cometogetherendproblemadmin.Model.LengthOFPendingHair;
import com.mustaqeem.cometogetherendproblemadmin.Model.LengthOFRejectedJob;
import com.mustaqeem.cometogetherendproblemadmin.Model.LengthOfAceptJob;
import com.mustaqeem.cometogetherendproblemadmin.Model.LengthOfPendingJob;
import com.mustaqeem.cometogetherendproblemadmin.Model.UsersModel;
import com.mustaqeem.cometogetherendproblemadmin.OverLapIteam.OverLapDecraion;
import com.mustaqeem.cometogetherendproblemadmin.Page.Feedback;
import com.mustaqeem.cometogetherendproblemadmin.Page.HirePage;
import com.mustaqeem.cometogetherendproblemadmin.Page.JobPage;
import com.mustaqeem.cometogetherendproblemadmin.Page.MyCharity;
import com.mustaqeem.cometogetherendproblemadmin.Page.News;
import com.mustaqeem.cometogetherendproblemadmin.Page.OurTeam;
import com.mustaqeem.cometogetherendproblemadmin.Page.UserPage;
import com.mustaqeem.cometogetherendproblemadmin.R;
import com.mustaqeem.cometogetherendproblemadmin.ViewModel.ViewModel;
import com.mustaqeem.cometogetherendproblemadmin.databinding.FragmentHomePageBinding;

import java.util.List;


public class HomePage extends Fragment {

    private CounterUserAdapter userAdapter;
    private ViewModel viewModel;
    private FragmentHomePageBinding binding;

    public HomePage() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home_page, container, false);
        init_view();

        return  binding.getRoot();
    }

    private void init_view(){

        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if(task.isSuccessful()){
                    Log.d("Token", task.getResult().getToken());
                }
            }
        });

        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        binding.UserRecylerViewID.setHasFixedSize(true);
        userAdapter = new CounterUserAdapter();
        binding.UserRecylerViewID.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        binding.UserRecylerViewID.addItemDecoration(new OverLapDecraion());
        binding.UserRecylerViewID.setAdapter(userAdapter);

        binding.AllUserButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), UserPage.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                Animatoo.animateSlideLeft(getActivity());
            }
        });

        viewModel.getDeactiveUser(false).observe(getViewLifecycleOwner(), new Observer<List<UsersModel>>() {
            @Override
            public void onChanged(List<UsersModel> usersModels) {
                if(usersModels !=null){
                    userAdapter.setUserModelList(usersModels);
                    userAdapter.notifyDataSetChanged();

                }

            }
        });

        binding.TeamButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), OurTeam.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                Animatoo.animateSlideLeft(getActivity());
            }
        });

        binding.NewsButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), News.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                Animatoo.animateSlideLeft(getActivity());
            }
        });

        binding.HireButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), HirePage.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                Animatoo.animateSlideLeft(getActivity());
            }
        });

        binding.CharityButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MyCharity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                Animatoo.animateSlideLeft(getActivity());

            }
        });

        binding.JobsButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goto_jobpage();
            }
        });

        binding.FeedbackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goto_feedbackpage();
            }
        });

        getall_datacount();
    }

    private void getall_datacount(){
        viewModel.getAcceptJobLength().observe(getViewLifecycleOwner(), new Observer<LengthOfAceptJob>() {
            @Override
            public void onChanged(LengthOfAceptJob lengthObject) {
                if(lengthObject != null){
                   binding.AcceptJobCount.setText(String.valueOf(lengthObject.getLength()));
                }
            }
        });

        viewModel.getlengthPendingJobs().observe(getViewLifecycleOwner(), new Observer<LengthOfPendingJob>() {
            @Override
            public void onChanged(LengthOfPendingJob lengthOfPendingJob) {
                if(lengthOfPendingJob != null){
                    binding.PendingJobCount.setText(String.valueOf(lengthOfPendingJob.getLength()));
                }
            }
        });

        viewModel.getLengthRejectedJobs().observe(getViewLifecycleOwner(), new Observer<LengthOFRejectedJob>() {
            @Override
            public void onChanged(LengthOFRejectedJob lengthOFRejectedJob) {
                if(lengthOFRejectedJob != null){
                    binding.JobRejectedCount.setText(String.valueOf(lengthOFRejectedJob.getLength()));
                }
            }
        });

        viewModel.getLengthOFNews().observe(getViewLifecycleOwner(), new Observer<LengthOFNews>() {
            @Override
            public void onChanged(LengthOFNews lengthOFNews) {
                if(lengthOFNews != null){
                    binding.LengthOFNews.setText(String.valueOf(lengthOFNews.getLength()));
                }
            }
        });

        viewModel.getLengthofCharity().observe(getViewLifecycleOwner(), new Observer<LengthOFCharity>() {
            @Override
            public void onChanged(LengthOFCharity lengthOFCharity) {
                if(lengthOFCharity != null){
                    binding.CharityCount.setText(String.valueOf(lengthOFCharity.getLength()));
                }
            }
        });

        viewModel.getLengthHair().observe(getViewLifecycleOwner(), new Observer<LengthOFHair>() {
            @Override
            public void onChanged(LengthOFHair lengthOFHair) {
                if(lengthOFHair != null){
                    binding.LengthOFHair.setText(String.valueOf(lengthOFHair.getLength()));
                }
            }
        });

        viewModel.getLengthOFPendingHair().observe(getViewLifecycleOwner(), new Observer<LengthOFPendingHair>() {
            @Override
            public void onChanged(LengthOFPendingHair lengthOFPendingHair) {
                if(lengthOFPendingHair != null){
                    binding.LengthPendingHair.setText(String.valueOf(lengthOFPendingHair.getLength()));
                }
            }
        });

        viewModel.getLengthOFHairRejected().observe(getViewLifecycleOwner(), new Observer<LengthOFHairRejected>() {
            @Override
            public void onChanged(LengthOFHairRejected lengthOFHairRejected) {
                if(lengthOFHairRejected != null){
                    binding.LengthOFHairRejected.setText(String.valueOf(lengthOFHairRejected.getLength()));
                }
            }
        });
    }

    private void goto_jobpage(){
        Intent intent = new Intent(getActivity(), JobPage.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Animatoo.animateSlideLeft(getActivity());
    }

    private void goto_feedbackpage(){
        Intent intent = new Intent(getActivity(), Feedback.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Animatoo.animateSlideLeft(getActivity());
    }
}